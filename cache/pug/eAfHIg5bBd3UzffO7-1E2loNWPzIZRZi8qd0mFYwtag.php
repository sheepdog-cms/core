<html lang="en"<?php $__classes = implode(" ", array_unique(array_merge(empty($__classes) ? array() : explode(" ", $__classes), array (
), array())) ); ?><?php if (!empty($__classes)) { echo ' class="' . $__classes . '"'; } unset($__classes);  ?>><head><meta charset="utf-8"<?php $__classes = implode(" ", array_unique(array_merge(empty($__classes) ? array() : explode(" ", $__classes), array (
), array())) ); ?><?php if (!empty($__classes)) { echo ' class="' . $__classes . '"'; } unset($__classes);  ?>><title><?php echo \Jade\Compiler::getEscapedValue($site['app_name'], '"') ?></title><link rel="stylesheet" href="/resources/css/install.css"<?php $__classes = implode(" ", array_unique(array_merge(empty($__classes) ? array() : explode(" ", $__classes), array (
), array())) ); ?><?php if (!empty($__classes)) { echo ' class="' . $__classes . '"'; } unset($__classes);  ?>></head><body><div<?php $__classes = implode(" ", array_unique(array_merge(empty($__classes) ? array() : explode(" ", $__classes), array (
  0 => 'flex-center',
  1 => 'position-ref',
  2 => 'full-height',
), array())) ); ?><?php if (!empty($__classes)) { echo ' class="' . $__classes . '"'; } unset($__classes);  ?>><div<?php $__classes = implode(" ", array_unique(array_merge(empty($__classes) ? array() : explode(" ", $__classes), array (
  0 => 'content',
), array())) ); ?><?php if (!empty($__classes)) { echo ' class="' . $__classes . '"'; } unset($__classes);  ?>><div<?php $__classes = implode(" ", array_unique(array_merge(empty($__classes) ? array() : explode(" ", $__classes), array (
  0 => 'title',
  1 => 'm-b-md',
), array())) ); ?><?php if (!empty($__classes)) { echo ' class="' . $__classes . '"'; } unset($__classes);  ?>><?php echo \Jade\Compiler::getEscapedValue($site['app_name'], '"') ?></div><?php if(!$php) { ?><div<?php $__classes = implode(" ", array_unique(array_merge(empty($__classes) ? array() : explode(" ", $__classes), array (
  0 => 'error',
), array())) ); ?><?php if (!empty($__classes)) { echo ' class="' . $__classes . '"'; } unset($__classes);  ?>>PHP 7.0 or greater must be installed.</div><?php } if(!$ext) { ?><div<?php $__classes = implode(" ", array_unique(array_merge(empty($__classes) ? array() : explode(" ", $__classes), array (
  0 => 'error',
), array())) ); ?><?php if (!empty($__classes)) { echo ' class="' . $__classes . '"'; } unset($__classes);  ?>>Make sure that PDO and PDO_MYSQL are installed.</div><?php } if(!$writeable) { ?><div<?php $__classes = implode(" ", array_unique(array_merge(empty($__classes) ? array() : explode(" ", $__classes), array (
  0 => 'error',
), array())) ); ?><?php if (!empty($__classes)) { echo ' class="' . $__classes . '"'; } unset($__classes);  ?>>Make sure that the installation root directory and its cache directory are writeable.</div><?php } if($error) { ?><div<?php $__classes = implode(" ", array_unique(array_merge(empty($__classes) ? array() : explode(" ", $__classes), array (
  0 => 'error',
), array())) ); ?><?php if (!empty($__classes)) { echo ' class="' . $__classes . '"'; } unset($__classes);  ?>><?php echo \Jade\Compiler::getEscapedValue($errordetails, '"') ?></div><div<?php $__classes = implode(" ", array_unique(array_merge(empty($__classes) ? array() : explode(" ", $__classes), array (
  0 => 'error',
), array())) ); ?><?php if (!empty($__classes)) { echo ' class="' . $__classes . '"'; } unset($__classes);  ?>>Got some error during the installation. Refresh and try again.</div><?php } if($ext && $php && $writeable && !$error) { ?><form method="POST"<?php $__classes = implode(" ", array_unique(array_merge(empty($__classes) ? array() : explode(" ", $__classes), array (
), array())) ); ?><?php if (!empty($__classes)) { echo ' class="' . $__classes . '"'; } unset($__classes);  ?>><div<?php $__classes = implode(" ", array_unique(array_merge(empty($__classes) ? array() : explode(" ", $__classes), array (
  0 => 'form-control',
), array())) ); ?><?php if (!empty($__classes)) { echo ' class="' . $__classes . '"'; } unset($__classes);  ?>><label for="sitename"<?php $__classes = implode(" ", array_unique(array_merge(empty($__classes) ? array() : explode(" ", $__classes), array (
), array())) ); ?><?php if (!empty($__classes)) { echo ' class="' . $__classes . '"'; } unset($__classes);  ?>>Site name</label><input id="sitename" type="text" name="sitename" required<?php $__classes = implode(" ", array_unique(array_merge(empty($__classes) ? array() : explode(" ", $__classes), array (
), array())) ); ?><?php if (!empty($__classes)) { echo ' class="' . $__classes . '"'; } unset($__classes);  ?>></div><div<?php $__classes = implode(" ", array_unique(array_merge(empty($__classes) ? array() : explode(" ", $__classes), array (
  0 => 'form-control',
), array())) ); ?><?php if (!empty($__classes)) { echo ' class="' . $__classes . '"'; } unset($__classes);  ?>><label for="sitedesc"<?php $__classes = implode(" ", array_unique(array_merge(empty($__classes) ? array() : explode(" ", $__classes), array (
), array())) ); ?><?php if (!empty($__classes)) { echo ' class="' . $__classes . '"'; } unset($__classes);  ?>>Site description</label><input id="sitedesc" type="text" name="sitedesc"<?php $__classes = implode(" ", array_unique(array_merge(empty($__classes) ? array() : explode(" ", $__classes), array (
), array())) ); ?><?php if (!empty($__classes)) { echo ' class="' . $__classes . '"'; } unset($__classes);  ?>></div><div<?php $__classes = implode(" ", array_unique(array_merge(empty($__classes) ? array() : explode(" ", $__classes), array (
  0 => 'form-control',
), array())) ); ?><?php if (!empty($__classes)) { echo ' class="' . $__classes . '"'; } unset($__classes);  ?>><label for="admname"<?php $__classes = implode(" ", array_unique(array_merge(empty($__classes) ? array() : explode(" ", $__classes), array (
), array())) ); ?><?php if (!empty($__classes)) { echo ' class="' . $__classes . '"'; } unset($__classes);  ?>>Admin name</label><input id="admname" type="text" name="admname" required<?php $__classes = implode(" ", array_unique(array_merge(empty($__classes) ? array() : explode(" ", $__classes), array (
), array())) ); ?><?php if (!empty($__classes)) { echo ' class="' . $__classes . '"'; } unset($__classes);  ?>></div><div<?php $__classes = implode(" ", array_unique(array_merge(empty($__classes) ? array() : explode(" ", $__classes), array (
  0 => 'form-control',
), array())) ); ?><?php if (!empty($__classes)) { echo ' class="' . $__classes . '"'; } unset($__classes);  ?>><label for="admpass"<?php $__classes = implode(" ", array_unique(array_merge(empty($__classes) ? array() : explode(" ", $__classes), array (
), array())) ); ?><?php if (!empty($__classes)) { echo ' class="' . $__classes . '"'; } unset($__classes);  ?>>Admin password</label><input id="admpass" type="password" name="admpass" required<?php $__classes = implode(" ", array_unique(array_merge(empty($__classes) ? array() : explode(" ", $__classes), array (
), array())) ); ?><?php if (!empty($__classes)) { echo ' class="' . $__classes . '"'; } unset($__classes);  ?>></div><div<?php $__classes = implode(" ", array_unique(array_merge(empty($__classes) ? array() : explode(" ", $__classes), array (
  0 => 'form-control',
), array())) ); ?><?php if (!empty($__classes)) { echo ' class="' . $__classes . '"'; } unset($__classes);  ?>><label for="dbhost"<?php $__classes = implode(" ", array_unique(array_merge(empty($__classes) ? array() : explode(" ", $__classes), array (
), array())) ); ?><?php if (!empty($__classes)) { echo ' class="' . $__classes . '"'; } unset($__classes);  ?>>MySQL Host</label><input id="dbhost" type="text" name="dbhost" required<?php $__classes = implode(" ", array_unique(array_merge(empty($__classes) ? array() : explode(" ", $__classes), array (
), array())) ); ?><?php if (!empty($__classes)) { echo ' class="' . $__classes . '"'; } unset($__classes);  ?>></div><div<?php $__classes = implode(" ", array_unique(array_merge(empty($__classes) ? array() : explode(" ", $__classes), array (
  0 => 'form-control',
), array())) ); ?><?php if (!empty($__classes)) { echo ' class="' . $__classes . '"'; } unset($__classes);  ?>><label for="dbname"<?php $__classes = implode(" ", array_unique(array_merge(empty($__classes) ? array() : explode(" ", $__classes), array (
), array())) ); ?><?php if (!empty($__classes)) { echo ' class="' . $__classes . '"'; } unset($__classes);  ?>>Database Name</label><input id="dbname" type="text" name="dbname" required<?php $__classes = implode(" ", array_unique(array_merge(empty($__classes) ? array() : explode(" ", $__classes), array (
), array())) ); ?><?php if (!empty($__classes)) { echo ' class="' . $__classes . '"'; } unset($__classes);  ?>></div><div<?php $__classes = implode(" ", array_unique(array_merge(empty($__classes) ? array() : explode(" ", $__classes), array (
  0 => 'form-control',
), array())) ); ?><?php if (!empty($__classes)) { echo ' class="' . $__classes . '"'; } unset($__classes);  ?>><label for="dbuser"<?php $__classes = implode(" ", array_unique(array_merge(empty($__classes) ? array() : explode(" ", $__classes), array (
), array())) ); ?><?php if (!empty($__classes)) { echo ' class="' . $__classes . '"'; } unset($__classes);  ?>>MySQL User</label><input id="dbuser" type="text" name="dbuser" required<?php $__classes = implode(" ", array_unique(array_merge(empty($__classes) ? array() : explode(" ", $__classes), array (
), array())) ); ?><?php if (!empty($__classes)) { echo ' class="' . $__classes . '"'; } unset($__classes);  ?>></div><div<?php $__classes = implode(" ", array_unique(array_merge(empty($__classes) ? array() : explode(" ", $__classes), array (
  0 => 'form-control',
), array())) ); ?><?php if (!empty($__classes)) { echo ' class="' . $__classes . '"'; } unset($__classes);  ?>><label for="dbpass"<?php $__classes = implode(" ", array_unique(array_merge(empty($__classes) ? array() : explode(" ", $__classes), array (
), array())) ); ?><?php if (!empty($__classes)) { echo ' class="' . $__classes . '"'; } unset($__classes);  ?>>MySQL Password</label><input id="dbpass" type="password" name="dbpass" required<?php $__classes = implode(" ", array_unique(array_merge(empty($__classes) ? array() : explode(" ", $__classes), array (
), array())) ); ?><?php if (!empty($__classes)) { echo ' class="' . $__classes . '"'; } unset($__classes);  ?>></div><input type="submit" name="submit" value="Install"<?php $__classes = implode(" ", array_unique(array_merge(empty($__classes) ? array() : explode(" ", $__classes), array (
), array())) ); ?><?php if (!empty($__classes)) { echo ' class="' . $__classes . '"'; } unset($__classes);  ?>></form><?php } ?></div></div></body></html>