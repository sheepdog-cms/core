<?php
/*
 * View Engine
 * Dispatches and caches parser instances.
 *
 * @package Meerkat
 * @since   0.0.4
*/
namespace Meerkat\Views;
use Meerkat\Panic\Panic;

class MeerkatViewEngine {
    private $Parsers = array();

    /*
     * Dispatches view engine.
     *
     * @param string $Filename
     * @param array $Data
     * @return mixed
    */
    public function render ($Filename, $Data) {
        global $config;

        $engine = explode('.', $Filename);
        $ext = $engine[sizeof($engine) - 1];

        if (!array_key_exists($ext, $this->Parsers)) {
            if (!array_key_exists($ext, $config['views']['types'])) {
                return 500;
            }

            $class = $config['views']['types'][$ext]['class'];
            $this->Parsers[$ext] = new $class($config['views']['types'][$ext]);
        }

        $view_path = "";
        foreach ($config['views']['search_path'] as $viewpathc) {
            if (file_exists($viewpathc . $Filename)) {
                $view_path = $viewpathc;
                break;
            }
        }

        if ($view_path === "") {
            return 500;
        }

        return $this->Parsers[$engine[1]]->render($view_path . $Filename, $Data);
    }
}