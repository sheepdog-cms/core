<?php
/*
 * View function
 * Returns generated HTML code from a view.
 *
 * @package Meerkat
 * @since   0.0.3
*/
require __DIR__ . '/ViewEngine.php';
use Meerkat\Views\MeerkatViewEngine;

global $config;
$viewengine = new MeerkatViewEngine();

function view ($ViewName, $ViewData = array()) {
    global $config;
    global $viewengine;

    $ViewData = array_merge($ViewData, array(
        'site' => $config
    ));

    return $viewengine->render($ViewName, $ViewData);
}