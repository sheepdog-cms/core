<?php
/*
 * Pug Template Engine
 * Pug-PHP wrapper for Meerkat.
 *
 * @package Meerkat
 * @since   0.0.4
*/
namespace Meerkat\Views\Engines;
use Pug\Pug;

class PugASheep {
    private $Pug = null;

    /*
     * Constructs Pug object.
     *
     * @param array $config
    */
    public function __construct ($config) {
        $this->Pug = new Pug(array(
            'prettyprint' => false,
            'extension' => '.pug',
            'cache' => $config['cache']
        ));
    }

    /*
     * Renders a template.
     *
     * @param string $File
     * @param array Data
    */
    public function render ($File, $Data) {
        return $this->Pug->render($File, $Data);
    }
}