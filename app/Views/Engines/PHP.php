<?php
/*
 * PHP Template Engine
 *
 * @package Meerkat
 * @since   0.0.6
*/
namespace Meerkat\Views\Engines;
use ReflectionFunction;

class PHP {

    /*
     * Renders a template.
     *
     * As of 0.4.0, new execution calls are disabled.
     * Enabling them will increase the performance
     * but may impact compatibility of views.
     *
     * The default setting may change soon.
     *
     * @param string $File
     * @param array Data
    */
    public function render ($File, $Data) {
        $func = require $File;

        global $config;

        if ($config['views']['types']['php']['force_new_execution_calls']) {
            unset($Data['site']);

            return $func($config, $Data);
        } else {
            $fct = new ReflectionFunction($func);

            if ($fct->getNumberOfRequiredParameters() >= 2) {
                unset($Data['site']);

                return $func($config, $Data);
            } else {
                return $func($Data);
            }
        }
    }
}