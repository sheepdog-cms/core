<?php
/*
 * Configuration preset
 * The default configuration. Do all of the overrides in
 * config.php from the application installation root directory
 * (/config.php, not /app/config.php).
 *
 * @package Meerkat
 * @since   0.0.2
*/

return array(
    // Application
    'app.name' => 'Meerkat',
    'app.version' => '0.7.8',
    'app.lang' => 'en-US',
    'site.name' => 'Meerkat',
    'debug' => false,
    'meerkat.extensions' => 'English,Theming2,PasswordAuthentication,PermissionsMiddleware,markdown,UrlTransliterator',

    // Caching
    'site.enable_cache_avoiding' => false,

    // Views
    'views' => array(
        'search_path' => array(
            __DIR__ . '/../resources/views/'
        ),
        'types' => array(
            'pug' => array(
                'class' => 'Meerkat\Views\Engines\PugASheep',
                'cache' => __DIR__ . '/../cache/pug/'
            ),
            'php' => array(
                'class' => 'Meerkat\Views\Engines\PHP',
                'force_new_execution_calls' => true
            )
        )
    ),

    // Cross Site Request Forgery
    'csrf.ignore' => array(),
    'csrf.enabled' => true,

    // Enable APIs (internal and integrations)
    'api.enable_v1' => false,
    'api.enable_v2' => false,
    'api.enable_v3' => true,
    'api.theming2' => true,

    // Site-wide experiments
    'experiments.default' => '',

    // Stacks
    'stacks.initialization.load_outdated' => true,

    // Database drivers and migrations
    'mysql.support_legacy' => false
);