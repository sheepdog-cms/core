<?php
/*
* API Formatter
* Formats the API responses.
*
* @package Meerkat
* @since   0.6.0
*/

function xml_encode_node ($input) {
    $out = "";

    foreach ($input as $key => $value) {
        if (is_array($value)) {
            if (is_int($key)) {
                $key = 'Object';
            }

            $out .= "<" . $key . ">" . xml_encode_node($value) . "</" . $key . ">";
        } else {
            $out .= "<" . $key . ">" . $value . "</" . $key . ">";
        }
    }

    return $out;
}

function xml_encode ($input) {
    $out = '<?xml version="1.0" encoding="UTF-8"?><response>';
    $out .= xml_encode_node($input);

    return $out . '</response>';
}