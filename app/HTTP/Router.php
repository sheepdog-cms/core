<?php
/*
 * Router
 * Calls appriopate functions basing on detected route match.
 *
 * @package Meerkat
 * @since   0.0.1
*/
namespace Meerkat\HTTP;
use Meerkat\HTTP\Status;
use Meerkat\Panic\Panic;
use Exception;

require __DIR__ . '/APIFormatter.php';

class Router {

    /*
     * Routes collection.
     * Use Route class instead of modifying this array.
    */
    public $Routes = array(
        "GET" => array(),
        "PUT" => array(),
        "DELETE" => array(),
        "POST" => array(),
        "PATCH" => array(),
        "OPTIONS" => array()
    );

    /*
     * Middleware array
     */
    public $Middlewares = array();

    /*
     * Registers a middleware.
     *
     * @param $Address string
    */
    public function RegisterMiddleware ($Address)
    {
        $this->Middlewares[] = $Address;
    }

    /*
     * Match
     * Finds a controller for request uri.
    */
    public function Match () {
        $_SERVER['REQUEST_URI'] = $this->NormalizePath($_SERVER['REQUEST_URI']);

        global $stacks;
        $stacks->Invoke('Meerkat\\Router\\BeforeMatch');

        $requestMethod = $_SERVER['REQUEST_METHOD'];
        $requestUri = $_SERVER['REQUEST_URI'];
        $raise404 = true;

        if (($pos = strpos($requestUri, '?')) !== false) {
            $requestUri = substr($requestUri, 0, $pos);
        }

        $uribreakdown = explode('/', substr($requestUri, 1, strlen($requestUri)));

        foreach ($this->Routes[$requestMethod] as $route => $controller) {
            $r=$route;//
            $route = explode('/', substr($route, 1, strlen($route)));
            
            $params = array();
            $matches = sizeof($uribreakdown) == sizeof($route);

            if (($pos = strpos($r, '/:')) !== false) {
                $matches = $matches || sizeof($uribreakdown) >= sizeof($route);
            }

            if ($matches) {
                $index = 0;

                foreach($route as $part) {
                    if (strlen($part) > 0) {
                        if ($part[0] === ':') {
                            $params[substr($part, 1, strlen($part))] = $uribreakdown[$index];
                            $index += 1;
                            continue;
                        }
                    }

                    if ($part !== $uribreakdown[$index]) {
                        $matches = false;
                        break;
                    }

                    $index += 1;
                }
            }

            if ($matches) {
                $raise404 = false;
                $this->ResolveController($controller, $params);
                break;
            }
        }
        
        if ($raise404) {
            $this->ResolveController($this->Routes['GET']['@404']);
        }
    }

    /*
     * Normalizes path.
    */
    public function NormalizePath($filename) {
        $path = '';

        foreach (explode('/', $filename) as $part) {
            if (empty($part) || $part === '.' || $part === '..') continue;

            $path .= '/' . $part;
        }

        return $path;
    }

    /*
     * Resolve Controller
     * Resolves and executes a controller.
     *
     * @param Clojure $Controller
     * @param optional array $Parameters
    */
    private function ResolveController ($Controller, $Parameters = array()) {
        $out = null;

        global $config;

        if ($_SERVER['REQUEST_METHOD'] == 'POST'
         || $_SERVER['REQUEST_METHOD'] == 'PUT'
         || $_SERVER['REQUEST_METHOD'] == 'DELETE'
         || $_SERVER['REQUEST_METHOD'] == 'PATCH') {
            if (!csrf_validate()) {
                $Panic = new Panic();
                $Panic->StatusCode = 500;
                $Panic->Name = "Cross Site Request Forgery check failed";
                echo $Panic->Show();
                return;
            }
        }

        if (!is_string($Controller)) {
            $out = $Controller();
        } else {
            $controllerinfo = explode('@', $Controller, 2);
            $class = $controllerinfo[0];
            $func = $controllerinfo[1];
            $instance = new $class();

            foreach ($this->Middlewares as $MiddlewareAddress) {
                $middlewareoutput = (new $MiddlewareAddress())->Execute($func, $instance);

                if (is_int($middlewareoutput)
                    && $middlewareoutput != 200
                    || is_string($middlewareoutput)
                    || is_array($middlewareoutput)) {
                    $out = $middlewareoutput;
                    break;
                }
            }

            if ($out == null) {
                try {
                    $out = call_user_func_array(array($instance, $func), $Parameters);
                } catch (Exception $e) {
                    if ($config['debug']) {
                        var_dump($e);
                    }

                    $out = 500;
                }
            }
        }

        if (is_int($out)) {
            $Panic = new Panic();
            $Panic->StatusCode = $out;
            $Panic->Name = Status::$Codes[$out];
            echo $Panic->Show($out !== 404 && $out !== 403);
        } else if (is_array($out)) {

            $format = 'json';

            if (array_key_exists('format', $_GET)) {
                if (empty($_GET['format'])) {
                    http_response_code(400);
                    header('Content-Type: application/json');

                    echo json_encode(array(
                        "code" => "400",
                        "data" => array(
                            "error" => "Invalid format query parameter."
                        )
                    ));

                    return;
                }

                $format = trim($_GET['format']);
            }

            http_response_code($out['response_code']);

            if ($format === 'xml') {
                header('Content-Type: application/xml');

                echo xml_encode(
                    array(
                        "code" => $out['response_code'],
                        "data" => $out['data']
                    )
                );
            } else {
                header('Content-Type: application/json');

                echo json_encode(
                    array(
                        "code" => $out['response_code'],
                        "data" => $out['data']
                    )
                );
            }
        } else {
            echo $out;
        }
    }
}