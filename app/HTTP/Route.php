<?php
/*
 * Route
 * Interface that allows you to create routes simply and without
 * playing around with the router itself.
 *
 * @package Meerkat
 * @since   0.0.1
*/
namespace Meerkat\HTTP;

class Route {
    public static $GET = 'GET';
    public static $POST = 'POST';
    public static $DELETE = 'DELETE';
    public static $PUT = 'PUT';
    public static $PATCH = 'PATCH';
    public static $OPTIONS = 'OPTIONS';

    /*
     * Sets up a GET route.
     *
     * @param $Path string
     * @param $Controller mixed
    */
    public static function GET ($Path, $Controller) {
        global $router;
        $router->Routes['GET'][$Path] = $Controller;
    }

    /*
     * Sets up a POST route.
     *
     * @param $Path string
     * @param $Controller mixed
    */
    public static function POST ($Path, $Controller) {
        global $router;
        $router->Routes['POST'][$Path] = $Controller;
    }

    /*
     * Duplicates route tree for a path.
     *
     * @param $BasePath string
     * @param $NewPath string
    */
    public static function DuplicateTree ($BasePath, $NewPath) {
        global $router;

        foreach ($router->Routes as $method_name => $tree) {
            foreach ($tree as $route => $controller) {
                if (substr($route, 0, strlen($BasePath)) == $BasePath) {
                    $router->Routes[$method_name][$NewPath . substr($route, strlen($BasePath))] = $controller;
                }
            }
        }
    }

    /*
     * Removes a route.
     *
     * @param $OldRoute string
    */
    public static function Remove ($Method, $OldRoute) {
        global $router;

        unset($router->Routes[$Method][$OldRoute]);
    }

    /*
     * Sets up a redirect on every request type to that resource.
     *
     * @param $Path string
     * @param $TargetPath string
    */
    public static function Redirect ($Path, $TargetPath) {
        global $router;

        $callback = function () use (&$TargetPath) {
            header('Location: ' . $TargetPath);
        };

        foreach ($router->Routes as $Method => $_) {
            $router->Routes[$Method][$Path] = $callback;
        }
    }
}