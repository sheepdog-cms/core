<?php
/*
 * Status Codes
 *
 * @package Meerkat
 * @since   0.0.4
*/
namespace Meerkat\HTTP;

class Status {
    public static $Codes = array(
        500 => 'Internal Server Error',
        501 => 'Not implemented',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        403 => 'Forbidden',
        404 => 'Not Found'
    );
}