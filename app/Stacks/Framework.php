<?php
/*
 * Stacks Host Layer
 * Quickly let third party code run when it needs to be ran.
 *
 * @package Meerkat
 * @since   0.0.9
*/
namespace Meerkat\Stacks;
use Symfony\Component\Yaml\Yaml;

class StacksFramework {
    public $Layers = array();
    public $Manifests = array();

    /*
     * Runs functions for specified layer.
     *
     * @param string $Layer
     * @param array $Parameters
    */
    public function Invoke ($Layer, $Output = false, $Parameters = array()) {
        if (array_key_exists($Layer, $this->Layers)) {
            $out = array();
            foreach($this->Layers[$Layer] as $instance) {
                $fout = call_user_func_array(array($instance, 'Run'), $Parameters);
                if ($Output && is_array($fout)) {
                    $out = array_merge_recursive($out, $fout);
                }
            }
            return $out;
        }
        return array();
    }

    /*
     * Registers a layer.
    */
    public function Register ($Layer, $ClassAddress) {
        
        if (!array_key_exists($Layer, $this->Layers)) {
            $this->Layers[$Layer] = array();
        }

        $this->Layers[$Layer][] = new $ClassAddress();
    }

    /*
     * Loads a stack manifest and locales.
     * You'll have to load the classes and call back LoadStackLayers.
     *
     * @param string $StackDirectory
     * @return array
    */
    public function LoadStackManifest ($StackDirectory) {
        $manifest = json_decode(file_get_contents($StackDirectory . '/stacks.json'), true);

        return $manifest;
    }

    /*
     * Loads stack layers.
     * Call this back when you have loaded the manifest
     * and all of the classes from autoload.
     *
     * @param array $StackManifest
    */
    public function LoadStack ($StackDirectory, $StackManifest) {
        $this->Manifests[$StackDirectory] = $StackManifest;

        foreach($StackManifest['Layers'] as $Layer => $ClassAddress) {
            $this->Register($Layer, $ClassAddress);
        }
    }

    /*
     * Loads stack locales.
     * Call this when you are ready.
     *
     * @return null
    */
    public function LoadStackLocale ($StackDirectory) {
        global $config;

        $config['locale'] = array_merge(
            $config['locale'],
            Yaml::parse(file_get_contents($StackDirectory . '/locale/en-US.yaml'))['meerkat']['locale']
        );

        if (file_exists($StackDirectory . '/locale/' . $config['app.lang'] . '.yaml') && $config['app.lang'] !== 'en-US') {
            $config['locale'] = array_merge(
                $config['locale'],
                Yaml::parse(file_get_contents($StackDirectory . '/locale/' . $config['app.lang'] . '.yaml'))['meerkat']['locale']
            );
        }
    }
}