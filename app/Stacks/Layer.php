<?php
/*
 * Stacks Layer
 * Layer prototype.
 *
 * @package Meerkat
 * @since   0.7.3
*/
namespace Meerkat\Stacks;

class Layer {
    /*
     * Execute layer code.
     *
     * @return boolean
    */
    public function Run ()
    {
        return false;
    }
}