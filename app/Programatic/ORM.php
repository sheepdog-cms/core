<?php
/*
 * Programatic ORM
 * Write database queries. From code!
 *
 * @package Meerkat
 * @since   0.0.5
*/
namespace Meerkat\Programatic;
use PDO;

class MeerkatProgramatic {

    public function Execute ($Query) {
        $stmt = $Query->Build();
        echo $stmt;
        die();
    }

    public function Connect ($Host, $Name, $User, $Password) {
        try {
            $this->PDO = new PDO('mysql:host=' . $Host . ';dbname=' . $Name, $User, $Password);
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
}