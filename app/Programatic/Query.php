<?php
/*
 * Programatic Query Constructor
 * Write database queries. From code!
 *
 * @package Meerkat
 * @since   0.0.5
*/
namespace Meerkat\Programatic;

class MeerkatProgramaticQuery {
    private $Selections = array();
    private $Where = "";
    private $Migration = "";
    private $Operation = "";
    private $Limit = 0;
    private $Values = array();
    private $Keys = array();
    private $Table = "";
    private $Built = false;

    public function Select ($Field) {
        $this->Selections[] = $Field;
        $this->Operation = "SELECT";
        return $this;
    }

    public function Build () {
        $stmt = $this->Operation;
        if (!$this->Built) {

        }
        return $stmt;
    }

    public function Where ($A, $Operator, $B, $And = true) {
        $where = $A . ' ' . $Operator . ' ' . $Field;
        if ($this->Where == "") {
            $this->Where = $where;
        } else {
            $this->Where = $this->Where . ' ' . ($And ? 'AND' : 'OR') .  ' ' . $where;
        }
        return $this;
    }

    public function Count ($Field) {
        $selector = 'COUNT(' . $Field . ')';
        if ($this->Selections == "") {
            $this->Selections = $selector;
        } else {
            $this->Selections = $this->Selections . ', ' . $selector;
        }
        return $this;
    }

    public function Insert ($Key, $Value) {
        $this->Keys[] = $Key;
        $this->Values[] = $Value;
        $this->Operation = "INSERT";
        return $this;
    }

    public function AsMigration ($Identifier) {

    }

    public function Delete () {

    }

    public function Update ($Fields) {

    }

    public function Limit ($Max = 1) {
        $this->Limit = $Max;
        return $this;
    }

    public function Table ($Name) {
        $this->Table = $Name;
        return $this;
    }

    public function Create ($Columns) {
        $com = "";
        foreach($Columns as $Column) {
            if ($com == "") {
                $com = $Column['Name'] . " " . $Column['DataType'];
            } else {
                $com = $com . ", " . $Column['Name'] . " " . $Column['DataType'];
            }
        }
        $this->Operation = "CREATE TABLE IF NOT EXISTS " . $this->Table . "(" . implode($Columns) . ")";
        $this->Built = true;
        return $this;
    }

    public function Column ($ColumnName, $DataType, $DefaultValue = null, $PrimaryKey = false) {
        return array(
            'Name' => $ColumnName,
            'DataType' => $DataType,
            'DefaultValue' => $DefaultValue,
            'PrimaryKey' => $PrimaryKey
        );
    }
}