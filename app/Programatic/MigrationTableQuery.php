<?php
/*
 * Migration Table Migration File
 *
 * @package Meerkat
 * @since   0.0.3
*/

return function ($pdo) {
    $stmt = $pdo->prepare("CREATE TABLE IF NOT EXISTS migrations (Identifier VARCHAR(64))");
    $stmt->execute();
};