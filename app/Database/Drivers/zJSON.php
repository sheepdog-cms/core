<?php
/*
 * Flat File Database Driver
 * Requires JSON.
 *
 * zJSON as it's the only name that
 * can be below MySQL during installation.
 *
 * Bare minimum to install Meerkat.
 *
 * @package Meerkat
 * @since   0.6.3
*/
namespace Meerkat\Database\Drivers;
use Meerkat\Database\Driver;
use Meerkat\Panic\Panic;
use PDO;
use PDOException;

class zJSON extends Driver {
    public $Order_ASCENDING = '';
    public $Order_DESCENDING = '';
    public $Order_BY = '';
    public $Order_DEFAULT = '';

    public $DataType_TEXT = 'string';
    public $DataType_INTEGER = 'int';
    public $DataType_BOOLEAN = 'boolean';
    public $DataType_GUID = 'string';

    /*
     * Represents short string with length defined
     * at runtime.
     *
     * @param $Length integer
    */
    public function DataType_CHARS ($Length) {
        return 'string';
    }

    private $DataJSON = null;

    /*
     * Loads or creates the data file.
    */
    public function __construct ($config)
    {
        if (!file_exists(__DIR__ . '/../../../' . $config['database']['path'])) {
            $this->DataJSON = array();
        } else {
            $datajson = fopen(__DIR__ . '/../../../' . $config['database']['path'], 'r');

            flock($datajson, LOCK_SH);
            $this->DataJSON = json_decode(fread($datajson, filesize(__DIR__ . '/../../../' . $config['database']['path'])), true);
            fclose($datajson);
        }
    }

    /*
     * Writes the data to the file.
    */
    private function Write ()
    {
        global $config;

        $datajson = fopen(__DIR__ . '/../../../' . $config['database']['path'], 'w');

        flock($datajson, LOCK_EX);
        fwrite($datajson, json_encode($this->DataJSON));
        fclose($datajson);
    }

    /*
     * Returns requested columns from a table.
     * Ordering and limiting are unsupported.
     *
     * @param $Columns array
     * @param $Table string
     * @param $Where array = []
     * @param $Order string = ''
     * @param $Limit integer = 10000
     * @return mixed
    */
    public function Query ($Columns,
                           $Table,
                           $Where = array(),
                           $Order = '',
                           $Limit = 10000) {
        $table = $this->DataJSON[$Table];

        $output = array();

        foreach ($table as $Row) {
            $matched = true;

            foreach ($Where as $Key => $Value) {
                $matched = $matched
                    && array_key_exists($Key, $Row)
                    && $Row[$Key] === $Value;

                if (!$matched) {
                    break;
                }
            }

            if ($matched) {
                $newrow = array();

                foreach ($Columns as $Column) {
                    if (array_key_exists($Column, $Row)) {
                        $newrow[$Column] = $Row[$Column];
                    } else {
                        $newrow[$Column] = null;
                    }
                }

                $output[] = $newrow;
            }
        }

        if (sizeof($output) !== 0) {
            if ($Limit === 1) {
                return $output[0];
            }

            return $output;
        }

        return false;
    }

    /*
     * Updates rows from table.
     *
     * @param $Columns array
     * @param $Where array
     * @param $Table string
     * @return boolean
    */
    public function Update ($Columns,
                            $Where,
                            $Table) {
        $table = $this->DataJSON[$Table];

        foreach ($table as $Id => $Row) {
            $matched = true;

            foreach ($Where as $Key => $Value) {
                $matched = $matched
                    && array_key_exists($Key, $Row)
                    && $Row[$Key] === $Value;

                if (!$matched) {
                    break;
                }
            }

            if ($matched) {
                foreach ($Columns as $Column => $NewValue) {
                    $table[$Id][$Column] = $NewValue;
                }
            }
        }

        $this->DataJSON[$Table] = $table;

        $this->Write();
        return true;
    }

    /*
     * Writes data into a table.
     *
     * @param $Data array
     * @param $Table string
     * @return boolean
    */
    public function Insert ($Data,
                            $Table) {
        $this->DataJSON[$Table][] = $Data;
        $this->Write();
        return true;
    }

    /*
     * Delete a row with provided discriminators
     * from specified table.
     *
     * Operation unsupported.
     *
     * @param $Discriminators array
     * @param $Table string
     * @return boolean
    */
    public function Delete ($Discriminators,
                            $Table) {
        return false;
    }

    /*
     * Create a table.
     *
     * @param $Table string
     * @param $Columns array
     * @return boolean
    */
    public function Create ($Table,
                            $Columns) {
        $this->DataJSON[$Table] = array();
        $this->Write();
        return true;
    }
}