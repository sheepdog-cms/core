<?php
/*
 * MySQL Database Driver
 * Requires PDO.
 *
 * @package Meerkat
 * @since   0.6.1
*/
namespace Meerkat\Database\Drivers;
use Meerkat\Database\Driver;
use Meerkat\Panic\Panic;
use PDO;
use PDOException;

global $pdo;
$pdo = null;

class MySQL extends Driver {
    public $Order_ASCENDING = 'ORDER ASC';
    public $Order_DESCENDING = 'ORDER DESC';
    public $Order_BY = 'ORDER BY';
    public $Order_DEFAULT = '';

    public $DataType_TEXT = 'TEXT';
    public $DataType_INTEGER = 'INTEGER';
    public $DataType_BOOLEAN = 'TINYINT';
    public $DataType_GUID = 'VARCHAR(38)';

    /*
     * Represents short string with length defined
     * at runtime.
     *
     * @param $Length integer
    */
    public function DataType_CHARS ($Length) {
        return 'VARCHAR(' . $Length . ')';
    }

    private $PDO = null;
    private $Debug = false;

    /*
     * Establishes database connection.
    */
    public function __construct($config)
    {
        $this->Debug = $config['debug'];

        try {
            $this->PDO = new PDO(
                'mysql:host=' . $config['database']['host'] . ';dbname=' . $config['database']['name'],
                $config['database']['user'],
                $config['database']['passwd']
            );
        } catch (PDOException $_) {
            $Panic = new Panic();
            $Panic->StatusCode = 500;
            $Panic->Name = 'Could not connect to the database.';
            die($Panic->Show());
        }

        if ($config['mysql.support_legacy']) {
            global $pdo;
            $pdo = $this->PDO;
        }
    }

    /*
     * Returns requested columns from a table.
     *
     * @param $Columns array
     * @param $Table string
     * @param $Where array = []
     * @param $Order string = ''
     * @param $Limit integer = 10000
     * @return mixed
    */
    public function Query ($Columns,
                           $Table,
                           $Where = array(),
                           $Order = '',
                           $Limit = 10000) {
        // Sanitize columns.
        foreach ($Columns as $Id => $Column) {
            $Columns[$Id] = '`' . $Column . '`';
        }

        // Build WHERE query's part.
        $Conditions = array();
        $Values = array();

        foreach ($Where as $Key => $Value) {
            $Conditions[] = '`' . $Key . '` = ?';
            $Values[] = $Value;
        }

        $WhereLen = sizeof($Where);
        $WhereStmt = $WhereLen >= 1 ? 'WHERE ' . join(' AND ', $Conditions) : '';

        // Build the query.
        $query = 'SELECT ' . join(', ', $Columns)
               . ' FROM `' . $Table . '` ' . $WhereStmt
               . ' ' . $Order . ' LIMIT ' . $Limit;

        // Prepare query.
        $stmt = $this->PDO->prepare($query);

        // Bind parameters from $Where.
        for ($i = 0; $i < $WhereLen; $i++) {
            $j = $i + 1;
            $stmt->bindParam($j, $Values[$i]);
        }

        // Execute query.
        if (!$stmt->execute()) {
            if ($this->Debug) {
                print_r($stmt->errorInfo());
            }

            return false;
        }

        if ($Limit === 1) {
            return $stmt->fetch();
        }

        return $stmt->fetchAll();
    }

    /*
     * Updates rows from table.
     *
     * @param $Columns array
     * @param $Where array
     * @param $Table string
     * @return boolean
    */
    public function Update ($Columns,
                            $Where,
                            $Table) {
        // Build WHERE query's part.
        $Conditions = array();
        $Values = array();

        foreach ($Where as $Key => $Value) {
            $Conditions[] = '`' . $Key . '` = ?';
            $Values[] = $Value;
        }

        $WhereLen = sizeof($Where);
        $WhereStmt = $WhereLen >= 1 ? 'WHERE ' . join(' AND ', $Conditions) : '';

        // Build SET query's part.
        $ColumnsA = array();
        $NewValues = array();

        foreach ($Columns as $Key => $Value) {
            $ColumnsA[] = '`' . $Key . '` = ?';
            $NewValues[] = $Value;
        }

        $ColumnsLen = sizeof($Columns);
        $SetSTMT = $ColumnsLen >= 1 ? 'SET ' . join(', ', $ColumnsA) : '';

        // Build the query.
        $query = 'UPDATE `' . $Table
            . '` ' . $SetSTMT . ' ' . $WhereStmt;

        // Prepare query.
        $stmt = $this->PDO->prepare($query);

        // Bind parameters from $Columns.
        for ($i = 0; $i < $ColumnsLen; $i++) {
            $j = $i + 1;
            $stmt->bindParam($j, $NewValues[$i]);
        }

        // Bind parameters from $Where.
        for ($i = 0; $i < $WhereLen; $i++) {
            $j = $i + $ColumnsLen + 1;
            $stmt->bindParam($j, $Values[$i]);
        }

        // Execute query.
        if (!$stmt->execute()) {
            if ($this->Debug) {
                print_r($stmt->errorInfo());
            }

            return false;
        }

        return true;
    }

    /*
     * Writes data into a table.
     *
     * @param $Data array
     * @param $Table string
     * @return boolean
    */
    public function Insert ($Data,
                            $Table) {
        // Build VALUES.
        $Columns = array();
        $Values = array();
        $ValuesA = array();

        foreach ($Data as $Key => $Value) {
            $Columns[] = '`' . $Key . '`';
            $Values[] = $Value;
            $ValuesA[] = '?';
        }

        $DataLen = sizeof($Data);

        // Build the query.
        $query = 'INSERT INTO `' . $Table . '` (' . join(', ', $Columns)
               . ') VALUES(' . join(', ', $ValuesA) . ')';

        // Prepare query.
        $stmt = $this->PDO->prepare($query);

        // Bind parameters from $Data.
        for ($i = 0; $i < $DataLen; $i++) {
            $j = $i + 1;
            $stmt->bindParam($j, $Values[$i]);
        }

        // Execute query.
        if (!$stmt->execute()) {
            if ($this->Debug) {
                print_r($stmt->errorInfo());
            }

            return false;
        }

        return true;
    }

    /*
     * Delete a row with provided discriminators
     * from specified table.
     *
     * @param $Discriminators array
     * @param $Table string
     * @return boolean
    */
    public function Delete ($Discriminators,
                            $Table) {
        // Build WHERE query's part.
        $Conditions = array();
        $Values = array();

        foreach ($Discriminators as $Key => $Value) {
            $Conditions[] = '`' . $Key . '` = ?';
            $Values[] = $Value;
        }

        $WhereLen = sizeof($Discriminators);
        $WhereStmt = $WhereLen >= 1 ? 'WHERE ' . join(' AND ', $Conditions) : '';

        // Build the query.
        $query = 'DELETE FROM `' . $Table . '` ' . $WhereStmt;

        // Prepare query.
        $stmt = $this->PDO->prepare($query);

        // Bind parameters from $Where.
        for ($i = 0; $i < $WhereLen; $i++) {
            $j = $i + 1;
            $stmt->bindParam($j, $Values[$i]);
        }

        // Execute query.
        if (!$stmt->execute()) {
            if ($this->Debug) {
                print_r($stmt->errorInfo());
            }

            return false;
        }

        return true;
    }

    /*
     * Create a table.
     *
     * @param $Table string
     * @param $Columns array
     * @return boolean
    */
    public function Create ($Table,
                            $Columns) {
        // CREATE TABLE IF NOT EXISTS `$T` (`$CN` $CT[, ...]);
        // Build COLUMNS query's part.
        $ColumnsArray = array();

        foreach ($Columns as $Name => $DataType) {
            $ColumnsArray[] = '`' . $Name . '` ' . $DataType;
        }

        $ColumnsStmt = join(', ', $ColumnsArray);

        // Build the query.
        $query = 'CREATE TABLE IF NOT EXISTS `' . $Table . '` (' . $ColumnsStmt . ')';

        // Prepare query.
        $stmt = $this->PDO->prepare($query);

        // Execute query.
        if (!$stmt->execute()) {
            if ($this->Debug) {
                print_r($stmt->errorInfo());
            }

            return false;
        }

        return true;
    }
}