<?php
/*
 * Database Driver Prototype
 * Base-class for every database driver.
 *
 * @package Meerkat
 * @since   0.6.1
*/
namespace Meerkat\Database;

class Driver {
    public $Order_ASCENDING = '';
    public $Order_DESCENDING = '';
    public $Order_DEFAULT = '';
    public $Order_BY = '';

    public $DataType_TEXT = 'string';
    public $DataType_INTEGER = 'int';
    public $DataType_BOOLEAN = 'boolean';
    public $DataType_GUID = 'string';

    /*
     * Represents short string with length defined
     * at runtime.
     *
     * @param $Length integer
    */
    public function DataType_CHARS ($Length) {
        return 'string';
    }

    /*
     * Returns requested columns from a table.
     *
     * @param $Columns array
     * @param $Table string
     * @param $Where array = []
     * @param $Order string = ''
     * @param $Limit integer = 10000
     * @return boolean
    */
    public function Query ($Columns,
                           $Table,
                           $Where = array(),
                           $Order = '',
                           $Limit = 10000) {
        return false;
    }

    /*
     * Writes data into a table.
     *
     * @param $Data array
     * @param $Table string
     * @return boolean
    */
    public function Insert ($Data,
                            $Table) {
        return false;
    }

    /*
     * Updates rows from table.
     *
     * @param $Columns array
     * @param $Where array
     * @param $Table string
     * @return boolean
    */
    public function Update ($Columns,
                            $Where,
                            $Table) {
        return false;
    }

    /*
     * Delete a row with provided discriminators
     * from specified table.
     *
     * @param $Discriminators array
     * @param $Table string
     * @return boolean
    */
    public function Delete ($Discriminators,
                            $Table) {
        return false;
    }

    /*
     * Create a table.
     *
     * @param $Table string
     * @param $Columns array
     * @return boolean
    */
    public function Create ($Table,
                            $Columns) {
        return false;
    }
}