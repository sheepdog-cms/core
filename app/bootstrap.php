<?php
/*
 * Bootstrap
 * Loads configuration and sets up basic routes, then calls the router.
 *
 * @package Meerkat
 * @since   0.0.1
*/
namespace Meerkat;
require __DIR__ . '/HTTP/autoload.php';
require __DIR__ . '/Panic/Panic.php';
require __DIR__ . '/Database/Driver.php';
require __DIR__ . '/CSRF/Functions.php';
require __DIR__ . '/Middlewares/Middleware.php';
require __DIR__ . '/Middlewares/AuthenticationMiddleware.php';
require __DIR__ . '/Stacks/Framework.php';
require __DIR__ . '/Stacks/Layer.php';
require __DIR__ . '/Views/Engines/Pug.php';
require __DIR__ . '/Views/Engines/PHP.php';
require __DIR__ . '/Views/View.php';
require __DIR__ . '/Localize/Functions.php';
require __DIR__ . '/Transliteration/Functions.php';
require __DIR__ . '/Controllers/Controller.php';
require __DIR__ . '/../vendor/autoload.php';
use Meerkat\HTTP\Router;
use Meerkat\Panic\Panic;
use Meerkat\Stacks\StacksFramework;
use Meerkat\Middlewares\AuthenticationMiddleware;

$router = new Router();
require __DIR__ . '/Routes/Web/Errors.php';

$config = include(__DIR__ . '/config.php');

if (file_exists(__DIR__ . '/../config.php')) {
    $config = array_merge(
                                $config,
                                include(__DIR__ . '/../config.php'),
                                array(
                                    'app.name' => 'Meerkat'
                                )
                            );
}

if ($config['debug']) {
    error_reporting(-1);
} else {
    error_reporting(0);
}

global $database;
$database = null;

session_start();

$stacks = new StacksFramework();

if (array_key_exists('database', $config)) {
    $database = new $config['database']['driver']($config);

    $settings = $database->Query(array(
        "Key",
        "Value"
    ), "settings");

    if ($settings === false) {
        $Panic = new Panic();
        $Panic->StatusCode = 500;
        $Panic->Name = 'Failed to pull settings from the database.';
        die($Panic->Show());
    }

    foreach ($settings as $row) {
        $config[$row['Key']] = $row['Value'];
    }

    $exts = explode(',', $config['meerkat.extensions']);

    foreach ($exts as $stackext) {
        if (strlen($stackext) == 0) continue;

        $stackdir = __DIR__ . "/../extensions/" . $stackext;
        $manifest = $stacks->LoadStackManifest($stackdir);

        if (array_key_exists('Meerkat-Outdated', $manifest['Extra'])
            && $manifest['Extra']['Meerkat-Outdated']
            && !$config['stacks.initialization.load_outdated']) {
            continue;
        }

        if (array_key_exists('Meerkat-Theming', $manifest['Extra'])
            && $config['api.theming2']){
            $config['views']['search_path'] = array_merge(
                array(
                    $stackdir . '/' . $manifest['Extra']['Meerkat-Theming']['Search'] . '/'
                ),
                $config['views']['search_path']
            );
        }

        foreach ($manifest['Autoload'] as $autoload) {
            require $stackdir . '/' . $autoload;
        }

        $stacks->LoadStack($stackdir, $manifest);
    }

    $config['locale'] = array();

    $stacks->Invoke('Meerkat\BeforeSetup');

    // Finish Stacks initialization (load locales)
    foreach ($exts as $stackext) {
        if (strlen($stackext) == 0) continue;

        $stackdir = __DIR__ . "/../extensions/" . $stackext;

        $stacks->LoadStackLocale($stackdir);
    }

    // Set up routes
    $stacks->Invoke('Meerkat\Routes\BeforeDefaults');

    require __DIR__ . '/Routes/Web/Admin.php';
    require __DIR__ . '/Routes/API/Admin.php';
    require __DIR__ . '/Routes/API/APIv2.php';
    require __DIR__ . '/Routes/API/APIv3.php';

    $stacks->Invoke('Meerkat\Routes\AfterDefaults');

    require __DIR__ . '/Routes/Web/Pages.php';

    $stacks->Invoke('Meerkat\Routes\AfterPageResolver');

    $router->RegisterMiddleware('Meerkat\Middlewares\AuthenticationMiddleware');

    $stacks->Invoke('Meerkat\Routes\RegisterMiddlewares');
} else {
    require __DIR__ . '/Routes/Web/Install.php';
}

$router->Match();