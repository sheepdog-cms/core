<?php
/*
 * Middleware Prototype
 * Base-class for every middleware.
 *
 * @package Meerkat
 * @since   0.7.4
*/
namespace Meerkat\Middlewares;
use Meerkat\Middlewares\Middleware;

class AuthenticationMiddleware extends Middleware
{
    /*
     * Authenticates the user.
     *
     * @param $Method string
     * @param $ControllerInstance Meerkat\Controllers\Controller
     * @return mixed
    */
    public function Execute($Method, &$ControllerInstance)
    {
        global $stacks;

        $ControllerInstance->IsAuthenticated = false;

        $isauthenticated_arr = $stacks->Invoke('Meerkat\Authentication', true);
        $ControllerInstance->IsAuthenticated = in_array(true, $isauthenticated_arr);

        if (in_array($Method, $ControllerInstance->RequireAuthentication)) {
            if ($ControllerInstance->HideEndpointIfNotAuthenticated
                && !$ControllerInstance->IsAuthenticated) {
                return 404;
            }

            return $ControllerInstance->IsAuthenticated ? true : $ControllerInstance->UnauthorizedPayload;
        } else {
            return true;
        }
    }
}