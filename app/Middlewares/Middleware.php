<?php
/*
 * Middleware Prototype
 * Base-class for every middleware.
 *
 * @package Meerkat
 * @since   0.7.4
*/
namespace Meerkat\Middlewares;

class Middleware
{
    /*
     * Execute the middleware's logic.
     *
     * May output integer (response code),
     * arrays (serializables), booleans (failure)
     * or strings (response).
     *
     * @param $Method string
     * @param $ControllerInstance Meerkat\Controllers\Controller
     * @return mixed
    */
    public function Execute ($Method, &$ControllerInstance) {
        return true;
    }
}