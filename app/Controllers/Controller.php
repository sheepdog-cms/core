<?php
/*
 * Controller Prototype
 * Base-class for every controller.
 *
 * @package Meerkat
 * @since   0.6.0
*/
namespace Meerkat\Controllers;

class Controller {
    /*
     * Does the controller requires valid session?
     */
    public $RequireAuthentication = array();

    /*
     * Does the controller requires valid session?
     */
    public $HideEndpointIfNotAuthenticated = false;

    /*
     * Is the user authenticated?
     */
    public $IsAuthenticated = false;

    /*
     * Endpoint permissions.
     * These should be registered if possible.
     */
    public $EndpointPermissions = array();

    /*
     * Array returned when not logged in.
    */
    public $UnauthorizedPayload = array(
        "response_code" => 401,
        "data" => array(
            "error" => "Unauthorized. Please pass valid SESSION cookie.",
            "localisation_key" => "api_unauthorized",
        )
    );

    /*
     * Returns true if user is logged in.
     *
     * @return boolean
    */
    public function HasValidSession () {
        return $this->IsAuthenticated;
    }

    /*
     * Returns error message when accessing deactivated
     * endpoint.
     *
     * The example of deactivated endpoint is list/pages
     * of version 1, where the indexing has been cut off.
     *
     * @return array
    */
    public function DeactivatedEndpoint () {
        return array(
            "response_code" => 400,
            "data" => array(
                "error" => "Requested API endpoint is unavailable.",
                "localisation_key" => "api_endpoint_deactivated"
            )
        );
    }

    /*
     * Makes us... panic?
     *
     * @return integer
    */
    public function Show () {
        return 501;
    }
}