<?php
/*
 * Page Controller
 * Basic page handler.
 *
 * @package Meerkat
 * @since   0.0.6
*/
namespace Meerkat\Controllers\Web;
use Meerkat\Controllers\Controller;
use Meerkat\Extensions\Markdown;

class PageController extends Controller
{
    /*
     * Queries database for the page.
     *
     * @param string $PageUri
     * @return mixed
    */
    public function Show ($PageUri = '')
    {
        global $config;
        global $database;

        $page = $database->Query(
            array(
                'Title',
                'Content',
                'Language'
            ),
            'pages',
            array(
                'Uri' => $PageUri,
                'Language' => $config['app.lang']
            ),
            $database->Order_DEFAULT,
            1
        );

        if (!$page) {
            return 404;
        }

        $parser = new Markdown();
        $parserresult = $parser->text($page['Content']);

        if ($parserresult['response_code'] != 200) {
            return $parserresult['response_code'];
        }

        return view('page.php', array(
            'title' => $page['Title'],
            'content' => $parserresult['data'],
            'page_lang' => $page['Language']
        ));
    }
}