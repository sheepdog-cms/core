<?php
/*
 * Admin Controller
 * /admin
 *
 * @package Meerkat
 * @since   0.0.7
*/
namespace Meerkat\Controllers\Web;
use Meerkat\Controllers\Controller;

class AdminController extends Controller {
    /*
     * Determines if user is logged in.
     *
     * @return string
    */
    public function Show ($Error = false) {
        global $config;

        $experiments = explode(' ', array_key_exists('experiment', $_GET) ? $_GET['experiment'] : $config['experiments.default']);

        return view('admin.php', array(
            'experiments' => $experiments
        ));
    }
}