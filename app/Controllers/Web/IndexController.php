<?php
/*
 * Index Controller
 * Queries database for index page.
 *
 * @package Meerkat
 * @since   0.0.6
*/
namespace Meerkat\Controllers\Web;
use Meerkat\Controllers\Controller;
use Meerkat\Extensions\Markdown;

class IndexController extends Controller {
    /*
     * Queries database for the page.
     *
     * @return mixed
    */
    public function Show () {
        global $config;
        global $database;

        $page = $database->Query(
            array(
                'Title',
                'Content',
                'Language'
            ),
            'pages',
            array(
                'IsIndex' => 1,
                'Language' => $config['app.lang']
            ),
            $database->Order_DEFAULT,
            1
        );

        if (!$page) {
            return 500;
        }

        $parser = new Markdown();
        $parserresult = $parser->text($page['Content']);

        if ($parserresult['response_code'] != 200) {
            return $parserresult['response_code'];
        }

        return view('page.php', array(
            'title' => $page['Title'],
            'content' => $parserresult['data'],
            'page_lang' => $page['Language']
        ));
    }
}