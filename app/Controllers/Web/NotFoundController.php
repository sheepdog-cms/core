<?php
/*
 * Not Found Controller
 * Basic 404 handler using Meerkat's Panic.
 *
 * @package Meerkat
 * @since   0.0.4
*/
namespace Meerkat\Controllers\Web;
use Meerkat\Controllers\Controller;

class NotFoundController extends Controller {
    /*
     * Forces router to execute Panic.
     * Of course you can write your own controller
     * which will return the appriopate status code
     * and error pages IF YOU DON'T APPRECIATE OUR HARD WORK.
     *
     * @return integer
    */
    public function Show () {
        return 404;
    }
}