<?php
/*
 * User CLI Controller
 *
 * @package Meerkat
 * @since   0.7.1
*/
namespace Meerkat\Controllers\CLI;
use Meerkat\Controllers\Controller;

class UserController extends Controller
{
    /*
     * Creates a user.
     *
     * @return array
    */
    public function CreateAUser ($_1, $_2, $_3, $Name, $Password) {
        if (empty ($Name) || empty($Password)) {
            return array(
                "response_code" => 400,
                "data" => array(
                    "error" => "name and password cannot be empty."
                )
            );
        }

        global $database;

        $exists = $database->Query(array(
            'Name'
        ), 'users', array(
            'Name' => $Name
        ));

        $hash = $Password === '!' ? '!' : password_hash($Password, PASSWORD_BCRYPT);

        $success = true;
        if ($exists) {
            $success = $database->Update(array(
                'Password' => $hash
            ), array(
                'Name' => $Name
            ), 'users');
        } else {
            $success = $database->Insert(array(
                'Name' => $Name,
                'Password' => $hash
            ), 'users');
        }

        if (!$success) {
            return array(
                "response_code" => 500,
                "data" => array()
            );
        }

        return array(
            "response_code" => 200,
            "data" => array()
        );
    }
}