<?php
/*
 * Code Signing CLI
 * Sign the MDEMs with your developer key.
 *
 * @package Meerkat
 * @since   0.7.3
*/
namespace Meerkat\Controllers\CLI;
use Meerkat\Controllers\Controller;

class CodeSigningController extends Controller
{
    /*
     * Signs the code.
     *
     * @return array
    */
    public function Sign ($_1, $_2, $_3) {
        echo 'Loading MDEM manifest...' . "\n";
        echo 'Loading your developer key...' . "\n";

        $isdeveloper = true;
        if ($isdeveloper) {
            echo "\n" . 'The developer key is invalid. While developing an extension you don\'t want '
                . 'to resign the code each time you do something so the MDEM flag is not required. '
                . 'MDEMs are meant for vendors or critical components.' . "\n\n"
                . 'Meerkat will refuse to load your MDEM until you get an official developer key. '
                . 'Please get your key signed or ask someone to publish your module.' . "\n";

            return false;
        }
    }
}