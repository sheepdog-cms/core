<?php
/*
 * Page Controller
 * Basic page handler.
 *
 * @package Meerkat
 * @since   0.0.6
*/
namespace Meerkat\Controllers\API;
use Meerkat\Controllers\Controller;

class AuthController extends Controller
{
    /*
     * Does the controller requires valid session?
     */
    public $RequireAuthentication = array(
        'Logout'
    );

    /*
     * Destroys the session.
     *
     * @return array
    */
    public function Logout () {
        if (!$this->HasValidSession()) {
            return $this->UnauthorizedPayload;
        }

        session_destroy();

        return array(
            "response_code" => 200,
            "data" => array()
        );
    }

    /*
     * Destroys the session.
     *
     * @return array
    */
    public function Login () {
        if ($this->HasValidSession()) {
            return array(
                "response_code" => 404,
                "data" => array()
            );
        }

        if (!array_key_exists("name", $_POST)
            || !array_key_exists("pass", $_POST)) {
            return array(
                "response_code" => 400,
                "data" => array(
                    'localisation_key' => 'xhr_error'
                )
            );
        }

        if (empty($_POST['name'])
            || empty($_POST['pass'])) {

            return array(
                "response_code" => 400,
                "data" => array(
                    'localisation_key' => 'xhr_error'
                )
            );
        }

        global $database;
        $user = $database->Query(
            array(
                'Password'
            ),
            'users',
            array(
                'Name' => $_POST['name']
            ),
            $database->Order_DEFAULT,
            1
        );

        if (!$user) {
            return array(
                "response_code" => 400,
                "data" => array(
                    'localisation_key' => 'xhr_error'
                )
            );
        }

        if (password_verify($_POST['pass'], $user['Password'])) {
            $_SESSION['username'] = $_POST['name'];

            return array(
                "response_code" => 200,
                "data" => array()
            );
        }

        return array(
            "response_code" => 400,
            "data" => array(
                'localisation_key' => 'xhr_error'
            )
        );
    }

    /*
     * Generates a new cross-site request
     * forgery token.
     *
     * @return array
    */
    public function CSRF ()
    {
        csrf_raw_token();

        return array(
            "response_code" => 200,
            "data" => array(
                "user" => $this->HasValidSession() ? $_SESSION['username'] : '',
                "csrf" => $_SESSION['csrf']
            )
        );
    }
}