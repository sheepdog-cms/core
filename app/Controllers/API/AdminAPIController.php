<?php
/*
 * Admin API Controller
 * /api/v1/admin
 *
 * @package Meerkat
 * @since   0.0.9
*/
namespace Meerkat\Controllers\API;

class AdminAPIController {
    /*
     * Array returned when not logged in.
    */
    private $UnauthorizedPayload = array(
        "response_code" => 401,
        "data" => array(
            "error" => "Unauthorized. Please pass valid SESSION cookie.",
            "localisation_key" => "api_unauthorized",
            "localised_message" => "await construct"
        )
    );

    /*
     * Set localised messages of static payloads.
    */
    public function __construct() {
        $this->UnauthorizedPayload['data']['localised_message'] = LC("api_unauthorized", true);
    }

    /*
     * Returns true if user is logged in.
     *
     * @return boolean
    */
    public function CheckLoggedIn () {
        return array_key_exists("username", $_SESSION);
    }

    /*
     * Returns current's site locale.
     *
     * @return array
    */
    public function ReturnLocale () {
        if (!$this->CheckLoggedIn()) {
            return $this->UnauthorizedPayload;
        }
        global $config;
        return array(
            "response_code" => 200,
            "data" => $config['locale']
        );
    }

    /*
     * Returns user name of currently logged in user who calls the function.
     *
     * @return array
    */
    public function WhoAmI () {
        if (!$this->CheckLoggedIn()) {
            return $this->UnauthorizedPayload;
        }

        csrf_raw_token();

        return array(
            "response_code" => 200,
            "data" => array(
                "user" => $_SESSION['username'],
                "csrf" => $_SESSION['csrf']
            )
        );
    }

    /*
     * Destroys the session.
     *
     * @return array
    */
    public function Logout () {
        if (!$this->CheckLoggedIn()) {
            return $this->UnauthorizedPayload;
        }
        session_destroy();
        return array(
            "response_code" => 200,
            "data" => array()
        );
    }

    /*
     * Returns all installed extensions.
     *
     * @return array
    */
    public function ListExtensions ($noindexing = false) {
        if (!$this->CheckLoggedIn()) {
            return $this->UnauthorizedPayload;
        }

        global $config;
        $output = array(
            "response_code" => 200,
            "data" => array(
                "_indexing" => array()
            )
        );
        $dirs = scandir(__DIR__ . '/../../../extensions/');
        
        $enabledexts = explode(',', $config['meerkat.extensions']);

        foreach($dirs as $dir) {
            if ($dir == '.' || $dir == '..') continue;
            $extdir = __DIR__ . '/../../../extensions/' . $dir;
            $manifest = json_decode(file_get_contents($extdir . '/stacks.json'), true);
            $output['data']['_indexing'][] = $dir;
            $extra = $manifest['Extra']['Meerkat-Extension']['en-US'];
            if (array_key_exists($config['site.lang'], $manifest['Extra']['Meerkat-Extension'])) {
                $extra = $manifest['Extra']['Meerkat-Extension'][$config['site.lang']];
            }
            $output['data'][$dir] = array(
                'enabled' => in_array($dir, $enabledexts),
                'name' => $extra['Name'],
                'description' => $extra['Description'],
                'author' => $extra['Author']
            );
        }

        if ($noindexing) {
            unset($output['data']['_indexing']);
        }

        return $output;
    }

    /*
     * Returns all users.
     *
     * @return array
    */
    public function ListUsers () {
        if (!$this->CheckLoggedIn()) {
            return $this->UnauthorizedPayload;
        }

        global $config;
        $output = array(
            "response_code" => 200,
            "data" => array(
                "_indexing" => array()
            )
        );

        global $pdo;

        $stmt = $pdo->prepare("SELECT Name FROM users");

        if (!$stmt->execute()) {
            return array(
                "response_code" => 500,
                "data" => array()
            );
        }

        $index = 0;
        while($row = $stmt->fetch()) {
            $output['data']['_indexing'][] = $index;
            $output['data'][] = array(
                "Name" => $row['Name']
            );
            $index++;
        }

        return $output;
    }

    /*
     * Returns all pages.
     *
     * @return array
    */
    public function ListPages () {
        if (!$this->CheckLoggedIn()) {
            return $this->UnauthorizedPayload;
        }

        global $config;

        $output = array(
            "response_code" => 200,
            "data" => array()
        );

        global $database;
        $pages = $database->Query(
            array(
                'Title',
                'GUID',
                'IsIndex',
                'Language',
                'Uri'
            ),
            'pages'
        );

        if (!$pages) {
            return array(
                "response_code" => 500,
                "data" => array()
            );
        }

        foreach ($pages as $page) {
            $output['data'][] = array(
                "Title" => $page['Title'],
                "Uri" => $page['Uri'],
                "IsIndex" => $page['IsIndex'],
                "Language" => $page['Language'],
                "GUID" => $page['GUID']
            );
        }

        return $output;
    }

    /*
     * Return a page's details.
     *
     * @param string $GUID
     * @return array
    */
    public function GetPage ($GUID) {
        if (!$this->CheckLoggedIn()) {
            return $this->UnauthorizedPayload;
        }

        global $config;
        $output = array(
            "response_code" => 200,
            "data" => array()
        );

        global $database;
        $page = $database->Query(
            array(
                'Title',
                'Content',
                'IsIndex',
                'Language',
                'Uri'
            ),
            'pages',
            array(
                'GUID' => $GUID
            ),
            $database->Order_DEFAULT,
            1
        );

        if (!$page) {
            return array(
                "response_code" => 500,
                "data" => array()
            );
        }

        $output['data'] = array(
            "Title" => $page['Title'],
            "Content" => $page['Content'],
            "IsIndex" => $page['IsIndex'],
            "Language" => $page['Language'],
            "Controller" => $page['Controller'],
            "View" => $page['View'],
            "URI" => $page['Uri']
        );

        return $output;
    }

    /*
     * Updates database with new values.
     *
     * @return array
    */
    public function Apply ($Key) {
        if (!$this->CheckLoggedIn()) {
            return $this->UnauthorizedPayload;
        }

        if (!array_key_exists ('value', $_POST)) {
            return array(
                "response_code" => 400,
                "data" => array(
                    "error" => "add value to your payload"
                )
            );
        }

        global $database;
        $success = $database->Update(
            array(
                'Value' => $_POST['value']
            ),
            array(
                'Key' => $Key
            ),
            'settings'
        );

        if (!$success) {
            return array(
                "response_code" => 500,
                "data" => array()
            );
        }

        return array(
            "response_code" => 200,
            "data" => array()
        );
    }

    /*
     * Updates database with extension state.
     *
     * @return array
    */
    public function SetExtensionState () {
        if (!$this->CheckLoggedIn()) {
            return $this->UnauthorizedPayload;
        }
        if (!array_key_exists ('name', $_POST)) {
            return array(
                "response_code" => 400,
                "data" => array(
                    "error" => "add name to your payload"
                )
            );
        }

        if (empty ($_POST['name'])) {
            return array(
                "response_code" => 400,
                "data" => array(
                    "error" => "name cannot be empty."
                )
            );
        }

        global $config;

        $aexts = explode(',', $config['meerkat.extensions']);
        if (($key = array_search($_POST['name'], $aexts)) !== false) {
            unset($aexts[$key]);
        } else {
            $aexts[] = $_POST['name'];
        }
        $exts = trim(implode(',', $aexts), ',');

        global $pdo;

        $stmt = $pdo->prepare("UPDATE `settings` SET `Value` = ? WHERE `Key` = 'meerkat.extensions'");
        $stmt->bindParam(1, $exts);

        if (!$stmt->execute()) {
            return array(
                "response_code" => 500,
                "data" => array()
            );
        }

        return array(
            "response_code" => 200,
            "data" => array()
        );
    }

    /*
     * Updates a page.
     *
     * @param string $GUID
     * @return array
    */
    public function SetPage ($GUID = "random") {
        if (!$this->CheckLoggedIn()) {
            return $this->UnauthorizedPayload;
        }

        if ($GUID == "random") {
            $GUID = create_guid();
        }

        if (!array_key_exists ('title', $_POST)
            || !array_key_exists ('content', $_POST)
            || !array_key_exists ('controller', $_POST)
            || !array_key_exists ('isindex', $_POST)
            || !array_key_exists ('language', $_POST)
            || !array_key_exists ('uri', $_POST)
            || !array_key_exists ('view', $_POST)) {
            return array(
                "response_code" => 400,
                "data" => array(
                    "error" => "add title, content, controller, isindex, uri, view or language to your payload"
                )
            );
        }

        if (empty ($_POST['title'])
            || empty ($_POST['content'])
            || empty ($_POST['controller'])
            || empty ($_POST['isindex'])
            || empty ($_POST['language'])
            || empty ($_POST['uri'])
            || empty ($_POST['view'])) {
            return array(
                "response_code" => 400,
                "data" => array(
                    "error" => "title, content, controller, isindex, uri, view and language cannot be empty."
                )
            );
        }

        global $pdo;

        $stmt = $pdo->prepare("SELECT `GUID` FROM pages WHERE `GUID` = ?");
        $stmt->bindParam(1, $GUID);

        if (!$stmt->execute()) {
            return array(
                "response_code" => 500,
                "data" => array()
            );
        }

        if ($row = $stmt->fetch()) {
            $stmt = $pdo->prepare("UPDATE pages SET Uri = ?, Title = ?, Content = ?, Controller = ?, IsIndex = ?, `Language` = ?, `View` = ? WHERE `GUID` = ?");
            $stmt->bindParam(1, $_POST['uri']);
            $stmt->bindParam(2, $_POST['title']);
            $stmt->bindParam(3, $_POST['content']);
            $stmt->bindParam(4, $_POST['controller']);
            $_POST['isindex'] = $_POST['isindex'] === "true";
            $stmt->bindParam(5, $_POST['isindex']);
            $stmt->bindParam(6, $_POST['language']);
            $stmt->bindParam(7, $_POST['view']);
            $stmt->bindParam(8, $GUID);
        } else {
            $stmt = $pdo->prepare("INSERT INTO pages(`Uri`, `Title`, `Content`, `Controller`, `IsIndex`, `Language`, `GUID`, `View`) VALUES(?, ?, ?, ?, ?, ?, ?, ?)");
            $stmt->bindParam(1, $_POST['uri']);
            $stmt->bindParam(2, $_POST['title']);
            $stmt->bindParam(3, $_POST['content']);
            $stmt->bindParam(4, $_POST['controller']);
            $_POST['isindex'] = $_POST['isindex'] == "true" ? 1 : 0;
            $stmt->bindParam(5, $_POST['isindex']);
            $stmt->bindParam(6, $_POST['language']);
            $stmt->bindParam(7, $GUID);
            $stmt->bindParam(8, $_POST['view']);
        }

        if (!$stmt->execute()) {
            return array(
                "response_code" => 500,
                "data" => array()
            );
        }

        return array(
            "response_code" => 200,
            "data" => array()
        );
    }

    /*
     * Creates a user.
     *
     * @return array
    */
    public function CreateAUser () {
        if (!$this->CheckLoggedIn()) {
            return $this->UnauthorizedPayload;
        }
        if (!array_key_exists ('name', $_POST) || !array_key_exists ('password', $_POST)) {
            return array(
                "response_code" => 400,
                "data" => array(
                    "error" => "add name and password to your payload"
                )
            );
        }

        if (empty ($_POST['name']) || empty($_POST['password'])) {
            return array(
                "response_code" => 400,
                "data" => array(
                    "error" => "name and password cannot be empty."
                )
            );
        }

        global $pdo;

        $stmt = $pdo->prepare("SELECT Name FROM users WHERE Name = ?");
        $stmt->bindParam(1, $_POST['name']);

        if (!$stmt->execute()) {
            return array(
                "response_code" => 500,
                "data" => array()
            );
        }
        
        $hash = password_hash($_POST['password'], PASSWORD_BCRYPT);

        if ($row = $stmt->fetch()) {
            $stmt = $pdo->prepare("UPDATE users SET Password = ? WHERE Name = ?");
            $stmt->bindParam(1, $hash);
            $stmt->bindParam(2, $_POST['name']);
        } else {
            $stmt = $pdo->prepare("INSERT INTO users VALUES(?, ?)");
            $stmt->bindParam(1, $_POST['name']);
            $stmt->bindParam(2, $hash);
        }

        if (!$stmt->execute()) {
            return array(
                "response_code" => 500,
                "data" => array()
            );
        }

        return array(
            "response_code" => 200,
            "data" => array()
        );
    }

    /*
     * Deletes a user.
     *
     * @return array
    */
    public function DeleteAUser () {
        if (!$this->CheckLoggedIn()) {
            return $this->UnauthorizedPayload;
        }
        if (!array_key_exists ('name', $_POST)) {
            return array(
                "response_code" => 400,
                "data" => array(
                    "error" => "add name to your payload"
                )
            );
        }

        if (empty ($_POST['name'])) {
            return array(
                "response_code" => 400,
                "data" => array(
                    "error" => "name cannot be empty."
                )
            );
        }

        global $pdo;

        $stmt = $pdo->prepare("DELETE FROM users WHERE Name = ?");
        $stmt->bindParam(1, $_POST['name']);

        if (!$stmt->execute()) {
            return array(
                "response_code" => 500,
                "data" => array()
            );
        }

        return array(
            "response_code" => 200,
            "data" => array()
        );
    }

    /*
     * Deletes a pages.
     *
     * @return array
    */
    public function DeleteAPage ($GUID) {
        if (!$this->CheckLoggedIn()) {
            return $this->UnauthorizedPayload;
        }

        global $pdo;

        $stmt = $pdo->prepare("DELETE FROM pages WHERE GUID = ?");
        $stmt->bindParam(1, $GUID);

        if (!$stmt->execute()) {
            return array(
                "response_code" => 500,
                "data" => array()
            );
        }

        return array(
            "response_code" => 200,
            "data" => array()
        );
    }

    /*
     * Migrates the database.
     *
     * @return array
    */
    public function Migrate () {
        if (!$this->CheckLoggedIn()) {
            return $this->UnauthorizedPayload;
        }

        global $pdo;
        global $config;

        $stmt = $pdo->prepare("SELECT Identifier FROM migrations");

        if (!$stmt->execute()) {
            return array(
                "response_code" => 500,
                "data" => array()
            );
        }

        $already_done = array();
        while ($row = $stmt->fetch()) {
            $already_done[] = $row['Identifier'];
        }

        $enabledexts = explode(',', $config['meerkat.extensions']);

        $shmigs = scandir(__DIR__ . "/../../../db/migrate/");

        foreach($shmigs as $migrate) {
            if ($migrate == '.' || $migrate == '..') continue;
            if (!in_array($migrate, $already_done)) {
                $mfunc = require __DIR__ . '/../../../db/migrate/' . $migrate;
                $mfunc($pdo);

                $stmt = $pdo->prepare("INSERT INTO migrations VALUES(?)");
                $stmt->bindParam(1, $migrate);
                if (!$stmt->execute()) {
                    return array(
                        "response_code" => 500,
                        "data" => array()
                    );
                }
                $already_done[] = $migrate;
            }
        }

        foreach($enabledexts as $dir) {
            if ($dir == '') continue;
            $extdir = __DIR__ . '/../../../extensions/' . $dir;
            $manifest = json_decode(file_get_contents($extdir . '/stacks.json'), true);

            foreach($manifest['Extra']['Meerkat-Extension']['Migrations'] as $migrate) {
                if($migrate == '') continue;
                $migrateid = $dir . "_" . $migrate;

                if (!in_array($migrateid, $already_done)) {
                    $mfunc = file_get_contents($extdir . '/db/migrate/' . $migrate);
                    $stmt = $pdo->prepare($mfunc);

                    if (!$stmt->execute()) {
                        return array(
                            "response_code" => 500,
                            "data" => array()
                        );
                    }

                    $stmt = $pdo->prepare("INSERT INTO migrations VALUES(?)");
                    $stmt->bindParam(1, $migrateid);
                    if (!$stmt->execute()) {
                        return array(
                            "response_code" => 500,
                            "data" => array()
                        );
                    }
                    $already_done[] = $migrateid;
                }
            }
        }

        return array(
            "response_code" => 200,
            "data" => array()
        );
    }

    /*
     * Returns available settings.
     *
     * @param $noindexingtable boolean
     * @return array
    */
    public function ListSettings ($noindexingtable = false) {
        if (!$this->CheckLoggedIn()) {
            return $this->UnauthorizedPayload;
        }
        global $config;
        global $stacks;

        $stacksoutput = $stacks->Invoke('Meerkat\\AdminAPI\\GenerateSettings', true);

        $output = array(
            "response_code" => 200,
            "data" => array(
                "site" => array(
                    "name" => array(
                        "type" => "text",
                        "value" => $config['site.name']
                    ),
                    "desc" => array(
                        "type" => "text",
                        "value" => $config['site.desc']
                    ),
                    "keywords" => array(
                        "type" => "text",
                        "value" => $config['site.keywords']
                    ),
                    "migrate" => array(
                        "type" => "submit",
                        "action" => "/api/v2/admin/migrate",
                        "value" => LC("migrate.button", true),
                        "text" => "migrate.button" // Keep text property for old administrator panel scripts.
                    )
                ),
                "users" => array(),
                "pages" => array(),
                "extensions" => array(
                    "manage" => array(
                        "type" => "widget",
                        "identifier" => "admin_extensions_panel",
                        "value" => "/resources/js/admin.extensions.js"
                    )
                )
            )
        );

        $output['data'] = array_merge_recursive(
            $output['data'],
            $stacksoutput
        );

        return $output;
    }

    /*
     * Returns available settings without top level indexing.
     *
     * @return array
    */
    public function ListSettingsNoIndexing () {
        return $this->ListSettings(true);
    }

    /*
     * Returns available extensions without top level indexing.
     *
     * @return array
    */
    public function ListExtensionsNoIndexing () {
        return $this->ListExtensions(true);
    }

    /*
     * Returns error message when accessing deactivated
     * endpoint.
     *
     * The example of deactivated endpoint is list/pages
     * of version 1, where the indexing has been cut off.
     *
     * @return array
    */
    public function DeactivatedEndpoint () {
        return array(
            "response_code" => 400,
            "data" => array(
                "error" => "Requested API endpoint is unavailable.",
                "localisation_key" => "api_endpoint_deactivated",
                "localised_message" => LC("api_endpoint_deactivated", true)
            )
        );
    }
}