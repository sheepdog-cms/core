<?php
/*
 * Page Controller
 * Basic page handler.
 *
 * @package Meerkat
 * @since   0.0.6
*/
namespace Meerkat\Controllers\API;
use Meerkat\Controllers\Controller;

class LocaleController extends Controller
{
    /*
     * Does the controller requires valid session?
     */
    public $RequireAuthentication = array(
        'Show'
    );

    /*
     * Returns current locale array.
     *
     * @return array
    */
    public function Show ()
    {
        global $config;

        return array(
            "response_code" => 200,
            "data" => $config['locale']
        );
    }
}