<?php
/*
 * Page Controller
 * Basic page handler.
 *
 * @package Meerkat
 * @since   0.0.6
*/
namespace Meerkat\Controllers\API;
use Meerkat\Controllers\Controller;

class MaintenanceController extends Controller
{
    /*
     * Does the controller requires valid session?
     */
    public $RequireAuthentication = array(
        'GetSettings',
        'MigrateDatabase',
        'UpdateSettings',
        'SetExtensionState',
        'GetExtensions'
    );

    /*
     * Endpoint permissions.
     * These should be registered if possible.
     */
    public $EndpointPermissions = array(
        'GetSettings' => 'maintainance',
        'MigrateDatabase' => 'maintainance',
        'UpdateSettings' => 'maintainance',
        'SetExtensionState' => 'maintainance',
        'GetExtensions' => 'maintainance'
    );

    /*
     * Returns available settings.
     *
     * @return array
    */
    public function GetSettings () {
        global $config;
        global $stacks;

        $stacksoutput = $stacks->Invoke('Meerkat\\AdminAPI\\GenerateSettings', true);

        $output = array(
            "response_code" => 200,
            "data" => array(
                "site" => array(
                    'icon' => '/resources/svg/site',
                    "name" => array(
                        "type" => "text",
                        "value" => $config['site.name']
                    ),
                    "desc" => array(
                        "type" => "text",
                        "value" => $config['site.desc']
                    ),
                    "keywords" => array(
                        "type" => "text",
                        "value" => $config['site.keywords']
                    )
                ),
                "upgrade" => array(
                    'icon' => '/resources/svg/update',
                    "migrate" => array(
                        "type" => "submit",
                        "action" => "/api/v3/maintenance/migrate_database",
                        "value" => LC("migrate.button", true),
                        "text" => "migrate.button" // Keep text property for old administrator panel scripts.
                    )
                ),
                "users" => array(
                    'icon' => '/resources/svg/users'
                ),
                "pages" => array(
                    'icon' => '/resources/svg/write',
                    "manage" => array(
                        "type" => "widget",
                        "identifier" => "__unused_pages",
                        "value" => "/resources/js/admin.pages.js"
                    )
                ),
                "extensions" => array(
                    'icon' => '/resources/svg/extensions',
                    "manage" => array(
                        "type" => "widget",
                        "identifier" => "admin_extensions_panel",
                        "value" => "/resources/js/admin.extensions.js"
                    )
                )
            )
        );

        $output['data'] = array_merge_recursive(
            $output['data'],
            $stacksoutput
        );

        return $output;
    }


    /*
     * Migrates the database.
     *
     * @return array
    */
    public function MigrateDatabase () {
        global $database;
        global $config;

        $merged_raw = $database->Query(array(
            'Identifier'
        ), 'migrations');

        if (!$merged_raw) {
            return array(
                "response_code" => 500,
                "data" => array(
                    'could not get merged migrations'
                )
            );
        }

        $merged = array();
        foreach ($merged_raw as $row) {
            $merged[] = $row['Identifier'];
        }

        $core_migrations = scandir(__DIR__ . "/../../../db/migrate/");

        foreach ($core_migrations as $migration) {
            if ($migration == '.' || $migration == '..') {
                continue;
            }

            if (!in_array($migration, $merged)) {
                $mfunc = require __DIR__ . '/../../../db/migrate/' . $migration;
                $result = $mfunc($database);

                if (!$result) {
                    return array(
                        "response_code" => 500,
                        "data" => array(
                            'migration fail: ' . $migration
                        )
                    );
                }

                if (!$database->Insert(array(
                    'Identifier' => $migration
                ), 'migrations')) {
                    return array(
                        "response_code" => 500,
                        "data" => array(
                            'migration commit fail: ' . $migration
                        )
                    );
                }

                $merged[] = $migration;
            }
        }

        $enabledexts = explode(',', $config['meerkat.extensions']);

        foreach ($enabledexts as $dir) {
            if ($dir == '') {
                continue;
            }

            $extdir = __DIR__ . '/../../../extensions/' . $dir;
            $manifest = json_decode(file_get_contents($extdir . '/stacks.json'), true);

            foreach ($manifest['Extra']['Meerkat-Extension']['Migrations'] as $migration) {
                if ($migration == '') {
                    continue;
                }

                $migration = $migration . '.php';

                $migrationid = $dir . "_" . $migration;

                if (!in_array($migrationid, $merged)) {
                    $mfunc = require $extdir . '/db/migrate/' . $migration;
                    $result = $mfunc($database);

                    if (!$result) {
                        return array(
                            "response_code" => 500,
                            "data" => array()
                        );
                    }

                    if (!$database->Insert(array(
                        'Identifier' => $migrationid
                    ), 'migrations')) {
                        return array(
                            "response_code" => 500,
                            "data" => array()
                        );
                    }

                    $merged[] = $migrationid;
                }
            }
        }

        return array(
            "response_code" => 200,
            "data" => array()
        );
    }


    /*
     * Updates database with new values.
     *
     * @return array
    */
    public function UpdateSettings ($Key) {
        if (!array_key_exists ('value', $_POST)) {
            return array(
                "response_code" => 400,
                "data" => array(
                    "error" => "add value to your payload"
                )
            );
        }

        global $database;
        $success = $database->Update(
            array(
                'Value' => $_POST['value']
            ),
            array(
                'Key' => $Key
            ),
            'settings'
        );

        if (!$success) {
            return array(
                "response_code" => 500,
                "data" => array()
            );
        }

        return array(
            "response_code" => 200,
            "data" => array()
        );
    }

    /*
     * Updates database with extension state.
     *
     * @return array
    */
    public function SetExtensionState () {
        if (!array_key_exists ('name', $_POST)) {
            return array(
                "response_code" => 400,
                "data" => array(
                    "error" => "add name to your payload"
                )
            );
        }

        if (empty ($_POST['name'])) {
            return array(
                "response_code" => 400,
                "data" => array(
                    "error" => "name cannot be empty."
                )
            );
        }

        global $config;

        $aexts = explode(',', $config['meerkat.extensions']);

        if (($key = array_search($_POST['name'], $aexts)) !== false) {
            unset($aexts[$key]);
        } else {
            $aexts[] = $_POST['name'];
        }

        $exts = trim(implode(',', $aexts), ',');

        $config['meerkat.extensions'] = $exts;

        global $database;
        $success = $database->Update(
            array(
                'Value' => $exts
            ),
            array(
                'Key' => 'meerkat.extensions'
            ),
            'settings'
        );

        if (!$success) {
            return array(
                "response_code" => 500,
                "data" => array(
                    'settings commit failed'
                )
            );
        }

        return $this->MigrateDatabase();
    }

    /*
     * Returns all installed extensions.
     *
     * @return array
    */
    public function GetExtensions () {
        global $config;
        $output = array(
            "response_code" => 200,
            "data" => array()
        );
        $dirs = scandir(__DIR__ . '/../../../extensions/');

        $enabledexts = explode(',', $config['meerkat.extensions']);
        $cems = array();

        foreach ($dirs as $dir) {
            if ($dir == '.' || $dir == '..') continue;
            $extdir = __DIR__ . '/../../../extensions/' . $dir;
            $manifest = json_decode(file_get_contents($extdir . '/stacks.json'), true);

            if (array_key_exists('Meerkat-Outdated', $manifest['Extra'])
             && !in_array($dir, $enabledexts)) {
                continue;
            }

            $extra = $manifest['Extra']['Meerkat-Extension']['en-US'];

            if (array_key_exists($config['app.lang'], $manifest['Extra']['Meerkat-Extension'])) {
                $extra = $manifest['Extra']['Meerkat-Extension'][$config['app.lang']];
            }

            $isCEM = array_key_exists('Meerkat-CEM', $manifest['Extra']);

            $extinfo = array(
                'enabled' => in_array($dir, $enabledexts),
                'name' => $extra['Name'],
                'description' => $extra['Description'],
                'author' => $extra['Author'],
                'CEM' => $isCEM,
                'Outdated' => array_key_exists('Meerkat-Outdated', $manifest['Extra']),
                'Deprecated' => array_key_exists('Meerkat-Deprecated', $manifest['Extra']),
                'IsAPI' => array_key_exists('Meerkat-API', $manifest['Extra']),
                'Experimental' => array_key_exists('Meerkat-Experimental', $manifest['Extra'])
            );

            if ($isCEM) {
                $cems[$dir] = $extinfo;
            } else {
                $output['data'][$dir] = $extinfo;
            }
        }

        $output['data'] = array_merge($output['data'], $cems);
        return $output;
    }
}