<?php
/*
 * Page Controller
 * Basic page handler.
 *
 * @package Meerkat
 * @since   0.0.6
*/
namespace Meerkat\Controllers\API;
use Meerkat\Controllers\Controller;

class UserController extends Controller
{
    /*
     * Does the controller requires valid session?
     */
    public $RequireAuthentication = array(
        'CreateAUser'
    );

    /*
     * Creates a user.
     *
     * @return array
    */
    public function CreateAUser () {
        if (!$this->HasValidSession()) {
            return $this->UnauthorizedPayload;
        }

        if (!array_key_exists ('name', $_POST) || !array_key_exists ('password', $_POST)) {
            return array(
                "response_code" => 400,
                "data" => array(
                    "error" => "add name and password to your payload"
                )
            );
        }

        if (empty ($_POST['name']) || empty($_POST['password'])) {
            return array(
                "response_code" => 400,
                "data" => array(
                    "error" => "name and password cannot be empty."
                )
            );
        }

        global $database;

        $exists = $database->Query(array(
            'Name'
        ), 'users', array(
            'Name' => $_POST['name']
        ));

        $hash = password_hash($_POST['password'], PASSWORD_BCRYPT);

        $success = true;
        if ($exists) {
            $success = $database->Update(array(
                'Password' => $hash
            ), array(
                'Name' => $_POST['name']
            ), 'users');
        } else {
            $success = $database->Insert(array(
                'Name' => $_POST['name'],
                'Password' => $hash
            ), 'users');
        }

        if (!$success) {
            return array(
                "response_code" => 500,
                "data" => array()
            );
        }

        return array(
            "response_code" => 200,
            "data" => array()
        );
    }
}