<?php
/*
 * Page Controller
 * Basic page handler.
 *
 * @package Meerkat
 * @since   0.0.6
*/
namespace Meerkat\Controllers\API;
use Meerkat\Controllers\Controller;

class PagesController extends Controller
{
    /*
     * Does the controller requires valid session?
     */
    public $RequireAuthentication = array(
        'Delete',
        'Write'
    );

    /*
     * Endpoint permissions.
     * These should be registered if possible.
     */
    public $EndpointPermissions = array(
        'Delete' => 'manage_pages',
        'Write' => 'manage_pages'
    );

    /*
     * Generates a page listing.
     *
     * @return array
    */
    public function List ()
    {
        global $database;
        $pages = $database->Query(
            array(
                'Title',
                'GUID',
                'IsIndex',
                'Language',
                'Uri'
            ),
            'pages'
        );

        if (!$pages) {
            return array(
                "response_code" => 500,
                "data" => array()
            );
        }

        $output = array(
            "response_code" => 200,
            "data" => array()
        );

        foreach ($pages as $row) {
            $output['data'][] = array(
                "Title" => $row['Title'],
                "Uri" => $row['Uri'],
                "IsIndex" => $row['IsIndex'],
                "Language" => $row['Language'],
                "GUID" => $row['GUID']
            );
        }

        return $output;
    }

    /*
     * Generates a page object (reads the page).
     *
     * @return array
    */
    public function Read ($GUID)
    {
        global $database;
        $page = $database->Query(
            array(
                'Title',
                'Content',
                'IsIndex',
                'Language',
                'Uri'
            ),
            'pages',
            array(
                'GUID' => $GUID
            ),
            $database->Order_DEFAULT,
            1
        );

        if (!$page) {
            return array(
                "response_code" => 500,
                "data" => array()
            );
        }

        $output = array(
            "response_code" => 200,
            "data" => array(
                "Title" => $page['Title'],
                "Content" => $page['Content'],
                "IsIndex" => $page['IsIndex'],
                "Language" => $page['Language'],
                "URI" => $page['Uri']
            )
        );

        return $output;
    }

    /*
     * Deletes a page.
     *
     * @return array
    */
    public function Delete ($GUID)
    {
        global $pdo;

        $stmt = $pdo->prepare("DELETE FROM pages WHERE GUID = ?");
        $stmt->bindParam(1, $GUID);

        if (!$stmt->execute()) {
            return array(
                "response_code" => 500,
                "data" => array()
            );
        }

        return array(
            "response_code" => 200,
            "data" => array()
        );
    }

    /*
     * Updates a page.
     *
     * @param string $GUID
     * @return array
    */
    public function Write ($GUID = "random")
    {
        if ($GUID === "random") {
            $GUID = create_guid();
        }

        if (!array_key_exists ('title', $_POST)
            || !array_key_exists ('content', $_POST)
            || !array_key_exists ('isindex', $_POST)
            || !array_key_exists ('language', $_POST)
            || !array_key_exists ('uri', $_POST)) {
            return array(
                "response_code" => 400,
                "data" => array(
                    "error" => "add title, content, controller, isindex, uri, view or language to your payload"
                )
            );
        }

        if (empty ($_POST['title'])
            || empty ($_POST['content'])
            || empty ($_POST['isindex'])
            || empty ($_POST['language'])
            || empty ($_POST['uri'])) {
            return array(
                "response_code" => 400,
                "data" => array(
                    "error" => "title, content, isindex, uri and language cannot be empty."
                )
            );
        }

        global $database;
        $success = false;
        global $pdo;

        $exists = $database->Query(
            array(
                'GUID'
            ),
            'pages',
            array(
                'GUID' => $GUID
            )
        );

        if ($exists) {
            $success = $database->Update(
                array(
                    'Uri' => $_POST['uri'],
                    'Title' => $_POST['title'],
                    'Content' => $_POST['content'],
                    'Language' => $_POST['language'],
                    'IsIndex' => $_POST['isindex'] == "true" ? 1 : 0
                ),
                array(
                    'GUID' => $GUID
                ),
                'pages'
            );
        } else {
            $success = $database->Insert(
                array(
                    'Uri' => $_POST['uri'],
                    'Title' => $_POST['title'],
                    'Content' => $_POST['content'],
                    'Language' => $_POST['language'],
                    'IsIndex' => $_POST['isindex'] == "true" ? 1 : 0,
                    'GUID' => $GUID
                ),
                'pages'
            );
        }

        if (!$success) {
            return array(
                "response_code" => 500,
                "data" => array()
            );
        }

        return array(
            "response_code" => 200,
            "data" => array()
        );
    }
}