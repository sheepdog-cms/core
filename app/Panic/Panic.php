<?php
/*
 * Panic
 * Displays an error message with some debugging info.
 *
 * @package Meerkat
 * @since   0.0.4
*/
namespace Meerkat\Panic;

class Panic {
    public $StatusCode = 500;
    public $Name = "500 Internal Server Error";

    public function Show ($NoDump = false) {
        global $config;

        $data = array(
                'name' => $this->Name,
                'code' => $this->StatusCode,
                'stacktrace' => debug_backtrace(),
                'server' => $_SERVER,
                'get' => $_GET,
                'post' => $_POST,
                'debug' => $config['debug']
            );
        
        if ($NoDump) {
            $config['debug'] = false;
            $data['debug'] = false;
        }

        if (!$config['debug']) {
            $data['get'] = null;
            $data['server'] = null;
            $data['post'] = null;
            $data['stacktrace'] = null;
        }

        http_response_code($this->StatusCode);

        return view('panic.pug', array(
            'panic' => $data
        ));
    }
}