#!/bin/bash
if [ ! -v APP_ROOT ]; then
  export APP_ROOT=/var/www/html
fi

if [ ! -v APP_DEBUG ]; then
  export APP_DEBUG=false
fi

if [ ! -f $APP_ROOT/config.php ]; then
  if [ ! -f $APP_ROOT/app/config.php ]; then
    echo "- Copying the source code..."
    cp -a /var/www/source/* /var/www/html/
    cp -a /var/www/source/.htaccess /var/www/html/
  fi

  if [ -v MEERKAT_ADMIN_NAME ] && [ -v MEERKAT_ADMIN_PASSWORD ] && [ -v MEERKAT_DATABASE_HOST ] && [ -v MEERKAT_DOMAIN ] && [ -v MEERKAT_DATABASE_USER ] && [ -v MEERKAT_DATABASE_PASSWORD ] && [ -v MEERKAT_DATABASE_NAME ]; then
    echo "- Writing configuration..."
    echo "<?php
require __DIR__ . '/app/Database/Drivers/MySQL.php';
return array(
    'debug' => $APP_DEBUG,
    'database' => array(
        'driver' => 'Meerkat\Database\Drivers\MySQL',
        'host' => '$MEERKAT_DATABASE_HOST',
        'user' => '$MEERKAT_DATABASE_USER',
        'passwd' => '$MEERKAT_DATABASE_PASSWORD',
        'name' => '$MEERKAT_DATABASE_NAME'
    ),
    'domain' => '$MEERKAT_DOMAIN'
);" > $APP_ROOT/config.php

    echo "- Migrating the database..."
    /usr/local/bin/php /$APP_ROOT/meerkat db:migrate

    echo "- Creating administrator account..."
    /usr/local/bin/php /$APP_ROOT/meerkat user:create "$MEERKAT_ADMIN_NAME" "$MEERKAT_ADMIN_PASSWORD"

    echo "- Creating system account..."
    /usr/local/bin/php /$APP_ROOT/meerkat user:create "system" "!"
  else
    echo "Meerkat will be not configured automatically, as there"
    echo "is no variable named MEERKAT_DOMAIN, MEERKAT_DATABASE_HOST, MEERKAT_DATABASE_USER,"
    echo "MEERKAT_DATABASE_NAME, MEERKAT_ADMIN_NAME, MEERKAT_ADMIN_PASSWORD,"
    echo "or MEERKAT_DATABASE_PASSWORD."
    echo
    echo "Please navigate to this container IP in your browser"
    echo "and manually install Meerkat."
  fi
fi

docker-php-entrypoint apache2-foreground