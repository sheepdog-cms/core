<?php
/*
 * CSRF Functions
 *
 * @package Meerkat
 * @since   0.0.8
*/
function rand_str($length = 16) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function avoid_resource_caching () {
    global $config;

    if ($config['site.enable_cache_avoiding']) {
        echo '?r&' . rand_str(16);
    }
}

function create_guid () {
    $data = rand_str(32);
    return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split($data, 4));
}

function csrf_raw_token () {
    if (!array_key_exists('csrf', $_SESSION)) {
        $_SESSION['csrf'] = substr(base64_encode(rand_str(64)), 0, 88);
    }
}

function csrf_token () {
    csrf_raw_token();

    echo '<input type="hidden" name="csrf" value="' . $_SESSION['csrf'] . '">';
}

function csrf_validate () {
    global $config;

    if ($config['csrf.enabled'] == false) {
        return true;
    }

    if (in_array($_SERVER['REQUEST_URI'], $config['csrf.ignore'])) {
        return true;
    }

    if (!array_key_exists('csrf', $_POST)) {
        return false;
    }

    $isvalid = $_POST['csrf'] == $_SESSION['csrf'];

    if ($isvalid) {
        unset($_SESSION['csrf']);
    }

    return $isvalid;
}