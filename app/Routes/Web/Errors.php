<?php
/*
 * Error routes
 * A set of routes for error pages.
 *
 * @package Meerkat
 * @since   0.0.1
*/
namespace Meerkat\Routes\API;
use Meerkat\HTTP\Route;

require __DIR__ . '/../../Controllers/Web/NotFoundController.php';
Route::GET('@404', 'Meerkat\Controllers\Web\NotFoundController@Show');