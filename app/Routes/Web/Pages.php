<?php
/*
 * Pages routes
 * A set of routes for pages.
 *
 * @package Meerkat
 * @since   0.0.4
*/
namespace Meerkat\Routes\API;
use Meerkat\HTTP\Route;

require __DIR__ . '/../../Controllers/Web/PageController.php';
require __DIR__ . '/../../Controllers/Web/IndexController.php';
Route::GET('/', 'Meerkat\Controllers\Web\IndexController@Show');
Route::GET('/:page', 'Meerkat\Controllers\Web\PageController@Show');
Route::GET('/:page/', 'Meerkat\Controllers\Web\PageController@Show');