<?php
/*
 * Admin routes
 * A set of routes for admin dashboard.
 *
 * @package Meerkat
 * @since   0.0.7
*/
namespace Meerkat\Routes\API;
use Meerkat\HTTP\Route;

require __DIR__ . '/../../Controllers/Web/AdminController.php';
Route::GET('/admin', 'Meerkat\Controllers\Web\AdminController@Show');
Route::GET('/admin/:subpath', 'Meerkat\Controllers\Web\AdminController@Show');