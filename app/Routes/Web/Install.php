<?php
/*
 * Installer routes
 * A set of routes for the installer.
 *
 * @package Meerkat
 * @since   0.0.5
*/
namespace Meerkat\Routes\Web;
use Meerkat\HTTP\Route;

require __DIR__ . '/../../Controllers/API/AuthController.php';
Route::GET('/api/v3/auth/csrf', 'Meerkat\Controllers\API\AuthController@CSRF');
Route::Redirect('/', '/install');
Route::GET('/install', function () {
    return view('install.php');
});
Route::POST('/install/db', function () {
    if (!array_key_exists('dbdriver', $_POST)
        && empty($_POST['dbdriver'])
        &&!array_key_exists('admname', $_POST)
        && empty($_POST['admname'])) {
        return array(
            "response_code" => 500,
            "data" => array()
        );
    }

    $manifest = json_decode(file_get_contents(__DIR__ . '/../../Database/Drivers/' . $_POST['dbdriver'] . '.json'), true);

    $drvload = "require __DIR__ . '/app/Database/Drivers/" . $manifest['Autoload'] . ".php';";
    $drvaddr = "Meerkat\Database\Drivers\\" . $_POST['dbdriver'];
    $dbarr = "        'driver' => '" . $drvaddr . "'";

    global $config;
    $config['database'] = array();

    global $database;

    foreach ($manifest['Configure'] as $Key => $_) {
        if (!array_key_exists($Key, $_POST)
            && empty($_POST[$Key])) {
            return array(
                "response_code" => 500,
                "data" => array()
            );
        }

        $config['database'][$Key] = $_POST[$Key];

        $dbarr .= ",\n        '" . $Key . "' => '" . addslashes($_POST[$Key]) . "'";
    }

    require __DIR__ . '/../../Database/Drivers/' . $manifest['Autoload'] . '.php';
    $database = new $drvaddr($config);

    if (!$database->Create('migrations', array(
        'Identifier' => $database->DataType_TEXT
    ))) {
        return array(
            "response_code" => 500,
            "data" => array()
        );
    }

    if (!$database->Insert(array(
        'Identifier' => 'Meerkat\MigrationTable'
    ), 'migrations')) {
        return array(
            "response_code" => 500,
            "data" => array()
        );
    }

    $_SESSION['username'] = $_POST['admname'];

    require __DIR__ . '/../../Controllers/API/MaintenanceController.php';
    $migrationresult = (new \Meerkat\Controllers\API\MaintenanceController())->MigrateDatabase();
    if ($migrationresult['response_code'] !== 200) {
        return array(
            "response_code" => 500,
            "data" => $migrationresult
        );
    }

    file_put_contents(__DIR__ . '/../../../config.php', "<?php
" . $drvload . "
return array(
    'debug' => false,
    'database' => array(
" . $dbarr . "
    ),
    'domain' => '" . $_SERVER['HTTP_HOST'] . "'
);");

    return array(
        "response_code" => 200,
        "data" => array()
    );
});
Route::Redirect('/:any', '/install');