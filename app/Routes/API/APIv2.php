<?php
/*
 * API version 2
 * A set of API routes for the updated administrator
 * dashboard.
 *
 * WARN: APIv2 is deprecated.
 *       Some endpoints might be deactivated.
 *
 * @package Meerkat
 * @since   0.4.0
*/
namespace Meerkat\Routes\API;
use Meerkat\HTTP\Route;

global $config;

if ($config['api.enable_v2']) {
    require_once __DIR__ . '/../../Controllers/API/AdminAPIController.php';

    Route::GET('/api/v2/admin/list/users', 'Meerkat\Controllers\API\AdminAPIController@ListUsers');
    Route::GET('/api/v2/admin/list/pages', 'Meerkat\Controllers\API\AdminAPIController@ListPages');
    Route::GET('/api/v2/admin/migrate', 'Meerkat\Controllers\API\AdminAPIController@DeactivatedEndpoint');
    Route::GET('/api/v2/admin/get/page/:GUID', 'Meerkat\Controllers\API\AdminAPIController@GetPage');
    Route::GET('/api/v2/admin/delete/page/:GUID', 'Meerkat\Controllers\API\AdminAPIController@DeleteAPage');
    Route::POST('/api/v2/admin/update/:key', 'Meerkat\Controllers\API\AdminAPIController@Apply');
    Route::POST('/api/v2/admin/new/user', 'Meerkat\Controllers\API\AdminAPIController@CreateAUser');
    Route::POST('/api/v2/admin/delete/user', 'Meerkat\Controllers\API\AdminAPIController@DeleteAUser');
    Route::POST('/api/v2/write-page/:GUID', 'Meerkat\Controllers\API\AdminAPIController@DeactivatedEndpoint');

    Route::GET('/api/v2/admin/list/settings', 'Meerkat\Controllers\API\AdminAPIController@ListSettingsNoIndexing');
    Route::GET('/api/v2/admin/list/extensions', 'Meerkat\Controllers\API\AdminAPIController@ListExtensionsNoIndexing');
    Route::GET('/api/v2/locale', 'Meerkat\Controllers\API\AdminAPIController@ReturnLocale');
    Route::GET('/api/v2/refresh_csrf', 'Meerkat\Controllers\API\AdminAPIController@WhoAmI');
    Route::GET('/api/v2/invalidate_session', 'Meerkat\Controllers\API\AdminAPIController@Logout');
    Route::POST('/api/v2/update_extension_state', 'Meerkat\Controllers\API\AdminAPIController@SetExtensionState');

}