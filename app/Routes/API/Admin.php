<?php
/*
 * Admin routes
 * A set of API routes for admin dashboard.
 *
 * WARN: These routes are deprecated.
 *
 * @package Meerkat
 * @since   0.0.7
*/
namespace Meerkat\Routes\API;
use Meerkat\HTTP\Route;

global $config;

if ($config['api.enable_v1']) {
    require_once __DIR__ . '/../../Controllers/API/AdminAPIController.php';

    Route::GET('/api/v1/admin/whoami', 'Meerkat\Controllers\API\AdminAPIController@WhoAmI');
    Route::GET('/api/v1/admin/locale', 'Meerkat\Controllers\API\AdminAPIController@ReturnLocale');
    Route::GET('/api/v1/admin/list/settings', 'Meerkat\Controllers\API\AdminAPIController@ListSettings');
    Route::GET('/api/v1/admin/list/extensions', 'Meerkat\Controllers\API\AdminAPIController@ListExtensions');
    Route::GET('/api/v1/admin/list/users', 'Meerkat\Controllers\API\AdminAPIController@ListUsers');
    Route::GET('/api/v1/admin/list/pages', 'Meerkat\Controllers\API\AdminAPIController@DeactivatedEndpoint');
    Route::GET('/api/v1/admin/logout', 'Meerkat\Controllers\API\AdminAPIController@Logout');
    Route::GET('/api/v1/admin/migrate', 'Meerkat\Controllers\API\AdminAPIController@DeactivatedEndpoint');
    Route::GET('/api/v1/admin/get/page/:GUID', 'Meerkat\Controllers\API\AdminAPIController@GetPage');
    Route::GET('/api/v1/admin/delete/page/:GUID', 'Meerkat\Controllers\API\AdminAPIController@DeleteAPage');
    Route::POST('/api/v1/admin/update/:key', 'Meerkat\Controllers\API\AdminAPIController@Apply');
    Route::POST('/api/v1/admin/new/user', 'Meerkat\Controllers\API\AdminAPIController@CreateAUser');
    Route::POST('/api/v1/admin/delete/user', 'Meerkat\Controllers\API\AdminAPIController@DeleteAUser');
    Route::POST('/api/v1/admin/update/page/:GUID', 'Meerkat\Controllers\API\AdminAPIController@DeactivatedEndpoint');
    Route::POST('/api/v1/admin/setextstate', 'Meerkat\Controllers\API\AdminAPIController@SetExtensionState');
}