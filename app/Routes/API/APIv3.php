<?php
/*
 * API version 3
 * Endpoints are now grouped into categories.
 * Also AUTH routes.
 *
 * Please keep POSTs and PUTs below GETs.
 *
 * @package Meerkat
 * @since   0.6.0
*/
namespace Meerkat\Routes\API;
use Meerkat\HTTP\Route;

global $config;

if ($config['api.enable_v3']) {

    // Locale API
    require __DIR__ . '/../../Controllers/API/LocaleController.php';
    Route::GET('/api/v3/locale', 'Meerkat\Controllers\API\LocaleController@Show');

    // Authentication API
    require __DIR__ . '/../../Controllers/API/AuthController.php';
    Route::GET('/api/v3/auth/csrf', 'Meerkat\Controllers\API\AuthController@CSRF');
    Route::GET('/api/v3/auth/invalidate', 'Meerkat\Controllers\API\AuthController@Logout');
    Route::POST('/api/v3/auth/login', 'Meerkat\Controllers\API\AuthController@Login');

    // Users API
    require __DIR__ . '/../../Controllers/API/UserController.php';
    Route::GET('/api/v3/users/list', 'Meerkat\Controllers\API\UserController@ListUsers');
    Route::POST('/api/v3/users/new', 'Meerkat\Controllers\API\UserController@CreateAUser');
    Route::POST('/api/v3/users/edit', 'Meerkat\Controllers\API\UserController@CreateAUser');
    Route::POST('/api/v3/users/delete', 'Meerkat\Controllers\API\UserController@DeleteAUser');

    // Pages API
    require __DIR__ . '/../../Controllers/API/PagesController.php';
    Route::GET('/api/v3/pages/list', 'Meerkat\Controllers\API\PagesController@List');
    Route::GET('/api/v3/pages/read/:GUID', 'Meerkat\Controllers\API\PagesController@Read');
    Route::GET('/api/v3/pages/delete/:GUID', 'Meerkat\Controllers\API\PagesController@Delete');
    Route::POST('/api/v3/pages/write/:GUID', 'Meerkat\Controllers\API\PagesController@Write');

    // Maintenance API
    require __DIR__ . '/../../Controllers/API/MaintenanceController.php';
    Route::GET('/api/v3/maintenance/migrate_database', 'Meerkat\Controllers\API\MaintenanceController@MigrateDatabase');
    Route::GET('/api/v3/maintenance/settings', 'Meerkat\Controllers\API\MaintenanceController@GetSettings');
    Route::POST('/api/v3/maintenance/settings/:key', 'Meerkat\Controllers\API\MaintenanceController@UpdateSettings');
    Route::GET('/api/v3/maintenance/extensions', 'Meerkat\Controllers\API\MaintenanceController@GetExtensions');
    Route::POST('/api/v3/maintenance/extensions', 'Meerkat\Controllers\API\MaintenanceController@SetExtensionState');
}