<?php
/*
 * CLI Kernel
 * Command Line Kernel.
 *
 * @package Meerkat
 * @since   0.7.1
*/
namespace Meerkat\CLI;
require __DIR__ . '/../Database/Driver.php';
require __DIR__ . '/../Controllers/Controller.php';

class Kernel {
    public $config = null;
    private $Arg1 = array(
        'db' => array(
            'migrate' => array(
                'Class' => 'Meerkat\Controllers\API\MaintenanceController@MigrateDatabase',
                'Load' => '/Controllers/API/MaintenanceController.php',
                'Argv' => false,
                'description' => 'Runs database migrations'
            )
        ),
        'user' => array(
            'create' => array(
                'Class' => 'Meerkat\Controllers\CLI\UserController@CreateAUser',
                'Load' => '/Controllers/CLI/UserController.php',
                'Argv' => true,
                'description' => 'Creates a user.'
            )
        ),
        'extensions' => array(
            'bundle' => array(
                'Class' => 'Meerkat\Controllers\CLI\CodeSigningController@Sign',
                'Load' => '/Controllers/CLI/CodeSigningController.php',
                'Argv' => true,
                'description' => 'Generates a signed bundle.'
            )
        )
    );

    public function Run () {
        global $argv;
        global $config;
        global $database;

        $config = array_merge(
            require __DIR__ . '/../config.php',
            require __DIR__ . '/../../config.php'
        );

        echo "Meerkat " . $config['app.version'] . "\n";

        $database = new $config['database']['driver']($config);

        echo "Attempting to initialize migrations table...\n";
        if (!$database->Create('migrations', array(
            'Identifier' => $database->DataType_TEXT
        ))) {
            echo "Error: Could not initialize migrations table, that is required to continue.\n";
            return 2;
        }

        if (!$database->Query(array(
            'Identifier'
        ), 'migrations')) {
            echo "Warning: migrations table is empty. Initializing as CLI-managed.\n";

            if (!$database->Insert(array(
                'Identifier' => 'Meerkat\CLIInitialization'
            ), 'migrations')) {
                echo "Error: Could not populate migrations table, that is required to continue.\n";
                return 2;
            }
        }

        $settings = $database->Query(array(
            "Key",
            "Value"
        ), "settings");

        if ($settings === false) {
            echo "Warning: Could not pull settings from the database.\n";
        } else {
            foreach ($settings as $row) {
                $config[$row['Key']] = $row['Value'];
            }
        }

        $argvb = explode(':', $argv[1]);
        $cmd = $this->Arg1[$argvb[0]][$argvb[1]];

        echo "Initializing session...\n";
        global $_SESSION;
        $_SESSION = array(
            'username' => $_SERVER['USER'] . '@localhost'
        );

        require __DIR__ . '/../' . $cmd['Load'];

        $controllerinfo = explode('@', $cmd['Class'], 2);
        $class = $controllerinfo[0];
        $func = $controllerinfo[1];
        $instance = new $class();

        $mtime = microtime(true) * 1000;

        $out = call_user_func_array(array($instance, $func), array_merge(
            array(
                'CLI' => true
            ),
            (
                $cmd['Argv'] ? $argv : array()
            )
        ));

        $mtime2 = microtime(true) * 1000;

        echo "\n";
        echo $argv[1] . " finished its execution.\n";
        $diff = $mtime2 - $mtime;
        echo "Time taken: " . round($diff, 3) . " milliseconds\n";
        var_dump($out);
    }
}