<?php
/*
 * Localisation Functions
 *
 * @package Meerkat
 * @since   0.0.8
*/
function LC ($Key, $Raw = false) {
    global $config;

    if (array_key_exists($Key, $config['locale'])) {
        if ($Raw) {
            return $config['locale'][$Key];
        }

        echo $config['locale'][$Key];
    } else {
        if ($Raw) {
            return $Key;
        }
        echo $Key;
    }
}

function meerkat_meta ($data) {
    global $config;
    global $stacks;
    ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo (array_key_exists('title', $data) && $data['title'][0] !== '%' ? $data['title'] . ' – ' : '') . $config['site.name'] ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="<?php echo $config['site.keywords'];?>">
    <meta name="description" content="<?php echo $config['site.desc'];?>">
    <link rel="canonical" href="<?php echo $config['domain'];?>">
    <?php
    $stacks->Invoke('Meerkat\Views\Meta');
}

function meerkat_html_tags ($data) {
    global $config;
    echo 'lang="' . (array_key_exists('page_lang', $data) ? $data['page_lang'] : $config['app.lang']) . '"';
}