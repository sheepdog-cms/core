<?php
return function ($database) {
    if (!$database->Insert(array(
        'Key' => 'fileupload.AllowedExtensions',
        'Value' => '*.(png|jpg|jpeg|gif|svg|webp|webm|mp3|mp4)'
    ), 'settings')) {
        return false;
    }

    return true;
};