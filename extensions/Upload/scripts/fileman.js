let curpath = '/'

xhr(`/api/v2/storage${curpath}`, (data) => {
    taiga.render({
        'head': [{
            tag: 'link',
            attributes: {
                href: '/extensions/Upload/css/fileman.css',
                rel: 'stylesheet'
            }
        }],
        '#fileman': [{
            tag: 'ul.fileman',
            datasource: data.data,
            onnewitem: (_, item) => {
                if (item.is_file) {
                    return {
                        tag: 'li.file',
                        children: [
                            {
                                tag: 'div.pad',
                                data: item.filename
                            },
                            {
                                tag: 'div.flex',
                                children: [
                                    {
                                        tag: 'button.rc',
                                        data: 'V'
                                    },
                                    {
                                        tag: 'button',
                                        data: 'D'
                                    }
                                ]
                            }
                        ]
                    }
                } else {
                    return {
                        tag: 'li.directory',
                        children: [
                            {
                                tag: 'div.pad',
                                html: true,
                                data: `${item.filename} <strong>/</strong>`
                            },
                            {
                                tag: 'div.flex',
                                children: [
                                    {
                                        tag: 'button.rc',
                                        data: 'V'
                                    },
                                    {
                                        tag: 'button',
                                        data: 'D'
                                    }
                                ]
                            }
                        ]
                    }
                }
            },
            children: [{
                tag: 'li.path',
                children: [
                    {
                        tag: 'div.pad',
                        html: true,
                        data: `<strong>Browsing</strong> /storage/uploads${curpath}`
                    },
                    {
                        tag: 'div.flex',
                        children: [
                            {
                                tag: 'button',
                                data: '<'
                            },
                            {
                                tag: 'button',
                                data: 'U'
                            }
                        ]
                    }
                ]
            }]
        }]
    })
})