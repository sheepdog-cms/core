<?php
/*
 * File Upload
 *
 * @package Meerkat\Extensions\FileUpload
 * @since   0.3.0
*/
namespace Meerkat\Extensions\FileUpload;
use Meerkat\HTTP\Route;

class RouteLayer {
	/*
	  Registers Upload API route.
	  
	  @return null
	*/
    public function Run () {
        global $config;

        if ($config['api.enable_v1']) {
            Route::POST('/api/v1/admin/fileupload', 'Meerkat\Extensions\FileUpload\UploadController@Handle');
        }

        if ($config['api.enable_v2']) {
            Route::POST('/api/v2/upload', 'Meerkat\Extensions\FileUpload\UploadController@Handle');
            Route::GET('/api/v2/storage/:path', 'Meerkat\Extensions\FileUpload\UploadController@StorageListing');
        }
    }
}