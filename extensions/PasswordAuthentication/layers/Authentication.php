<?php
/*
 * Password Authentication
 * Adds few checks so you can use standard
 * user-password authentication.
 *
 * Note that this CEM adds only an authenticator.
 * The user system is by itself deeply integrated
 * with Meerkat.
 *
 * @package Meerkat\Extensions\PasswordAuthentication
 * @since   0.7.5
*/
namespace Meerkat\Extensions\PasswordAuthentication;
use Meerkat\Stacks\Layer;

class Authentication extends Layer {
	/*
	  Performs the authentication check.
	  
	  @return mixed
	*/
    public function Run () {
        return array(array_key_exists('username', $_SESSION));
    }
}