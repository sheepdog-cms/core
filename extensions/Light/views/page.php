<?php
return function ($site, $data) {
    ?>
    <!doctype html>
    <html lang="<?php echo ''; ?>">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <title><?php echo $site['site.name'];?></title>
        <link rel="stylesheet" href="/themes/stories/css/main.css"/>
        <style>
            header{background:<?php
                    echo $site['stacks.Light.header.background-color'];
                ?>}
        </style>
        <?php meerkat_meta($data); ?>
    <body>
    <header>
        <container>
            <flex class="full-width">
                <img width="144" height="144" src="<?php echo $site['stacks.Light.header.logourl'];?>" alt="<?php echo $site['site.name'];?>"/>
            </flex>

            <flex class="full-width">
                <h1>
                    <?php echo $site['site.name'];?>
                </h1>
            </flex>

            <space-between>
                <flex>
                    <ul id="primary-tags">
                        <li>
                            <a href="/search/tag/">Engineering</a>
                        </li>
                        <li>
                            <a href="/search/tag/">Engineering</a>
                        </li>
                        <li>
                            <a href="/search/tag/">Engineering</a>
                        </li>
                    </ul>
                </flex>

                <flex>
                    <button>Follow</button>
                </flex>
            </space-between>
        </container>
    </header>
    <container>
        <article>

        </article>
    </container>
    </body>
    </html>
    <?php
};