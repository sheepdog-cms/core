<?php
/*
 * File Upload
 *
 * @package Meerkat\Extensions\FileUpload
 * @since   0.3.0
*/
namespace Meerkat\Extensions\FileUpload;

class GenerateSettingsLayer {
	/*
	  Returns settings array.
	  
	  @return array
	*/
    public function Run () {
        global $config;
		
        return array (
            "fileupload" => array(
                "FileManager" => array(
                    "type" => "widget",
                    "identifier" => "fileman",
                    "value" => "/extensions/Upload/scripts/fileman.js"
                ),
                "AllowedExtensions" => array(
                    "type" => "text",
                    "value" => $config['fileupload.AllowedExtensions']
                )
            )
        );
    }
}