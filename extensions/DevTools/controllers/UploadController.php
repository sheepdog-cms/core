<?php
/*
 * File Upload
 * Controller that will receive the file from the user and
 * write it to the disk.
 *
 * @package Meerkat\Extensions\FileUpload
 * @since   0.3.0
*/
namespace Meerkat\Extensions\FileUpload;

function getAbsoluteFilename($filename) {
    $path = [];
    foreach(explode('/', $filename) as $part) {
        if (empty($part) || $part === '.') continue;

        if ($part !== '..') {
            array_push($path, $part);
        }
        else if (count($path) > 0) {
            array_pop($path);
        } else {
            throw new \Exception('Climbing above the root is not permitted.');
        }
    }

    return join('/', $path);
}

class UploadController {
    /*
     * Array returned when not logged in.
    */
    private $UnauthorizedPayload = array(
        "response_code" => 401,
        "data" => array(
            "error" => "you must have a valid session"
        )
    );

    /*
     * Returns true if user is logged in.
     *
     * @return boolean
    */
    public function CheckLoggedIn () {
        return array_key_exists("username", $_SESSION);
    }

    /*
     * Writes the file.
     *
     * @return array
    */
    public function Handle () {
        if (!$this->CheckLoggedIn()) {
            return $this->UnauthorizedPayload;
        }

        global $stacks;
        $uploadfile = __DIR__ . '/../../../storage/uploads/' . basename($_FILES['file']['name']);

        if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
            $stacks->Invoke('Meerkat\\Extensions\\Upload\\AfterWrite', false, array(
                "Filename" => $uploadfile
            ));

            return array(
                "response_code" => 200,
                "data" => "Success."
            );
        } else {
            return array(
                "response_code" => 400,
                "data" => "Rejected."
            );
        }
    }

    /*
     * Generates the JSON for file manager.
     *
     * @return array
    */
    public function StorageListing ($Path) {
        if (!$this->CheckLoggedIn()) {
            return $this->UnauthorizedPayload;
        }

        $AbsPath = __DIR__ . '/../../../storage/uploads/' . getAbsoluteFilename($Path);

        if (!file_exists($AbsPath)) {
            return 404;
        }

        $listing = scandir($AbsPath);
        $output = array();

        foreach($listing as $file) {
            if ($file == '..' || $file == '.') continue;

            $output[] = array(
                "is_file" => !is_dir($AbsPath . '/' . $file),
                "filename" => $file
            );
        }

        return array(
            "response_code" => 200,
            "data" => $output
        );
    }
}