<?php
function setHeaders ($len) {
    header('Cache-Control: max-age=' . 60*60*24*3);
    header('Content-Length: ' . $len);
}

$_SERVER['REQUEST_URI'] = explode('?', $_SERVER['REQUEST_URI'], 2)[0];
$uri = __DIR__ . '/../../' . $_SERVER['REQUEST_URI'];

if (!file_exists($uri)) {
    http_response_code(404);
    echo '404 Not Found';
    return;
}

if (substr($_SERVER['REQUEST_URI'], -3) === '.js') {
    header('Content-Type: application/javascript');
} else if (substr($_SERVER['REQUEST_URI'], -4) === '.css') {
    header('Content-Type: text/css');
} else {
    $mime = mime_content_type($uri);
    header('Content-Type: ' . $mime);
}

if (file_exists($uri . '.gz')
    && strpos($_SERVER['HTTP_ACCEPT_ENCODING'], "gzip") !== false) {
    $out = file_get_contents($uri . '.gz');
    header('Content-Encoding: gzip');
    header('Vary: Accept-Encoding');
    setHeaders(strlen($out));
    echo $out;
    return;
}

if (file_exists($uri . '.br')
    && strpos($_SERVER['HTTP_ACCEPT_ENCODING'], "br") !== false) {
    $out = file_get_contents($uri . '.br');
    header('Content-Encoding: br');
    header('Vary: Accept-Encoding');
    setHeaders(strlen($out));
    echo $out;
    return;
}

$out = file_get_contents($uri);
setHeaders(strlen($out));
echo $out;