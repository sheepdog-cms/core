<?php
/*
 * Password Authentication
 * Adds few checks so you can use standard
 * user-password authentication.
 *
 * Note that this CEM adds only an authenticator.
 * The user system is by itself deeply integrated
 * with Meerkat.
 *
 * @package Meerkat\Extensions\PasswordAuthentication
 * @since   0.7.5
*/
namespace Meerkat\Extensions\ApplicationIntegration;
use Meerkat\Stacks\Layer;
use Meerkat\Controllers\Controller;

class ApplicationController extends Controller {
    /*
      Shows application authorization dialog.

      @return string
    */
    public function Token () {
        return view('application.php');
    }

    /*
      Performs server-side handshake
      and adds the authorization token to trusted.

      @return array
    */
    public function Exchange () {
        if (!array_key_exists('app_secret_salt', $_SESSION)
            || !array_key_exists('app_secret_pending', $_SESSION)
            || !array_key_exists('exchange_token', $_POST)) {
            return array(
                "response_code" => 400,
                "data" => array()
            );
        }

        if ($_SESSION['app_secret_pending'] !== $_POST['exchange_token']) {
            return array(
                "response_code" => 401,
                "data" => array()
            );
        }

        csrf_raw_token();

        $authtoken = "Meerkat-Authorization-Token="
            . hash('sha512', $_SESSION['app_secret_salt'])
            . substr($_SESSION['csrf'], 8, 64);
        $encodedauthtoken = base64_encode(
            $authtoken
        );

        unset($_SESSION['app_secret_salt']);
        unset($_SESSION['csrf']);
        unset($_SESSION['app_secret_pending']);

        global $database;
        $database->Insert(
            array(
                "Secret" => $authtoken,
                "User" => $_SESSION['username'],
                "Description" => "Unnamed application"
            ), 'applications'
        );

        return array(
            "response_code" => 200,
            "data" => array(
                "authtoken" => $encodedauthtoken,
                "size" => strlen($authtoken),
                "integrity" => hash('SHA512', hash('SHA512', $authtoken))
            )
        );
    }
}