function encode(obj) {
    let str = ""
    for (let key in obj) {
        if (str != "") {
            str += "&"
        }
        str += key + "=" + encodeURIComponent(obj[key])
    }
    return str
}
function spawnProgress() {
    taiga.render({
        body: [
            {
                tag: `div.progress`,
                data: LC('updating'),
                save: true
            }
        ]
    })
    return taiga.saved_object
}
function destroyProgress(pbar) {
    setTimeout(()=>{document.body.removeChild(pbar)}, 2500)
}
function progressSuccess(pbar) {
    pbar.className = 'success'
    pbar.innerText = LC('updated')
}
function progressError(pbar) {
    pbar.className = 'error'
    pbar.innerText = LC('update_error')
}
function __setup_rjs() {
    if (__meerkat_rpc) {
        let rpcaddr = 'http://localhost:19482'
    }
    xhr('/api/v3/locale', (locale_) => {
        locale = locale_.data
        taiga.render({
            container: [
                {
                    tag: 'h1',
                    data: LC('authorize_application')
                },
                {
                    tag: 'p#notice',
                    data: LC('authorization_notice'),
                    html: true
                },
                {
                    tag: 'div.buttons',
                    children: [
                        {
                            tag: 'button',
                            data: LC('authorization_cancel'),
                            on: {
                                click: () => {
                                    history.back()
                                }
                            }
                        },
                        {
                            tag: 'button.accent',
                            data: LC('authorization_ok'),
                            on: {
                                click: () => {
                                    taiga.replace('#notice', {
                                        tag: 'p',
                                        data: LC('authorization_token'),
                                        html: true
                                    })

                                    taiga.render({
                                        '.buttons': false,
                                        container: [{
                                            tag: 'input',
                                            save: true,
                                            attributes: {
                                                type: 'text',
                                                placeholder: LC('authorization_exchange'),
                                                readonly: true
                                            }
                                        }]
                                    })

                                    xhrp('/api/v3/application/exchange', (res) => {
                                        let rawtoken = atob(res.data.authtoken)
                                        let size = rawtoken.length
                                        let integrity = sha512(sha512(rawtoken))

                                        if (res.data.size === size &&
                                            res.data.integrity === integrity) {
                                            taiga.saved_object.placeholder = LC('authorization_success')
                                            taiga.saved_object.value = res.data.authtoken
                                        } else {
                                            taiga.saved_object.placeholder = LC('authorization_integrity_fail')
                                        }
                                    }, {
                                        exchange_token: __meerkat_authtoken_exchange_validation_key
                                    })
                                }
                            }
                        }
                    ]
                }
            ]
        })
        taiga.replace('.overlay', {
            tag: 'div.overlay.dismiss',
            inheritChildren: true
        })
    })
}