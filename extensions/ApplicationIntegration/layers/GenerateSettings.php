<?php
/*
 * File Upload
 *
 * @package Meerkat\Extensions\FileUpload
 * @since   0.3.0
*/
namespace Meerkat\Extensions\ApplicationIntegration;

class EmbedWidget {
	/*
	  Returns settings array.
	  
	  @return array
	*/
    public function Run () {
        return array (
            "applications" => array(
                "Manager" => array(
                    "type" => "widget",
                    "identifier" => "applicationsmanager",
                    "value" => "/extensions/ApplicationIntegration/scripts/manager.js"
                )
            )
        );
    }
}