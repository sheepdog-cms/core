<?php
/*
 * Password Authentication
 * Adds few checks so you can use standard
 * user-password authentication.
 *
 * Note that this CEM adds only an authenticator.
 * The user system is by itself deeply integrated
 * with Meerkat.
 *
 * @package Meerkat\Extensions\PasswordAuthentication
 * @since   0.7.5
*/
namespace Meerkat\Extensions\ApplicationIntegration;
use Meerkat\Stacks\Layer;

class CSRF extends Layer {
	/*
	  Validates CSRF.

	  @return null
	*/
    public function Run () {
        global $config;

        $isPOST = array_key_exists('app_secret', $_POST);

        if ($isPOST || array_key_exists('app_secret', $_GET)) {
            $config['csrf.enabled'] = false;
        }
    }
}