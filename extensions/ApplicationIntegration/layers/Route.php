<?php
/*
 * Password Authentication
 * Adds few checks so you can use standard
 * user-password authentication.
 *
 * Note that this CEM adds only an authenticator.
 * The user system is by itself deeply integrated
 * with Meerkat.
 *
 * @package Meerkat\Extensions\PasswordAuthentication
 * @since   0.7.5
*/
namespace Meerkat\Extensions\ApplicationIntegration;
use Meerkat\Stacks\Layer;
use Meerkat\HTTP\Route;

class Routes extends Layer {
	/*
	  Adds routes.
	  
	  @return null
	*/
    public function Run () {
        Route::GET('/meerkat/application', 'Meerkat\Extensions\ApplicationIntegration\ApplicationController@Token');
        Route::GET('/api/v3/application/token', 'Meerkat\Extensions\ApplicationIntegration\ApplicationController@Token');
        Route::GET('/api/v3/application/revoke', 'Meerkat\Extensions\ApplicationIntegration\ApplicationController@RevokeToken');
        Route::POST('/api/v3/application/description', 'Meerkat\Extensions\ApplicationIntegration\ApplicationController@Description');
        Route::POST('/api/v3/application/exchange', 'Meerkat\Extensions\ApplicationIntegration\ApplicationController@Exchange');
    }
}