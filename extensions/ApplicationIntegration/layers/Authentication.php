<?php
/*
 * Password Authentication
 * Adds few checks so you can use standard
 * user-password authentication.
 *
 * Note that this CEM adds only an authenticator.
 * The user system is by itself deeply integrated
 * with Meerkat.
 *
 * @package Meerkat\Extensions\PasswordAuthentication
 * @since   0.7.5
*/
namespace Meerkat\Extensions\ApplicationIntegration;
use Meerkat\Stacks\Layer;

class Authentication extends Layer {
	/*
	  Performs the authentication check.
	  
	  @return array
	*/
    public function Run () {
        $isPOST = array_key_exists('app_secret', $_POST);

        if (!$isPOST && !array_key_exists('app_secret', $_GET)) {
            return false;
        }

        $token = $isPOST ? $_POST['app_secret'] : $_GET['app_secret'];

        $token = base64_decode($token);

        global $database;

        $query = $database->Query(
            array(
                "User"
            ),
            "applications",
            array(
                "Secret" => $token
            ),
            $database->Order_DEFAULT,
            1
        );

        $isapp = false;
        if ($query) {
            $isapp = true;
        } else {
            return array(false);
        }

        if ($isapp) {
            session_destroy();
            $_SESSION = array(
                'username' => $query['User'],
                'csrf' => 'no maze for ya'
            );
        }

        return array($isapp);
    }
}