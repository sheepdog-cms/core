<?php
return function ($database) {
    if (!$database->Create('applications', array(
        'Secret' => $database->DataType_TEXT,
        'User' => $database->DataType_TEXT,
        'Description' => $database->DataType_TEXT
    ))) {
        return false;
    }

    return true;
};