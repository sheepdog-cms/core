<?php
/*
 * Network Manager
 * The main framework code.
 *
 * @package Meerkat\Extensions\Networks
 * @since   0.4.2
*/
namespace Meerkat\Extensions\Networks;
use Meerkat\Panic\Panic;

class NetworkManager {
    public $isRootSite = false;
    public $SiteID = '';

    /*
     * Array returned when not logged in.
    */
    private $UnauthorizedPayload = array(
        "response_code" => 401,
        "data" => array(
            "error" => "you must have a valid session"
        )
    );

    /*
     * Returns true if user is logged in.
     *
     * @return boolean
    */
    public function CheckLoggedIn () {
        return array_key_exists("username", $_SESSION);
    }

    /*
     * Detects site and loads the settings.
     *
     * @return null
    */
    public function Run () {
        global $config;
        global $pdo;

        $this->SiteID = $_SERVER['HTTP_HOST'];
        $this->isRootSite = $config['domain'] === $this->SiteID;

        if (!$this->isRootSite) {
            $_config = include(__DIR__ . '/../../app/config.php');

            $stmt = $pdo->prepare("SELECT `Key`, `Value` FROM network_site_settings WHERE `ID` = ?");
            $stmt->bindParam(1, $this->SiteID);

            if (!$stmt->Execute()) {
                $Panic = new Panic();
                $Panic->StatusCode = 500;
                $Panic->Name = 'Could not connect to the database.';
                die($Panic->Show());
            }

            while ($row = $stmt->fetch()) {
                $_config[$row['Key']] = $row['Value'];
            }

            if (!array_key_exists('in_network', $_config)) {
                $Panic = new Panic();
                $Panic->StatusCode = 404;
                $Panic->Name = 'Site Not Found';
                die($Panic->Show());
            }

            $_config['locale'] = $config['locale'];

            $config = $_config;
        }
    }

    /*
     * A null API response.
     *
     * @return array
    */
    public function NULL_API_REQUEST () {
        return array(
            "response_code" => 200,
            "data" => array()
        );
    }
}

$networkmanager = new NetworkManager();