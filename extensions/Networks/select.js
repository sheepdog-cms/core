xhr('/api/v2/mouse/themes', (s) => {
    taiga.render({
        '#mouse_select_theme': [
            {
                tag: 'div.grid',
                datasource: s.data,
                onnewitem: (row, data) => {
                    attributes = {
                        type: 'checkbox'
                    }

                    if (data.enabled) {
                        attributes.checked = true
                    }

                    return {
                        tag: 'div',
                        children: [
                            {
                                tag: 'input',
                                attributes: attributes,
                                on: {
                                    change: (view) => {
                                        let pbar = spawnProgress()
                                        xhr('/api/v2/refresh_csrf', (a) => {
                                            let http = new XMLHttpRequest()
                                            http.open("POST", `/api/v2/admin/update/mouse.theme`, true)
                                            http.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
                                            http.onreadystatechange = () => {
                                                if (http.readyState === 4) {
                                                    if (http.status === 200 && JSON.parse(http.responseText).code === 200) {
                                                        progressSuccess(pbar)
                                                        destroyProgress(pbar)
                                                    } else {
                                                        progressError(pbar)
                                                        destroyProgress(pbar)
                                                    }
                                                }
                                            }
                                            http.send(encode({
                                                "value": view.checked ? row : '',
                                                "csrf": a.data.csrf
                                            }))
                                        })
                                    }
                                }
                            },
                            {
                                tag: 'strong',
                                data: data.name
                            },
                            {
                                tag: 'p',
                                data: data.description
                            }
                        ]
                    }
                }
            }
        ]
    })
})