<?php
/*
 * Network Stacks Compatibility Layer
 * This is only a Stacks-compatible binding.
 *
 * @package Meerkat\Extensions\Networks
 * @since   0.4.2
*/
namespace Meerkat\Extensions\Networks;

class StacksCompat {

    /*
     * Apply site-specific settings root.
     *
     * @return null
    */
    public function Run () {
        global $networkmanager;

        $networkmanager->Run();
    }

}