<?php
/*
 * Mouse Framework
 * Home routes
 *
 * @package Meerkat\Extensions\MouseFramework
 * @since   0.4.0
*/
namespace Meerkat\Extensions\Networks;
use Meerkat\HTTP\Route;

class RouteLayer {
    /*
     * Registers /network endpoint and blocks
     * sensitive-ones for non-root sites.
     *
     * @return null
    */
    public function Run () {
        global $networkmanager;

        Route::GET('/api/v2/networks', 'Meerkat\Extensions\Networks\NetworkManager@List');

        if (!$networkmanager->isRootSite) {
            Route::GET('/api/v2/admin/list/pages', 'Meerkat\Extensions\Networks\NetworkManager@ListSitePages');
            Route::GET('/api/v2/admin/list/users', 'Meerkat\Extensions\Networks\NetworkManager@ListSiteUsers');
            Route::GET('/api/v2/admin/delete/page/:GUID', 'Meerkat\Extensions\Networks\NetworkManager@DeleteAPage');
            Route::POST('/api/v2/admin/update/:key', 'Meerkat\Extensions\Networks\NetworkManager@Apply');
            Route::POST('/api/v2/admin/new/user', 'Meerkat\Extensions\Networks\NetworkManager@CreateAUser');
            Route::POST('/api/v2/admin/delete/user', 'Meerkat\Extensions\Networks\NetworkManager@DeleteAUser');
            Route::POST('/api/v2/admin/update/page/:GUID', 'Meerkat\Extensions\Networks\NetworkManager@SetPage');
            Route::GET('/api/v2/admin/list/extensions', 'Meerkat\Extensions\Networks\NetworkManager@NULL_API_REQUEST');
            Route::POST('/api/v2/admin/update_extension_state', 'Meerkat\Extensions\Networks\NetworkManager@NULL_API_REQUEST');
        }
    }
}