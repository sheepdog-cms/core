CREATE TABLE IF NOT EXISTS network_sites (`Domain` TEXT);
CREATE TABLE IF NOT EXISTS network_permissions (`Domain` TEXT, `User` TEXT);
CREATE TABLE IF NOT EXISTS network_site_settings (`ID` TEXT, `Key` TEXT, `Value` TEXT);