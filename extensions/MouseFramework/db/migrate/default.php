<?php
return function ($database) {
    if (!$database->Insert(array(
        'Key' => 'mouse.theme',
        'Value' => ''
    ), 'settings')) {
        return false;
    }

    return true;
};