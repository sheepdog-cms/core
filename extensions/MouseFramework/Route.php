<?php
/*
 * Mouse Framework
 * Home routes
 *
 * @package Meerkat\Extensions\MouseFramework
 * @since   0.4.0
*/
namespace Meerkat\Extensions\MouseFramework;
use Meerkat\HTTP\Route;
use Meerkat\Stacks\Layer;

class HomeRoutes extends Layer {
    /*
      Registers home routes.

      @return null
    */
    public function Run () {
        global $mouse;

        if ($mouse->current_theme !== '') {
            Route::GET('/', 'Meerkat\Extensions\MouseFramework\HomeController@HandleHome');
            Route::GET('/home', 'Meerkat\Extensions\MouseFramework\HomeController@HandleHome');
            Route::GET('/index', 'Meerkat\Extensions\MouseFramework\HomeController@HandleHome');
            Route::GET('/index.html', 'Meerkat\Extensions\MouseFramework\HomeController@HandleHome');
            Route::GET('/index.php', 'Meerkat\Extensions\MouseFramework\HomeController@HandleHome');
            Route::GET('/home.php', 'Meerkat\Extensions\MouseFramework\HomeController@HandleHome');
        }
    }
}