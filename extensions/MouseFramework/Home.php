<?php
/*
 * Mouse Framework
 * home.php controller.
 *
 * @package Meerkat\Extensions\MouseFramework
 * @since   0.7.4
*/
namespace Meerkat\Extensions\MouseFramework;
use Meerkat\Controllers\Controller;

class HomeController extends Controller {
    /*
     * Returns home page generated code.
     *
     * @return mixed
    */
    public function HandleHome () {
        return view('home.php');
    }
}