<?php
/*
 * Mouse Framework
 * This is only a compatibility class so
 * the theme data don't go into void.
 *
 * @package Meerkat\Extensions\MouseFramework
 * @since   0.4.0
*/
namespace Meerkat\Extensions\MouseFramework;
use Meerkat\Stacks\Layer;

class MouseStacksCompat extends Layer {

    /*
     * Loads installed theme.
     *
     * @return null
    */
    public function Run () {
        global $mouse;

        $mouse->Run();
    }

}