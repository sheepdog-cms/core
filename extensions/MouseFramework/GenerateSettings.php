<?php
/*
 * Mouse Framework
 *
 * @package Meerkat\Extensions\MouseFramework
 * @since   0.4.0
*/
namespace Meerkat\Extensions\MouseFramework;
use Meerkat\Stacks\Layer;

class GenerateSettingsLayer extends Layer {

    /*
      Returns settings array.

      @return array
    */
    public function Run () {
        global $mouse;
        global $config;
        global $database;

        $settings = $mouse->manifest['Settings'];
        $output = array();

        foreach ($settings as $group => $keys) {
            $groupformat = 'mouse.theme.' . $mouse->current_theme . '.' . $group;
            $output[$groupformat] = array();

            foreach($keys as $key => $data) {
                $fullformat = $groupformat . '.' . $key;

                if (array_key_exists($fullformat, $config)) {
                    $data['value'] = $config[$fullformat];

                    $output[$groupformat][$key] = $data;
                } else {
                    if (!$database->Insert(array(
                        'Key' => $fullformat,
                        'Value' => $data['value']
                    ), 'settings')) {
                        return array(
                            "response_code" => 500,
                            "data" => "mousecore: on-demand migration fail"
                        );
                    }

                    $output[$groupformat][$key] = $data;
                }
            }
        }

        return array_merge(
            array (
                "mouse_begin" => array(
                    "handler" => "header"
                ),
                "mouse" => array(
                    "manage" => array(
                        "type" => "widget",
                        "identifier" => "mouse_select_theme",
                        "value" => "/extensions/MouseFramework/select.js"
                    )
                )
            ),
            $output
        );
    }
}