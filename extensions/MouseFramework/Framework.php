<?php
/*
 * Mouse Framework
 * The main framework code.
 *
 * Even if this is a Layer class,
 * this is not compatible with Stacks.
 *
 * @package Meerkat\Extensions\MouseFramework
 * @since   0.4.0
*/
namespace Meerkat\Extensions\MouseFramework;
use Meerkat\HTTP\Route;
use Meerkat\Stacks\Layer;

class MouseCore extends Layer {
    public $Settings = array();
    public $current_theme = '';
    public $manifest = array(
        "Settings" => array()
    );

    /*
     * Does the controller requires valid session?
     */
    public $RequireAuthentication = array(
        'List'
    );

    /*
     * Does the controller requires valid session?
     */
    public $HideEndpointIfNotAuthenticated = false;

    /*
     * Is the user authenticated?
     */
    public $IsAuthenticated = false;

    /*
     * Endpoint permissions.
     * These should be registered if possible.
     */
    public $EndpointPermissions = array(
        'List' => 'maintainance'
    );

    /*
     * Array returned when not logged in.
    */
    public $UnauthorizedPayload = array(
        "response_code" => 401,
        "data" => array(
            "error" => "Unauthorized. Please pass valid SESSION cookie.",
            "localisation_key" => "api_unauthorized",
        )
    );

    /*
     * Registers a view search path.
     *
     * @return null
    */
    public function RegisterSearchPath ($Path) {
        global $config;

        $config['views']['search_path'] = array_merge(
            array(
                __DIR__ . '/../../' . $Path
            ),
            $config['views']['search_path']
        );
    }

    /*
     * Registers API routes and loads installed theme.
     *
     * @return null
    */
    public function Run () {
        global $config;

        Route::GET('/api/v2/mouse/themes', 'Meerkat\Extensions\MouseFramework\MouseCore@List');
        Route::GET('/api/v3/themes', 'Meerkat\Extensions\MouseFramework\MouseCore@List');

        if (!array_key_exists('mouse.theme', $config)) {
            return;
        }

        if (empty($config['mouse.theme'])) {
            return;
        }

        if (!file_exists(__DIR__ . '/../../themes/' . $config['mouse.theme'] . '/mouse.json')) {
            return;
        }

        $this->current_theme = $config['mouse.theme'];

        $this->manifest = json_decode(file_get_contents(__DIR__ . '/../../themes/' . $this->current_theme . '/mouse.json'), true);

        $this->LoadLocale($this->manifest['en-US']['Locale']);

        if (array_key_exists($config['app.lang'], $this->manifest)) {
            $this->LoadLocale($this->manifest[$config['app.lang']]['Locale']);
        }

        $this->RegisterSearchPath('/themes/' . $this->current_theme . '/');
    }

    /*
     * Loads locale.
     *
     * @param array LocaleArray
    */
    public function LoadLocale ($LocaleArray) {
        global $config;

        foreach ($LocaleArray as $key => $value) {
            $config['locale']['mouse.theme.' . $this->current_theme . '.' . $key] = $value;
        }
    }

    /*
     * Returns a theme setting.
     *
     * @return mixed
    */
    public function GetThemeSetting ($Group, $Key) {
        global $config;

        $format = 'mouse.theme.' . $this->current_theme . '.' . $Group . '.' . $Key;

        if (array_key_exists($format, $config)) {
            return $config[$format];
        } else {
            return $this->manifest['Settings'][$Group][$Key]['value'];
        }
    }

    /*
     * Forces new execution calls if PHP is configured
     * as a view engine.
     *
     * @return null
    */
    public function PHP_FORCE_NON_BUNDLE_CALLS () {
        global $config;

        if (array_key_exists('php', $config['views']['types'])) {
            // PHP view engine is registered.

            $config['views']['types']['php']['force_new_execution_calls'] = true;
        }
    }

    /*
     * Lists installed theme.
     *
     * @return array
    */
    public function List () {
        global $config;

        $output = array();

        $dirs = scandir(__DIR__ . '/../../themes/');

        foreach($dirs as $dir) {
            if ($dir == '.' || $dir == '..') continue;
            $extdir = __DIR__ . '/../../themes/' . $dir . '/';
            $manifest = json_decode(file_get_contents($extdir . '/mouse.json'), true);
            $extra = $manifest['en-US'];

            if (array_key_exists($config['app.lang'], $manifest)) {
                $extra = array_merge($extra, $manifest[$config['app.lang']]);
            }

            $output[$dir] = array(
                'enabled' => $dir === $config['mouse.theme'],
                'name' => $extra['Name'],
                'description' => $extra['Description']
            );
        }

        return array(
            "response_code" => 200,
            "data" => $output
        );
    }
}

$mouse = new MouseCore();