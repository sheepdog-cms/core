<?php
/*
 * Purge Default Locale
 *
 * @package Meerkat\Extensions\DisableEnglish
 * @deprecated
 * @since   0.6.0
*/
namespace Meerkat\Extensions\DisableEnglish;

class Register {
    /*
      Deletes all locale settings.

      @return null
    */
    public function Run () {
        global $config;

        $config['locale'] = array();
    }
}
