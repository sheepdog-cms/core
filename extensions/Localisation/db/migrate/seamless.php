<?php
return function ($database) {
    if (!$database->Insert(array(
        'Key' => 'localisation.seamless',
        'Value' => 'false'
    ), 'settings')) {
        return false;
    }

    return true;
};