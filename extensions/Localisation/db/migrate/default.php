<?php
return function ($database) {
    if (!$database->Insert(array(
        'Key' => 'localisation.languages',
        'Value' => 'en-US'
    ), 'settings')) {
        return false;
    }

    return true;
};