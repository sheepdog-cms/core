<?php
/*
 * Localisation
 *
 * @package Meerkat\Extensions\Localisation
 * @since   0.7.0
*/
namespace Meerkat\Extensions\Localisation;

class GenerateSettingsLayer {

    /*
      Returns settings array.

      @return array
    */
    public function Run ()
    {
        global $config;

        return array(
            "localisation_begin" => array(
                "handler" => "header"
            ),
            "localisation" => array(
                "languages" => array(
                    "type" => "text",
                    "value" => $config['localisation.languages']
                ),
                "seamless" => array(
                    "type" => "checkbox",
                    "value" => $config['localisation.seamless']
                )
            )
        );
    }
}