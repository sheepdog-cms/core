<?php
/*
 * Localisation
 * Generate meta tags.
 *
 * @package Meerkat\Extensions\Localisation
 * @since   0.7.1
*/
namespace Meerkat\Extensions\Localisation;

class MetaTags {
    /*
     * Parsers the URI for language
     * and changes the application language.
     *
     * @layer Meerkat\Views\Meta
     * @return null
    */
    public function Run () {
        global $config;

        $langs = explode(', ', $config['localisation.languages']);

        foreach ($langs as $lang) {
            if ($lang === $config['app.lang']) {
                continue;
            }

            ?>
            <link rel="alternate" hreflang="<?php echo substr($lang, 0, 2); ?>" href="//<?php echo $_SERVER['SERVER_NAME'];?>/<?php echo $lang . $_SERVER['REQUEST_URI'];?>" />
            <?php
        }
    }
}