<?php
/*
 * Localisation
 * Middleware that detects the language.
 *
 * @package Meerkat\Extensions\Localisation
 * @since   0.7.0
*/
namespace Meerkat\Extensions\Localisation;

class Middleware {
    /*
     * Parsers the URI for language
     * and changes the application language.
     *
     * @layer Meerkat\BeforeSetup
     * @return null
    */
    public function Run () {
        global $config;

        $langs = explode(', ', $config['localisation.languages']);
        $breakdown = explode('/', $_SERVER['REQUEST_URI']);

        if (strlen($breakdown[1]) === 2) {
            $breakdown[1] = $breakdown[1] . '-' . strtoupper($breakdown[1]);
        }

        if (in_array($breakdown[1], $langs)) {
            $config['app.lang'] = $breakdown[1];

            // Join the URI with slashes and set
            // as REQUEST_URI variable.
            unset($breakdown[1]);
            $_SERVER['REQUEST_URI'] = join('/', $breakdown);
        } else {
            $browser = explode(';', $_SERVER['HTTP_ACCEPT_LANGUAGE'])[0];
            $browser = explode(',', $browser);

            foreach ($browser as $langcode) {
                if (strlen($langcode) == 2) {
                    $langcode = $langcode . '-' . strtoupper($langcode);
                }

                if (in_array($langcode, $langs)) {
                    $config['app.lang'] = $langcode;
                    return;
                }
            }
        }
    }
}