<?php
return function ($database) {
    if (!$database->Insert(array(
        'User' => $_SESSION['username'],
        'Permissions' => 0x2376
    ), 'permissions')) {
        return false;
    }

    return true;
};