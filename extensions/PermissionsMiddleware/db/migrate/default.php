<?php
return function ($database) {
    if (!$database->Create('permissions', array(
        'User' => $database->DataType_TEXT,
        'Permissions' => $database->DataType_INTEGER
    ))) {
        return false;
    }

    return true;
};