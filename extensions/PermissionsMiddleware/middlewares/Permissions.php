<?php
namespace Meerkat\Extensions\PermissionsMiddleware;
use Meerkat\Middlewares\Middleware;

class PMiddleware extends Middleware {
    /*
      Checks the permissions.

      @return int
    */
    public function Execute ($Method, &$ControllerInstance) {
        global $database;

        if ($ControllerInstance->IsAuthenticated) {
            $query = $database->Query(
                array(
                    "Permissions"
                ),
                "permissions",
                array(
                    'User' => $_SESSION['username']
                ),
                $database->Order_DEFAULT,
                1
            );

            if (!$query) {
                $_SESSION['permissions'] = 1;
            } else {
                $_SESSION['permissions'] = $query['Permissions'];
            }
        } else {
            $_SESSION['permissions'] = 1;
            return 200;
        }

        global $_intpearr;
        if (array_key_exists($Method, $ControllerInstance->EndpointPermissions)) {
            $permission = $ControllerInstance->EndpointPermissions[$Method];

            if (($_SESSION['permissions'] | $_intpearr['administrator']) == $_intpearr['administrator']
                || ($_SESSION['permissions'] | $_intpearr[$permission]) == $_intpearr[$permission]) {
                return 200;
            } else {
                $ControllerInstance->IsAuthenticated = false;

                if (in_array($Method, $ControllerInstance->RequireAuthentication)) {
                    if ($ControllerInstance->HideEndpointIfNotAuthenticated
                        && !$ControllerInstance->IsAuthenticated) {
                        return 404;
                    }

                    return $ControllerInstance->UnauthorizedPayload;
                } else {
                    return 200;
                }
            }
        }
    }
}