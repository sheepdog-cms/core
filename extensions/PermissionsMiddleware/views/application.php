<?php
return function ($site, $data) {
    csrf_raw_token();

    if (!array_key_exists('app_secret_salt', $_SESSION)) {
        $_SESSION['app_secret_salt'] = rand_str(32);
    }

    $_SESSION['app_secret_pending'] = 'Meerkat-Authorization-Key='
        . substr(strtoupper(
                hash('sha256', $_SESSION['app_secret_salt'])
                    . $_SESSION['csrf']
        ), 0, 128);
    ?>
    <html lang="<?php echo $site['app.lang']; ?>">
    <head>
        <meta charset="utf-8">
        <?php meerkat_meta(array(
            'title' => LC('authorize-application', true)
        )); ?>
        <link rel="stylesheet" href="/extensions/ApplicationIntegration/css/application.css"/>
    </head>
    <body>
        <container></container>
        <script src="/resources/js/taiga.js<?php avoid_resource_caching();?>"></script>
        <script src="/extensions/ApplicationIntegration/js/sha512.js<?php avoid_resource_caching();?>"></script>
        <script>
            let __SIGNIN17_AVAILABLE = true;
            let locale
            function LC(k) {
                if (typeof locale[k] === 'undefined') return k
                return locale[k]
            }
            let __meerkat_rpc = <?php echo $_SERVER['REQUEST_URI'] === '/meerkat/application' ? 'true' : 'false'?>;
            let __meerkat_authtoken_exchange_validation_key = '<?php echo $_SESSION['app_secret_pending'];?>'
        </script>
        <script async src="/resources/js/signin17.js<?php avoid_resource_caching();?>"></script>
        <div class="overlay"><div class="indicator"></div><p class="indicator_label"><?php LC('loading');?></p></div>
        <script async src="/extensions/ApplicationIntegration/js/application.js<?php avoid_resource_caching();?>"></script>
    </body>
    </html>
    <?php
};