<?php
namespace Meerkat\Extensions\PermissionsMiddleware;

class RegisterMiddleware {
	/*
	  Registers the middleware.
	*/
    public function Run () {
        global $router;

        $router->RegisterMiddleware('Meerkat\Extensions\PermissionsMiddleware\PMiddleware');
    }
}