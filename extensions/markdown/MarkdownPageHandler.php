<?php
/*
 * Page Controller
 * Basic page handler.
 *
 * @package Meerkat
 * @since   0.0.6
*/
namespace Meerkat\Extensions;
use Parsedown;
use ParsedownExtra;

class Markdown {
    /*
     * Parses the content of your pages.
     *
     * @param string $Text
     * @return array
    */
    public function text ($Text) {
        $pd = new ParsedownExtra();

        return array(
            "response_code" => 200,
            "data" => $pd->text($Text)
        );
    }
}