<?php
/*
 * Polish
 * Overrides the locale settings.
 *
 * @package Meerkat\Extensions\Polish
 * @since   0.6.0
*/
namespace Meerkat\Extensions\Polish;

class RegisterAsLanguage {
    /*
      Registers home routes.

      @return null
    */
    public function Run () {
        global $config;

        $config['app.lang'] = 'pl-PL';
    }
}