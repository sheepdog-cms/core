<?php
return function ($site, $data) {
    $continue = true;
    ?>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><?php echo $site['app.name']?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="/resources/css/install.css">
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="title m-b-md"><?php echo $site['app.name']?></div>
            <?php if (intval(phpversion()[0]) < 7) {
                $continue = false;
                ?>
                <div class="error">PHP 7.0 or greater must be installed.</div>
            <?php } ?>
                
            <?php if (!extension_loaded('openssl')
                || !extension_loaded('zlib')
                || !extension_loaded('json')) {
                $continue = false;
                ?>
                <div class="error">Make sure that OpenSSL, zlib and json are installed.</div>
            <?php } ?>
                
            <?php if (!is_writable(__DIR__ . '/../../../') && !is_writable(__DIR__ . '/../../../cache')) {
                $continue = false;
                ?>
                <div class="error">Make sure that the installation root directory and its cache directory are writeable.</div>
            <?php } ?>
                
            <?php if ($continue) {?>
                <script>let __meerkat_install_drvconf = {}</script>
                <div class="form-control">
                    <label for="sitename">Site name <sup>(REQUIRED)</sup></label>
                    <input id="sitename" type="text" name="sitename" required>
                </div>
                <div class="form-control">
                    <label for="sitedesc">Site description</label>
                    <input id="sitedesc" type="text" name="sitedesc">
                </div>
                <div class="form-control">
                    <label for="admname">Administrator username <sup>(REQUIRED)</sup></label>
                    <input id="admname" type="text" name="admname" required>
                </div>
                <div class="form-control">
                    <label for="dbpass">Administrator password <sup>(REQUIRED)</sup></label>
                    <input id="admpass" type="password" name="admpass" required>
                </div>
                <div class="form-control">
                    <?php
                    $drvconf = array();
                    ?>
                    <label for="dbdriver">Database Driver <sup>(REQUIRED)</sup></label>
                    <select id="dbdriver" name="dbdriver" required>
                        <?php
                        foreach (glob(__DIR__ . '/../../app/Database/Drivers/*.json') as $driver) {
                            $manifest = json_decode(file_get_contents($driver), true);
                            $reqMet = true;

                            foreach ($manifest['Requirements'] as $requirement) {
                                if (!extension_loaded($requirement)) {
                                    $reqMet = false;
                                    break;
                                }
                            }

                            if ($reqMet) {

                                $drvconf[$manifest['Autoload']] = $manifest['Configure'];

                                ?>
                                    <option value="<?php echo $manifest['Autoload']?>"><?php echo $manifest['Name']?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </div>
                <script>
                    let __meerkat_drvconf = <?php echo json_encode($drvconf); ?>
                </script>
                <script src="/resources/js/taiga.js"></script>
                <script src="/resources/js/install.js" async></script>
            <?php } ?>
        </div>
    </body>
</html>
<?php }?>
