<?php
function roll_out_by_chance ($dataarr, $experimentname, $chance) {
    if (!array_key_exists('experiment_' . $experimentname, $_SESSION)) {
        $_SESSION['experiment_' . $experimentname] = rand(0, 100);
    }

    if ($_SESSION['experiment_' . $experimentname] <= $chance) {
        return $experimentname;
    }

    return "";
}

return function ($site, $data) {
    ?>
<html lang="<?php echo $site['app.lang']; ?>">
    <head>
        <meta charset="utf-8">
        <?php meerkat_meta(array(
                'title' => LC('dashboard', true)
        )); ?>
        <?php if (in_array('use:apiv1', $data['experiments'])
            || in_array('use:apiv2', $data['experiments'])) {?>
            <link rel="stylesheet" href="/resources/css/admin.css"/>
        <?php } else if (!in_array('no-dashboard', $data['experiments'])) {?>
            <link rel="stylesheet" href="/resources/css/dashboard.css"/>
        <?php } else {?>
            <link rel="stylesheet" href="/resources/css/refreshed-admin.css"/>
        <?php } ?>
    </head>
    <body>
        <?php if (!in_array('use:apiv1', $data['experiments'])
                && !in_array('use:apiv2', $data['experiments'])) {?>
            <h1 id="__site_name" style="display: none"><?php echo $site['site.name']; ?></h1>
            <header></header>
        <?php } else { ?>
            <header>
                <div id="buttons">
                    <a href="/" id="back-a">
                        <button id="back"></button>
                    </a>
                </div>
                <h1 id="__site_name"><?php echo $site['site.name']; ?></h1>
                <div class="right">
                    <span id="uname"></span>
                </div>
            </header>
        <?php }?>
        <container></container>

        <script src="/resources/js/taiga.js<?php avoid_resource_caching();?>"></script>
        <script>let __SIGNIN17_AVAILABLE = true</script>
        <script async src="/resources/js/signin17.js<?php avoid_resource_caching();?>"></script>

        <?php if (in_array('use:apiv1', $data['experiments'])) {?>
            <script src="https://cdn.jsdelivr.net/markdown-it/8.3.1/markdown-it.min.js"></script>
            <nav class="full-height"></nav>
            <div class="overlay"><p class="indicator_label"></p></div>
            <script src="/resources/js/admin.js<?php avoid_resource_caching();?>"></script>
        <?php } else if (in_array('use:apiv2', $data['experiments'])) {?>
            <script async src="/resources/js/admin.v2.js<?php avoid_resource_caching();?>"></script>
            <div class="overlay"><p class="indicator_label"></p></div>
        <?php
        } else {
            ?>
            <div class="overlay"><div class="indicator"></div><p class="indicator_label"><?php LC('loading');?></p></div>
            <script async src="/resources/js/refreshed_admin.js<?php avoid_resource_caching();?>"></script>
            <?php
        }
        ?>

    </body>
</html>
<?php
};