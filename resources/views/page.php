<?php
return function ($site, $data) {
    ?>
<html lang="<?php echo $data['page_lang']; ?>">
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="/resources/css/panic.css"/>
        <?php meerkat_meta($data); ?>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md"><?php echo $data['title'];?></div>
                <div class="links"><?php echo $data['content'];?></div>
            </div>
        </div>
    </body>
</html>
<?php
};