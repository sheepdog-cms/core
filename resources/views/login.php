<?php
return function ($site, $data) {
    ?>
<html lang="<?php echo $site['app.lang']; ?>">
    <head>
        <meta charset="utf-8">
        <title><?php LC('loginto');?> <?php echo $site['site.name'];?></title>
        <link rel="stylesheet" href="/resources/css/login.css"/>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md"><?php LC('loginto');?> <?php echo $site['site.name'];?></div>
                    <form method="POST">
                        <?php echo csrf_token(); ?>
                        <?php if ($data['Error']) {?>
                            <div class="error">
                                <?php LC('loginfail');?>
                            </div>
                        <?php } ?>
                        <div class="form-control">
                            <input id="name" type="text" name="name" placeholder="<?php LC('username');?>">
                        </div>
                        <div class="form-control">
                            <input id="pass" type="password" name="pass" placeholder="<?php LC('password');?>">
                        </div>
                        <input name="submit" type="submit" value="<?php LC('login');?>"/>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
<?php
};