function spawnProgress() {
    taiga.render({
        body: [
            {
                tag: `div.progress`,
                data: 'Please wait...',
                save: true
            }
        ]
    })
    return taiga.saved_object
}
function encode(obj) {
    let str = ""
    for (let key in obj) {
        if (str != "") {
            str += "&"
        }
        str += key + "=" + encodeURIComponent(obj[key])
    }
    return str
}
function destroyProgress(pbar) {
    setTimeout(()=>{document.body.removeChild(pbar)}, 2500)
}
function progressSuccess(pbar) {
    pbar.className = 'success'
    pbar.innerText = 'Ok, here\'s the next step...'
}
function progressError(pbar) {
    pbar.className = 'error'
    pbar.innerText = 'Oops! Something is wrong.'
}

taiga.render({
    'body > div.flex-center.position-ref.full-height': [{
        tag: 'input#submit',
        attributes: {
            value: 'Continue',
            type: 'submit'
        },
        on: {
            click: (view) => {
                let sitename = document.querySelector('#sitename')
                let sitedesc = document.querySelector('#sitedesc')
                let admname = document.querySelector('#admname')
                let admpass = document.querySelector('#admpass')
                let dbdriver = document.querySelector('#dbdriver')

                let pbar = spawnProgress()

                if (!sitename.checkValidity()
                || !admname.checkValidity()
                || !admpass.checkValidity()
                || !dbdriver.checkValidity()) {
                    progressError(pbar)
                    pbar.innerText = 'Fill the required fields.'
                    destroyProgress(pbar)
                    return
                }

                let __data = {
                    sitename: sitename.value,
                    sitedesc: sitedesc.value,
                    admname: admname.value,
                    admpass: admpass.value,
                    dbdriver: dbdriver.value
                }

                let formcontrols = document.getElementsByClassName('form-control')
                for (let i = 0; i < formcontrols.length;) {
                    formcontrols[0].parentNode.removeChild(formcontrols[0])
                }

                view.parentNode.removeChild(view)

                let drvdata = __meerkat_drvconf[dbdriver.value]

                for (let key in drvdata) {
                    taiga.render({
                        'body > div.flex-center.position-ref.full-height': [
                            {
                                tag: 'div.form-control',
                                children: [
                                    {
                                        tag: 'label',
                                        attributes: {
                                            for: key
                                        },
                                        data: `${drvdata[key]} <sup>(REQUIRED)</sup>`,
                                        html: true
                                    },
                                    {
                                        tag: `input#db${key}`,
                                        attributes: {
                                            type: 'text',
                                            name: key,
                                            required: true
                                        }
                                    }
                                ]
                            }
                        ]
                    })
                }

                progressSuccess(pbar)
                destroyProgress(pbar)

                taiga.render({
                    'body > div.flex-center.position-ref.full-height': [{
                        tag: 'input#submit',
                        attributes: {
                            value: 'Finish',
                            type: 'submit'
                        },
                        on: {
                            click: (view) => {
                                let payload = {
                                    admname: __data.admname,
                                    dbdriver: __data.dbdriver
                                }

                                for (let key in drvdata) {
                                    let node = document.querySelector(`#db${key}`)
                                    payload[key] = node.value
                                }

                                let formcontrols = document.getElementsByClassName('form-control')
                                for (let i = 0; i < formcontrols.length;) {
                                    formcontrols[0].parentNode.removeChild(formcontrols[0])
                                }

                                view.parentNode.removeChild(view)

                                taiga.render({
                                    'body > div.flex-center.position-ref.full-height': [{
                                        tag: 'p',
                                        data: 'Please wait while we are configuring your website...',
                                        save: true
                                    }]
                                })

                                let status = taiga.saved_object

                                let pbar = spawnProgress()

                                xhrp('/install/db', () => {
                                    status.innerText = 'Updating the database with new values (so your site has a name)...'
                                    xhrp('/api/v3/maintenance/settings/site.name', () => {
                                        xhrp('/api/v3/maintenance/settings/site.desc', () => {
                                            status.innerText = 'Creating superuser account...'
                                            xhrp('/api/v3/users/new', () => {
                                                status.innerText = 'Done.'
                                                setTimeout(() => {
                                                    window.location = '/';
                                                }, 1500)
                                            }, {
                                                name: __data.admname,
                                                password: __data.admpass
                                            }, pbar)
                                        }, {
                                            value: __data.sitedesc
                                        }, pbar)
                                    }, {
                                        value: __data.sitename
                                    }, pbar)
                                }, payload, pbar)
                            }
                        }
                    }]
                })
            }
        }
    }]
})
