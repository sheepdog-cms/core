class Taiga {
    constructor () {
        this.render({
            body: [{
                tag: 'DOM#__taiga_hidden_DOM',
                attributes: {
                    style: 'display: none'
                }
            }]
        })
    }

    renderNode (node, parent) {
        let selector = node.tag.split(/(?=\.)|(?=#)|(?=\[)/)

        let docnode = document.createElement(selector[0])

        for (let event in node.on) {
            docnode.addEventListener(event, node.on[event].bind(docnode, docnode))
        }

        for (let i = 1; i < selector.length; i++) {
            switch (selector[i][0]) {
                case '#':
                    if (docnode.id.length !== 0) {
                        docnode.id += ' '
                    }

                    docnode.id += selector[i].substr(1)
                    break
                case '.':
                    if (docnode.className.length !== 0) {
                        docnode.className += ' '
                    }

                    docnode.className += selector[i].substr(1)
                    break
            }
        }

        if (node.data) {
            if (node.html) {
                docnode.innerHTML = node.data
            } else {
                docnode.innerText = node.data
            }
        }

        if (node.attributes) {
            for (let attribute in node.attributes) {
                docnode.setAttribute(attribute, node.attributes[attribute])
            }
        }

        if (node.properties) {
            for (let property in node.properties) {
                docnode[property] = node.properties[property]
            }
        }

        if (node.datasource) {
            if (!node.children) {
                node.children = []
            }

            window.requestAnimationFrame(() => {
                for (let row in node.datasource) {
                    let newnode = node.onnewitem(row, node.datasource[row])

                    if (typeof newnode !== 'undefined') {
                        this.renderNode(newnode, docnode)
                    }
                }

                if (node.onitemsrendered) {
                    node.onitemsrendered()
                }
            })
        }

        if (node.save) {
            this.saved_object = docnode
        }

        parent.appendChild(docnode)

        if (node.children) {
            node.children.forEach(childnode => {
                this.renderNode(childnode, docnode)
            })
        }
    }

    render (structure) {
        for (let element in structure) {
            let docnode = document.querySelector(element)

            if (!structure[element]) {
                docnode.parentNode.removeChild(docnode)
                continue
            }

            structure[element].forEach(node => {
                this.renderNode(node, docnode)
            })
        }
    }

    replace (selector, newDefinition) {
        newDefinition.save = true
        let old = document.querySelector(selector)

        this.render({
            'DOM#__taiga_hidden_DOM': [newDefinition]
        })

        if (newDefinition.inheritChildren) {
            while (true) {
                let childNode = old.childNodes[0]

                if (!(childNode instanceof Node)) {
                    break
                }

                this.saved_object.appendChild(childNode)
            }
        }

        this.saved_object.parentNode.removeChild(this.saved_object)

        old.parentNode.replaceChild(this.saved_object, old)
    }

    queryProperties (selector, properties) {
        let output = {}
        let node = document.querySelector(selector)

        for (let property in properties) {
            let key = properties[property]

            output[key] = node[key]
        }

        return output
    }
}
let taiga = new Taiga()

function xhr (uri, cb, pbar) {
    let x = new XMLHttpRequest()
    x.onreadystatechange = () => {
        if (x.readyState === 4) {
            if (x.status === 200) {
                window.requestAnimationFrame(() => {
                    cb(JSON.parse(x.responseText))
                })
            } else if (pbar) {
                try {
                    let res = JSON.parse(x.responseText)

                    if (res.data.localisation_key) {
                        progressError(pbar)
                        destroyProgress(pbar)
                        pbar.innerText = LC(res.data.localisation_key)
                    }
                } catch (err) {
                    progressError(pbar)
                    destroyProgress(pbar)
                    pbar.innerText = LC('xhr_error_internal')
                }
            }
        }
    }
    x.onerror = (_) => {
        progressError(pbar)
        destroyProgress(pbar)
        pbar.innerText = LC('xhr_error')
    }
    x.open("GET", uri, true)
    x.send()
}
function xhrp (uri, cb, payload, pbar) {
    xhr('/api/v3/auth/csrf', (a) => {
        payload.csrf = a.data.csrf
        let x = new XMLHttpRequest()
        x.onreadystatechange = () => {
            if (x.readyState === 4) {
                if (x.status === 200) {
                    window.requestAnimationFrame(() => {
                        cb(JSON.parse(x.responseText))
                    })
                } else if (pbar) {
                    try {
                        let res = JSON.parse(x.responseText)

                        if (res.data.localisation_key) {
                            progressError(pbar)
                            destroyProgress(pbar)
                            pbar.innerText = LC(res.data.localisation_key)
                        }
                    } catch (err) {
                        progressError(pbar)
                        destroyProgress(pbar)
                        pbar.innerText = LC('xhr_error_internal')
                    }
                }
            }
        }
        x.onerror = (_) => {
            progressError(pbar)
            destroyProgress(pbar)
            pbar.innerText = LC('xhr_error')
        }
        x.open("POST", uri, true)
        x.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
        x.send(encode(payload))
    }, pbar)
}