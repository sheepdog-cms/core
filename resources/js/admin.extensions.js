xhr('/api/v3/maintenance/extensions', (s) => {
    taiga.render({
        '#admin_extensions_panel': [
            {
                tag: 'div.grid',
                datasource: s.data,
                onnewitem: (row, data) => {
                    attributes = {
                        type: 'checkbox'
                    }

                    if (data.enabled) {
                        attributes.checked = true
                    }

                    return {
                        tag: `div${data.Outdated ? '.outdated' : ''}`,
                        children: [
                            {
                                tag: 'div.metacontainer',
                                children: [
                                    {
                                        tag: 'strong',
                                        data: data.name
                                    },
                                    {
                                        tag: `label.badge`,
                                        data: data.Outdated ? LC('Outdated-Extension') : data.Experimental ? LC('Experimental-Extension') : ''
                                    },
                                    {
                                        tag: `label.badge${data.Deprecated ? '.warning' : ''}`,
                                        data: data.Deprecated ? LC('Deprecated-Extension') : ''
                                    },
                                    {
                                        tag: `label.badge${data.CEM ? '.critical' : ''}`,
                                        data: data.CEM ? LC('CEM-Extension')
                                            : data.IsAPI ? LC('API-Extension') : LC('Normal-Extension'),
                                        attributes: {
                                            title: data.CEM ? LC('CEM-Extension-Tooltip')
                                                : data.IsAPI ? LC('API-Extension') : LC('Normal-Extension')
                                        }
                                    }
                                ]
                            },
                            {
                                tag: 'input',
                                attributes: attributes,
                                on: {
                                    change: (_) => {
                                        let pbar = spawnProgress()
                                        xhrp('/api/v3/maintenance/extensions', (a) => {
                                            progressSuccess(pbar)
                                            destroyProgress(pbar)
                                        }, {
                                            name: row
                                        }, pbar)
                                    }
                                }
                            },
                            {
                                tag: 'p',
                                attributes: {
                                    style: `color:#bf1040;display:${data.CEM || data.Deprecated || data.Outdated ? 'block' : 'none'}`
                                },
                                data: data.Outdated ? LC('Outdated-Extension-Info') : data.Deprecated ? LC('Deprecated-Extension-Info') : LC('CEM-Extension-Info')
                            },
                            {
                                tag: 'p',
                                data: data.description
                            }
                        ]
                    }
                }
            }
        ]
    })
})