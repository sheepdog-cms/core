let spawnProgress=()=>{};let progressError=spawnProgress;let progressSuccess=spawnProgress;let destroyProgress=spawnProgress
function xhr (uri, cb) {
    let x = new XMLHttpRequest()
    x.onreadystatechange = () => {
        if (x.readyState == 4 && (x.status == 200 || x.status == 401)) {
           window.requestAnimationFrame(() => {cb(JSON.parse(x.responseText))})
        }
    }
    x.open("GET", uri, true)
    x.send()
}
function url_slug(s) {
	let char_map = {
		// Latin
		'À': 'A', 'Á': 'A', 'Â': 'A', 'Ã': 'A', 'Ä': 'A', 'Å': 'A', 'Æ': 'AE', 'Ç': 'C', 
		'È': 'E', 'É': 'E', 'Ê': 'E', 'Ë': 'E', 'Ì': 'I', 'Í': 'I', 'Î': 'I', 'Ï': 'I', 
		'Ð': 'D', 'Ñ': 'N', 'Ò': 'O', 'Ó': 'O', 'Ô': 'O', 'Õ': 'O', 'Ö': 'O', 'Ő': 'O', 
		'Ø': 'O', 'Ù': 'U', 'Ú': 'U', 'Û': 'U', 'Ü': 'U', 'Ű': 'U', 'Ý': 'Y', 'Þ': 'TH', 
		'ß': 'ss', 
		'à': 'a', 'á': 'a', 'â': 'a', 'ã': 'a', 'ä': 'a', 'å': 'a', 'æ': 'ae', 'ç': 'c', 
		'è': 'e', 'é': 'e', 'ê': 'e', 'ë': 'e', 'ì': 'i', 'í': 'i', 'î': 'i', 'ï': 'i', 
		'ð': 'd', 'ñ': 'n', 'ò': 'o', 'ó': 'o', 'ô': 'o', 'õ': 'o', 'ö': 'o', 'ő': 'o', 
		'ø': 'o', 'ù': 'u', 'ú': 'u', 'û': 'u', 'ü': 'u', 'ű': 'u', 'ý': 'y', 'þ': 'th', 
		'ÿ': 'y',

		// Latin symbols
		'©': '(c)',

		// Greek
		'Α': 'A', 'Β': 'B', 'Γ': 'G', 'Δ': 'D', 'Ε': 'E', 'Ζ': 'Z', 'Η': 'H', 'Θ': '8',
		'Ι': 'I', 'Κ': 'K', 'Λ': 'L', 'Μ': 'M', 'Ν': 'N', 'Ξ': '3', 'Ο': 'O', 'Π': 'P',
		'Ρ': 'R', 'Σ': 'S', 'Τ': 'T', 'Υ': 'Y', 'Φ': 'F', 'Χ': 'X', 'Ψ': 'PS', 'Ω': 'W',
		'Ά': 'A', 'Έ': 'E', 'Ί': 'I', 'Ό': 'O', 'Ύ': 'Y', 'Ή': 'H', 'Ώ': 'W', 'Ϊ': 'I',
		'Ϋ': 'Y',
		'α': 'a', 'β': 'b', 'γ': 'g', 'δ': 'd', 'ε': 'e', 'ζ': 'z', 'η': 'h', 'θ': '8',
		'ι': 'i', 'κ': 'k', 'λ': 'l', 'μ': 'm', 'ν': 'n', 'ξ': '3', 'ο': 'o', 'π': 'p',
		'ρ': 'r', 'σ': 's', 'τ': 't', 'υ': 'y', 'φ': 'f', 'χ': 'x', 'ψ': 'ps', 'ω': 'w',
		'ά': 'a', 'έ': 'e', 'ί': 'i', 'ό': 'o', 'ύ': 'y', 'ή': 'h', 'ώ': 'w', 'ς': 's',
		'ϊ': 'i', 'ΰ': 'y', 'ϋ': 'y', 'ΐ': 'i',

		// Turkish
		'Ş': 'S', 'İ': 'I', 'Ç': 'C', 'Ü': 'U', 'Ö': 'O', 'Ğ': 'G',
		'ş': 's', 'ı': 'i', 'ç': 'c', 'ü': 'u', 'ö': 'o', 'ğ': 'g', 

		// Russian
		'А': 'A', 'Б': 'B', 'В': 'V', 'Г': 'G', 'Д': 'D', 'Е': 'E', 'Ё': 'Yo', 'Ж': 'Zh',
		'З': 'Z', 'И': 'I', 'Й': 'J', 'К': 'K', 'Л': 'L', 'М': 'M', 'Н': 'N', 'О': 'O',
		'П': 'P', 'Р': 'R', 'С': 'S', 'Т': 'T', 'У': 'U', 'Ф': 'F', 'Х': 'H', 'Ц': 'C',
		'Ч': 'Ch', 'Ш': 'Sh', 'Щ': 'Sh', 'Ъ': '', 'Ы': 'Y', 'Ь': '', 'Э': 'E', 'Ю': 'Yu',
		'Я': 'Ya',
		'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ё': 'yo', 'ж': 'zh',
		'з': 'z', 'и': 'i', 'й': 'j', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n', 'о': 'o',
		'п': 'p', 'р': 'r', 'с': 's', 'т': 't', 'у': 'u', 'ф': 'f', 'х': 'h', 'ц': 'c',
		'ч': 'ch', 'ш': 'sh', 'щ': 'sh', 'ъ': '', 'ы': 'y', 'ь': '', 'э': 'e', 'ю': 'yu',
		'я': 'ya',

		// Ukrainian
		'Є': 'Ye', 'І': 'I', 'Ї': 'Yi', 'Ґ': 'G',
		'є': 'ye', 'і': 'i', 'ї': 'yi', 'ґ': 'g',

		// Czech
		'Č': 'C', 'Ď': 'D', 'Ě': 'E', 'Ň': 'N', 'Ř': 'R', 'Š': 'S', 'Ť': 'T', 'Ů': 'U', 
		'Ž': 'Z', 
		'č': 'c', 'ď': 'd', 'ě': 'e', 'ň': 'n', 'ř': 'r', 'š': 's', 'ť': 't', 'ů': 'u',
		'ž': 'z', 

		// Polish
		'Ą': 'A', 'Ć': 'C', 'Ę': 'e', 'Ł': 'L', 'Ń': 'N', 'Ó': 'o', 'Ś': 'S', 'Ź': 'Z', 
		'Ż': 'Z', 
		'ą': 'a', 'ć': 'c', 'ę': 'e', 'ł': 'l', 'ń': 'n', 'ó': 'o', 'ś': 's', 'ź': 'z',
		'ż': 'z',

		// Latvian
		'Ā': 'A', 'Č': 'C', 'Ē': 'E', 'Ģ': 'G', 'Ī': 'i', 'Ķ': 'k', 'Ļ': 'L', 'Ņ': 'N', 
		'Š': 'S', 'Ū': 'u', 'Ž': 'Z', 
		'ā': 'a', 'č': 'c', 'ē': 'e', 'ģ': 'g', 'ī': 'i', 'ķ': 'k', 'ļ': 'l', 'ņ': 'n',
		'š': 's', 'ū': 'u', 'ž': 'z'
	}
	for (let k in char_map) {
		s = s.replace(RegExp(k, 'g'), char_map[k])
	}
    let alnum = (typeof(XRegExp) === 'undefined') ? RegExp('[^a-z0-9]+', 'ig') : XRegExp('[^\\p{L}\\p{N}]+', 'ig');
	s = s.replace(alnum, '-')
	s = s.replace(RegExp('[-]{2,}', 'g'), '-')
    s = s.replace(RegExp('(^-|-$)', 'g'), '');
	s = s.substring(0, 120)
	return s.toLowerCase()
}
let locale
function LC(k) {
    if (typeof locale[k] === 'undefined') return k
    return locale[k]
}
function encode(obj) {
    let str = ""
    for (let key in obj) {
        if (str != "") {
            str += "&"
        }
        str += key + "=" + encodeURIComponent(obj[key])
    }
    return str
}
function ssd (n, g, h=0) {
    let d = document.createElement('div')
    d.innerHTML = `<strong>${LC(g)}</strong>`
    n.appendChild(d)
    let v = document.createElement('section')
    if (!h) {
    d.addEventListener('click', ((d, v) => {
        document.getElementsByClassName('active')[0].className = ''
        let sac = document.getElementsByClassName('sactive')[0]
        sac.style.display = 'none'
        sac.className = 'position-ref'
        v.style.display = 'block'
        d.className = 'active'
        v.className = 'position-ref sactive'
    }).bind(null, d, v))
    if (!a) {
        v.className = 'position-ref'
        v.style.display = 'none'
    } else {
        v.className = 'position-ref sactive'
        d.className = 'active'
        a = false
    }
    document.body.appendChild(v)
    } else {
        d.className = 'header'
    }
    return v
}
let a = true
function od(v, o, c) {
    let g = document.createElement('div')
    let l = document.createElement('strong')
    let p = document.createElement('p')
    l.innerText = LC(o)
    p.innerHTML = LC(`${o}.desc`)
    g.appendChild(l)
    g.appendChild(p)
    v.appendChild(g)
    g.className = 'indexed'
    if (c.type == 'widget') {
        g.id = c.identifier
        let script = document.createElement('script')
        script.src = c.value
        document.body.appendChild(script)
        return
    }
    let i = document.createElement('input')
    i.type = c.type
    i.key = o
    if (c.spoiler) {
        i.className = 'spoiler'
    }
    if (typeof c.text === 'undefined') {
        i.value = c.value
    } else {
        i.value = LC(c.text)
    }
    if (typeof c.maxlength !== 'undefined') {
        i.maxLength = c.maxlength
    }
    if (c.type === 'text') {
        i.addEventListener('keydown', ((i, event) => {
            if (event.keyCode == 13) {
                let e = document.createElement('div')
                e.className = 'progress'
                e.innerText = LC('updating')
                document.body.appendChild(e)
                xhr('/api/v1/admin/whoami', (a) => {
                    let http = new XMLHttpRequest();
                    http.open("POST", `/api/v1/admin/update/${i.key}`, true);
                    http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                    http.onreadystatechange = () => {
                        if (http.readyState == 4) {
                            if (http.status == 200 && JSON.parse(http.responseText).code == 200) {
                                e.className = 'success'
                                e.innerText = LC('updated')
                                setTimeout(()=>{document.body.removeChild(e)}, 2500)
                            } else {
                                e.className = 'error'
                                e.innerText = LC('update_error')
                                setTimeout(()=>{document.body.removeChild(e)}, 2500)
                            }
                        }
                    }
                    http.send(encode({
                        "value": i.value,
                        "csrf": a.data.csrf
                    }))
                })
            }
        }).bind(null, i))
    } else if (c.type == 'submit') {
        i.addEventListener('click', (event) => {
            let e = document.createElement('div')
            e.className = 'progress'
            e.innerText = LC('updating')
            document.body.appendChild(e)
            xhr('/api/v1/admin/whoami', (a) => {
                let http = new XMLHttpRequest();
                http.open("GET", c.action, true);
                http.onreadystatechange = () => {
                    if (http.readyState == 4) {
                        if (http.status == 200 && JSON.parse(http.responseText).code == 200) {
                            e.className = 'success'
                            e.innerText = LC('updated')
                            setTimeout(()=>{document.body.removeChild(e)}, 2500)
                        } else {
                            e.className = 'error'
                            e.innerText = LC('update_error')
                            setTimeout(()=>{document.body.removeChild(e)}, 2500)
                        }
                    }
                }
                http.send(null)
            })
        })
    }
    g.appendChild(i)
}
// SIG compat
function __setup_rjs() {
    taiga.render({'.overlay':false})//remove SIG overlay
    xhr('/api/v1/admin/whoami', (a) => {
        if (a.code === 401) {
            location.reload()
            return
        }


        xhr('/api/v1/admin/locale', (l) => {
            locale = l.data

            let depr = document.createElement('div')
            depr.className = 'deprecated'
            depr.innerHTML = LC('deprecated')
            depr.addEventListener('click', () => {
                depr.className = 'deprecated np'
            })
            document.body.appendChild(depr)

            document.getElementById('uname').innerHTML = `${a.data.user}<button class="logout">${LC('logout')}</button>`
            document.getElementsByClassName('logout')[0].addEventListener('click', () => {
                xhr('/api/v1/admin/logout', (s) => {
                    location.href = '/admin'
                })
            })

            let nav = document.getElementsByTagName('nav')[0]

            xhr('/api/v1/admin/list/settings', (s) => {
                s = s.data
                for (let groupi in s._indexing) {
                    let group = s[s._indexing[groupi]]
                    let v = ssd(nav, s._indexing[groupi], group.handler == 'header')
                    switch (group.handler) {
                        case 'indexed':
                            for (let optioni in group._indexing) {
                                let option = group[group._indexing[optioni]]
                                od(v, `${s._indexing[groupi]}.${group._indexing[optioni]}`, option)
                            }
                            break
                        case 'pages':
                            let tablep = document.createElement('table')
                            tablep.innerHTML = `<tr><th><strong>${LC('pagetitle')}</strong></th><th><strong>${LC('language')}</strong></th><th><strong>${LC('isindex')}</strong></th><th><strong>${LC('actions')}</strong></th></tr>`
                            v.appendChild(tablep)
                            let ipgs = document.createElement('input')
                            ipgs.type = 'submit'
                            ipgs.value = LC('newpage')
                            xhr('/api/v1/admin/list/pages', ((e) => {
                                for (let pagei in e.data._indexing) {
                                    let page = e.data[pagei]
                                    let t = document.createElement('tr')
                                    let d = document.createElement('td')
                                    let s = document.createElement('strong')
                                    let i = document.createElement('td')
                                    let a = document.createElement('td')
                                    i.innerText = page.IsIndex == "1" ? LC('yes') : LC('no')
                                    let l = document.createElement('td')
                                    l.innerText = page.Language
                                    s.innerText = page.Title
                                    let del = document.createElement('button')
                                    del.innerText = LC('delete')
                                    del.addEventListener('click', () => {
                                        let e = document.createElement('div')
                                        e.className = 'progress'
                                        e.innerText = LC('updating')
                                        document.body.appendChild(e)
                                        xhr(`/api/v1/admin/delete/page/${page.GUID}`, (pagec) => {
                                            if (pagec.code == 200) {
                                                e.className = 'success'
                                                e.innerText = LC('updated')
                                                setTimeout(() => {
                                                    document.body.removeChild(e)
                                                }, 2500)
                                            } else {
                                                e.className = 'error'
                                                e.innerText = LC('update_error')
                                                setTimeout(() => {
                                                    document.body.removeChild(e)
                                                }, 2500)
                                            }
                                        })
                                    })
                                    a.appendChild(del)
                                    let edi = document.createElement('button')
                                    edi.innerText = LC('edit')
                                    edi.addEventListener('click', () => {
                                        ipgs.click()
                                        xhr(`/api/v1/admin/get/page/${page.GUID}`, (pagec) => {
                                            let code = document.getElementsByClassName('editor-codearea')[0]
                                            code.innerText = pagec.data.Content
                                            let ctrl = document.getElementsByClassName('editor-meta-controller')[0]
                                            ctrl.value = pagec.data.Controller
                                            let title = document.getElementsByClassName('editor-meta-title')[0]
                                            title.value = pagec.data.Title
                                            let lang = document.getElementsByClassName('editor-meta-language')[0]
                                            lang.value = pagec.data.Language
                                            let uri = document.getElementsByClassName('editor-meta-uri')[0]
                                            uri.value = page.Uri
                                            let index = document.getElementsByClassName('editor-meta-index')[0]
                                            index.checked = pagec.data.IsIndex == '1'
                                            let view = document.getElementsByClassName('editor-meta-view')[0]
                                            view.value = pagec.data.View
                                            code.existingpageguid = page.GUID
                                        })
                                    })
                                    a.appendChild(edi)
                                    d.appendChild(s)
                                    t.appendChild(d)
                                    t.appendChild(l)
                                    t.appendChild(i)
                                    t.appendChild(a)
                                    tablep.appendChild(t)
                                }
                            }))
                            let divv = document.createElement('div')
                            divv.className = 'indexed'
                            divv.innerHTML = `<strong>${LC('newpage')}</strong><p>${LC('newpage.desc')}</p>`
                            ipgs.addEventListener('click', () => {
                                let e = document.createElement('div')
                                e.className = 'progress'
                                e.innerText = LC('loadingeditor')
                                document.body.appendChild(e)
                                let editor = document.createElement('section')
                                editor.className = 'editor'
                                let blackout = document.createElement('div')
                                document.body.appendChild(blackout)
                                blackout.className = 'blackout'
                                let title = document.createElement('input')
                                title.type = 'text'
                                title.placeholder = LC('pagetitle')
                                title.className = 'editor-meta-title'
                                editor.appendChild(title)
                                let split = document.createElement('div')
                                split.className = 'split'
                                let code = document.createElement('textarea')
                                code.className = 'editor-codearea'
                                code.innerText = '# Hello world!'
                                split.appendChild(code)
                                let preview = document.createElement('div')
                                split.appendChild(preview)
                                let mdit = window.markdownit()
                                code.addEventListener('keydown', () => {
                                    preview.innerHTML = mdit.render(code.value)
                                })
                                editor.appendChild(split)
                                let meta = document.createElement('div')
                                meta.className = 'metadata'
                                let edgroup = document.createElement('div')
                                edgroup.className = 'editor-group-primitive'
                                let header = document.createElement('h2')
                                header.className = 'editor-header'
                                header.innerText = LC('editor.metadata')
                                let submt = document.createElement('button')
                                submt.className = 'editor-btn'
                                submt.innerText = LC('editor.submit')
                                edgroup.appendChild(header)
                                edgroup.appendChild(submt)
                                meta.appendChild(edgroup)
                                let edgroup2 = document.createElement('div')
                                edgroup2.className = 'editor-group'
                                let label1 = document.createElement('label')
                                label1.innerText = LC('editor.uri')
                                let uriin = document.createElement('input')
                                uriin.type = 'text'
                                uriin.className = 'editor-meta-uri'
                                uriin.maxLength = 120
                                title.addEventListener('keyup', () => {
                                    uriin.value = url_slug(title.value)
                                })
                                edgroup2.appendChild(label1)
                                edgroup2.appendChild(uriin)
                                meta.appendChild(edgroup2)
                                let edgroup3 = document.createElement('div')
                                edgroup3.className = 'editor-group'
                                let label2 = document.createElement('label')
                                label2.innerText = LC('editor.controller')
                                let controllerin = document.createElement('input')
                                controllerin.type = 'text'
                                controllerin.className = 'editor-meta-controller'
                                controllerin.value = 'Sheepdog\\Extensions\\Markdown'
                                edgroup3.appendChild(label2)
                                edgroup3.appendChild(controllerin)
                                meta.appendChild(edgroup3)
                                let edgroup4 = document.createElement('div')
                                edgroup4.className = 'editor-group'
                                let label3 = document.createElement('label')
                                label3.innerText = LC('language')
                                let languagein = document.createElement('input')
                                languagein.type = 'text'
                                languagein.maxLength = 2
                                languagein.className = 'editor-meta-language'
                                languagein.value = ''
                                edgroup4.appendChild(label3)
                                edgroup4.appendChild(languagein)
                                meta.appendChild(edgroup4)
                                let edgroup5 = document.createElement('div')
                                edgroup5.className = 'editor-group'
                                let label4 = document.createElement('label')
                                label4.innerText = LC('isindex')
                                let isindexin = document.createElement('input')
                                isindexin.type = 'checkbox'
                                isindexin.className = 'editor-meta-index'
                                edgroup5.appendChild(label4)
                                edgroup5.appendChild(isindexin)
                                let edgroup6 = document.createElement('div')
                                edgroup6.className = 'editor-group'
                                let label5 = document.createElement('label')
                                label5.innerText = LC('editor.view')
                                let viewin = document.createElement('input')
                                viewin.type = 'text'
                                viewin.text = 'page.php'
                                viewin.className = 'editor-meta-view'
                                edgroup6.appendChild(label5)
                                edgroup6.appendChild(viewin)
                                meta.appendChild(edgroup5)
                                meta.appendChild(edgroup6)
                                editor.appendChild(meta)
                                document.body.appendChild(editor)
                                code.existingpageguid = 'random'
                                submt.addEventListener('click', () => {
                                    let e = document.createElement('div')
                                    e.className = 'progress'
                                    e.innerText = LC('updating')
                                    document.body.appendChild(e)
                                    xhr('/api/v1/admin/whoami', (a) => {
                                        let http = new XMLHttpRequest();
                                        http.open("POST", `/api/v1/admin/update/page/${code.existingpageguid}`, true)
                                        http.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
                                        http.onreadystatechange = () => {
                                            if (http.readyState == 4) {
                                                if (http.status == 200 && JSON.parse(http.responseText).code == 200) {
                                                    e.className = 'success'
                                                    e.innerText = LC('updated')
                                                    location.reload()
                                                } else {
                                                    e.className = 'error'
                                                    e.innerText = LC('update_error')
                                                    setTimeout(() => {
                                                        document.body.removeChild(e)
                                                    }, 2500)
                                                }
                                            }
                                        }
                                        http.send(encode({
                                            "title": title.value,
                                            "content": code.value,
                                            "language": languagein.value,
                                            "controller": controllerin.value,
                                            "isindex": isindexin.checked,
                                            "uri": uriin.value,
                                            "view": viewin.value,
                                            "csrf": a.data.csrf
                                        }))
                                    })
                                })
                                setTimeout(() => {
                                    document.body.removeChild(e)
                                }, 1000)
                            })
                            divv.appendChild(ipgs)
                            v.appendChild(divv)
                            break
                        case 'users':
                            v.innerHTML = `<p>${LC('usersnotice')}</p>`
                            let table = document.createElement('table')
                            table.innerHTML = `<tr><th><strong>${LC('username')}</strong></th><th><strong>${LC('actions')}</strong></th></tr>`
                            v.appendChild(table)
                            xhr('/api/v1/admin/list/users', ((v, e) => {
                                for (let useri in e.data._indexing) {
                                    let user = e.data[useri]
                                    let t = document.createElement('tr')
                                    let d = document.createElement('td')
                                    let s = document.createElement('strong')
                                    s.innerText = user.Name
                                    d.appendChild(s)
                                    let a = document.createElement('td')
                                    let del = document.createElement('button')
                                    del.innerText = LC('deleteuser')
                                    del.addEventListener('click', () => {
                                        let e = document.createElement('div')
                                        e.className = 'progress'
                                        e.innerText = LC('updating')
                                        document.body.appendChild(e)
                                        xhr('/api/v1/admin/whoami', (a) => {
                                            let http = new XMLHttpRequest();
                                            http.open("POST", `/api/v1/admin/delete/user`, true);
                                            http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                                            http.onreadystatechange = () => {
                                                if (http.readyState == 4) {
                                                    if (http.status == 200 && JSON.parse(http.responseText).code == 200) {
                                                        e.className = 'success'
                                                        e.innerText = LC('updated')
                                                        location.reload()
                                                    } else {
                                                        e.className = 'error'
                                                        e.innerText = LC('update_error')
                                                        setTimeout(() => {
                                                            document.body.removeChild(e)
                                                        }, 2500)
                                                    }
                                                }
                                            }
                                            http.send(encode({
                                                "name": user.Name,
                                                "csrf": a.data.csrf
                                            }))
                                        })
                                    })
                                    a.appendChild(del)
                                    t.appendChild(d)
                                    t.appendChild(a)
                                    table.appendChild(t)
                                }
                            }).bind(null, v))
                            let div = document.createElement('div')
                            div.className = 'indexed'
                            div.innerHTML = `<strong>${LC('newuser')}</strong><p>${LC('newuser.desc')}</p>`
                            let ius = document.createElement('input')
                            ius.type = 'text'
                            ius.placeholder = LC('username')
                            let ips = document.createElement('input')
                            ips.type = 'password'
                            ips.placeholder = LC('password')
                            let isb = document.createElement('input')
                            isb.type = 'submit'
                            isb.value = LC('newuser')
                            isb.addEventListener('click', () => {
                                let e = document.createElement('div')
                                e.className = 'progress'
                                e.innerText = LC('updating')
                                document.body.appendChild(e)
                                xhr('/api/v1/admin/whoami', (a) => {
                                    let http = new XMLHttpRequest();
                                    http.open("POST", `/api/v1/admin/new/user`, true);
                                    http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                                    http.onreadystatechange = () => {
                                        if (http.readyState == 4) {
                                            if (http.status == 200 && JSON.parse(http.responseText).code == 200) {
                                                e.className = 'success'
                                                e.innerText = LC('updated')
                                                location.reload()
                                            } else {
                                                e.className = 'error'
                                                e.innerText = LC('update_error')
                                                setTimeout(() => {
                                                    document.body.removeChild(e)
                                                }, 2500)
                                            }
                                        }
                                    }
                                    http.send(encode({
                                        "name": ius.value,
                                        "password": ips.value,
                                        "csrf": a.data.csrf
                                    }))
                                })
                            })
                            div.appendChild(ius)
                            div.appendChild(ips)
                            div.appendChild(isb)
                            v.appendChild(div)
                            break
                        case 'upload':
                            let dropzoneform = document.createElement('form')
                            dropzoneform.className = 'dropzone'
                            dropzoneform.action = '/api/v1/admin/fileupload'
                            dropzoneform.enctype = 'multipart/form-data'
                            v.appendChild(dropzoneform)
                            let dropzone = new Dropzone(".dropzone", {
                                    url: "/api/v1/admin/fileupload"
                                }
                            )
                            break
                        case 'extensions':
                            let d = document.createElement('div')
                            d.className = 'grid'
                            v.appendChild(d)
                            xhr('/api/v1/admin/list/extensions', ((v, e) => {
                                for (let exti in e.data._indexing) {
                                    let name = e.data._indexing[exti]
                                    let ext = e.data[e.data._indexing[exti]]
                                    let g = document.createElement('div')
                                    let l = document.createElement('strong')
                                    let i = document.createElement('input')
                                    i.checked = ext.enabled
                                    i.type = "checkbox"
                                    i.addEventListener('change', () => {
                                        let e = document.createElement('div')
                                        e.className = 'progress'
                                        e.innerText = LC('updating')
                                        document.body.appendChild(e)
                                        xhr('/api/v1/admin/whoami', (a) => {
                                            let http = new XMLHttpRequest()
                                            http.open("POST", `/api/v1/admin/setextstate`, true)
                                            http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                                            http.onreadystatechange = () => {
                                                if (http.readyState == 4) {
                                                    if (http.status == 200 && JSON.parse(http.responseText).code == 200) {
                                                        e.className = 'success'
                                                        e.innerText = LC('updated')
                                                    } else {
                                                        e.className = 'error'
                                                        e.innerText = LC('update_error')
                                                        setTimeout(() => {
                                                            document.body.removeChild(e)
                                                        }, 2500)
                                                    }
                                                }
                                            }
                                            http.send(encode({
                                                "name": name,
                                                "csrf": a.data.csrf
                                            }))
                                        })
                                    })
                                    l.innerText = ext.name
                                    g.title = ext.description
                                    g.appendChild(i)
                                    g.appendChild(l)
                                    d.appendChild(g)
                                }
                            }).bind(null, v))
                            break
                    }
                }
            })
        })
    })
}
if(typeof __SIGNIN17_AVAILABLE==='undefined'){__setup_rjs()}