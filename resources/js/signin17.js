let indicator = document.querySelector('.overlay .indicator_label')
let prevoverlaytext = indicator.innerText
xhr('/api/v3/auth/csrf', (a) => {
    if (a.data.user === '') {
        indicator.innerText = 'Should our quantum hamsters trust you?'
        taiga.render({
            '.overlay': [
                {
                    tag: 'input.sgiinput#sgin',
                    attributes: {
                        type: 'text',
                        placeholder: 'Name'
                    }
                },
                {
                    tag: 'input.sgiinput#sgipwd',
                    attributes: {
                        type: 'password',
                        placeholder: 'Password'
                    }
                },
                {
                    tag: 'input.sgibtn',
                    attributes: {
                        type: 'submit',
                        value: 'Sign In'
                    },
                    on: {
                        click: () => {
                            let name = taiga.queryProperties('#sgin', ['value'])
                            let passwd = taiga.queryProperties('#sgipwd', ['value'])

                            locale = {
                                updating:'Verifying...',
                                xhr_error: 'Such user with those data does not exist.'
                            }
                            let pbar = spawnProgress()
                            xhrp('/api/v3/auth/login', () => {
                                taiga.render({
                                    '#sgipwd': false,
                                    '#sgin': false,
                                    '.sgibtn': false
                                })
                                indicator.innerText = prevoverlaytext
                                pbar.parentNode.removeChild(pbar)
                                __setup_rjs()
                            }, {
                                name: name.value,
                                pass: passwd.value
                            }, pbar)
                        }
                    }
                }
            ]
        })
    } else {
        __setup_rjs()
    }
})