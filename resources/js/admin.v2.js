function urlslug(s) {
    let char_map = {
        'À': 'A', 'Á': 'A', 'Â': 'A', 'Ã': 'A', 'Ä': 'A', 'Å': 'A', 'Æ': 'AE', 'Ç': 'C',
        'È': 'E', 'É': 'E', 'Ê': 'E', 'Ë': 'E', 'Ì': 'I', 'Í': 'I', 'Î': 'I', 'Ï': 'I',
        'Ð': 'D', 'Ñ': 'N', 'Ò': 'O', 'Ó': 'O', 'Ô': 'O', 'Õ': 'O', 'Ö': 'O', 'Ő': 'O',
        'Ø': 'O', 'Ù': 'U', 'Ú': 'U', 'Û': 'U', 'Ü': 'U', 'Ű': 'U', 'Ý': 'Y', 'Þ': 'TH',
        'ß': 'ss',
        'à': 'a', 'á': 'a', 'â': 'a', 'ã': 'a', 'ä': 'a', 'å': 'a', 'æ': 'ae', 'ç': 'c',
        'è': 'e', 'é': 'e', 'ê': 'e', 'ë': 'e', 'ì': 'i', 'í': 'i', 'î': 'i', 'ï': 'i',
        'ð': 'd', 'ñ': 'n', 'ò': 'o', 'ó': 'o', 'ô': 'o', 'õ': 'o', 'ö': 'o', 'ő': 'o',
        'ø': 'o', 'ù': 'u', 'ú': 'u', 'û': 'u', 'ü': 'u', 'ű': 'u', 'ý': 'y', 'þ': 'th',
        'ÿ': 'y',
        '©': '(c)',
        'Α': 'A', 'Β': 'B', 'Γ': 'G', 'Δ': 'D', 'Ε': 'E', 'Ζ': 'Z', 'Η': 'H', 'Θ': '8',
        'Ι': 'I', 'Κ': 'K', 'Λ': 'L', 'Μ': 'M', 'Ν': 'N', 'Ξ': '3', 'Ο': 'O', 'Π': 'P',
        'Ρ': 'R', 'Σ': 'S', 'Τ': 'T', 'Υ': 'Y', 'Φ': 'F', 'Χ': 'X', 'Ψ': 'PS', 'Ω': 'W',
        'Ά': 'A', 'Έ': 'E', 'Ί': 'I', 'Ό': 'O', 'Ύ': 'Y', 'Ή': 'H', 'Ώ': 'W', 'Ϊ': 'I',
        'Ϋ': 'Y',
        'α': 'a', 'β': 'b', 'γ': 'g', 'δ': 'd', 'ε': 'e', 'ζ': 'z', 'η': 'h', 'θ': '8',
        'ι': 'i', 'κ': 'k', 'λ': 'l', 'μ': 'm', 'ν': 'n', 'ξ': '3', 'ο': 'o', 'π': 'p',
        'ρ': 'r', 'σ': 's', 'τ': 't', 'υ': 'y', 'φ': 'f', 'χ': 'x', 'ψ': 'ps', 'ω': 'w',
        'ά': 'a', 'έ': 'e', 'ί': 'i', 'ό': 'o', 'ύ': 'y', 'ή': 'h', 'ώ': 'w', 'ς': 's',
        'ϊ': 'i', 'ΰ': 'y', 'ϋ': 'y', 'ΐ': 'i',
        'Ş': 'S', 'İ': 'I', 'Ğ': 'G',
        'ş': 's', 'ı': 'i', 'ğ': 'g',
        'А': 'A', 'Б': 'B', 'В': 'V', 'Г': 'G', 'Д': 'D', 'Е': 'E', 'Ё': 'Yo', 'Ж': 'Zh',
        'З': 'Z', 'И': 'I', 'Й': 'J', 'К': 'K', 'Л': 'L', 'М': 'M', 'Н': 'N', 'О': 'O',
        'П': 'P', 'Р': 'R', 'С': 'S', 'Т': 'T', 'У': 'U', 'Ф': 'F', 'Х': 'H', 'Ц': 'C',
        'Ч': 'Ch', 'Ш': 'Sh', 'Щ': 'Sh', 'Ъ': '', 'Ы': 'Y', 'Ь': '', 'Э': 'E', 'Ю': 'Yu',
        'Я': 'Ya',
        'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ё': 'yo', 'ж': 'zh',
        'з': 'z', 'и': 'i', 'й': 'j', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n', 'о': 'o',
        'п': 'p', 'р': 'r', 'с': 's', 'т': 't', 'у': 'u', 'ф': 'f', 'х': 'h', 'ц': 'c',
        'ч': 'ch', 'ш': 'sh', 'щ': 'sh', 'ъ': '', 'ы': 'y', 'ь': '', 'э': 'e', 'ю': 'yu',
        'я': 'ya',
        'Є': 'Ye', 'І': 'I', 'Ї': 'Yi', 'Ґ': 'G',
        'є': 'ye', 'і': 'i', 'ї': 'yi', 'ґ': 'g',
        'Ď': 'D', 'Ě': 'E', 'Ň': 'N', 'Ř': 'R', 'Ť': 'T', 'Ů': 'U',
        'ď': 'd', 'ě': 'e', 'ň': 'n', 'ř': 'r', 'ť': 't', 'ů': 'u',
        'Ą': 'A', 'Ć': 'C', 'Ę': 'e', 'Ł': 'L', 'Ń': 'N', 'Ś': 'S', 'Ź': 'Z',
        'Ż': 'Z',
        'ą': 'a', 'ć': 'c', 'ę': 'e', 'ł': 'l', 'ń': 'n', 'ś': 's', 'ź': 'z',
        'ż': 'z',
        'Ā': 'A', 'Č': 'C', 'Ē': 'E', 'Ģ': 'G', 'Ī': 'i', 'Ķ': 'k', 'Ļ': 'L', 'Ņ': 'N',
        'Š': 'S', 'Ū': 'u', 'Ž': 'Z',
        'ā': 'a', 'č': 'c', 'ē': 'e', 'ģ': 'g', 'ī': 'i', 'ķ': 'k', 'ļ': 'l', 'ņ': 'n',
        'š': 's', 'ū': 'u', 'ž': 'z'
    }
    for (let k in char_map) {
        s = s.replace(RegExp(k, 'g'), char_map[k])
    }
    let alnum = (typeof(XRegExp) === 'undefined') ? RegExp('[^a-z0-9]+', 'ig') : XRegExp('[^\\p{L}\\p{N}]+', 'ig');
    s = s.replace(alnum, '-')
    s = s.replace(RegExp('[-]{2,}', 'g'), '-')
    s = s.replace(RegExp('(^-|-$)', 'g'), '');
    s = s.substring(0, 120)
    return s.toLowerCase()
}


let locale
function LC(k) {
    if (typeof locale[k] === 'undefined') return k
    return locale[k]
}
function encode(obj) {
    let str = ""
    for (let key in obj) {
        if (str != "") {
            str += "&"
        }
        str += key + "=" + encodeURIComponent(obj[key])
    }
    return str
}
function spawnProgress() {
    taiga.render({
        body: [
            {
                tag: `div.progress`,
                data: LC('updating'),
                save: true
            }
        ]
    })
    return taiga.saved_object
}
function destroyProgress(pbar) {
    setTimeout(()=>{document.body.removeChild(pbar)}, 2500)
}
function progressSuccess(pbar) {
    pbar.className = 'success'
    pbar.innerText = LC('updated')
}
function progressError(pbar) {
    pbar.className = 'error'
    pbar.innerText = LC('update_error')
}
// __setup_rjs even if this is not rjs.
function __setup_rjs() {
    taiga.render({'.overlay':false})
    xhr('/api/v2/refresh_csrf', (a) => {
        let isfirst = true
        if (a.code === 401) {
            location.reload()
            return
        }

        xhr('/api/v2/locale', (l) => {
            locale = l.data

            taiga.render({
                body: [{
                    tag: 'div.deprecated',
                    data: LC('deprecated'),
                    html: true,
                    on: {
                        click: (view) => {
                            view.className = 'deprecated np'
                        }
                    }
                }]
            })

            taiga.render({
                '#buttons': [{
                    tag: 'button.logout',
                    data: LC('logout'),
                    on: {
                        click: () => {
                            xhr('/api/v2/invalidate_session', (s) => {
                                location.href = '/admin'
                            })
                        }
                    }
                }],
                '#uname': [{
                    tag: 'span',
                    data: a.data.user
                }]
            })

            xhr('/api/v2/admin/list/settings', (s) => {
                taiga.render({
                    body: [
                        {
                            tag: 'nav',
                            datasource: s.data,
                            onnewitem: (row, data) => {
                                taiga.render({
                                    body: [
                                        {
                                            tag: `section#s-${row.replace(/\./g, '_')}`,
                                            attributes: {
                                                class: isfirst ? 'sactive position-ref' : 'position-ref',
                                                style: isfirst ? 'display: block' : 'display: none'
                                            },
                                            datasource: data,
                                            onnewitem: (srow, rowdata) => {
                                                let inputnode
                                                if (rowdata.type === 'widget') {
                                                    inputnode = {
                                                        tag: `div#${rowdata.identifier}`,
                                                        children: [
                                                            {
                                                                tag: 'script',
                                                                attributes: {
                                                                    async: true,
                                                                    src: rowdata.value
                                                                }
                                                            }
                                                        ]
                                                    }
                                                } else {
                                                    inputnode = {
                                                        tag: 'input',
                                                        attributes: {
                                                            class: rowdata.spoiler ? 'spoiler' : '',
                                                            value: rowdata.value,
                                                            type: rowdata.type
                                                        },
                                                        on: {}
                                                    }

                                                    if (rowdata.type === 'submit') {
                                                        inputnode.on.click = (view, event) => {
                                                            let e = spawnProgress()
                                                            let http = new XMLHttpRequest()
                                                            http.open("GET", rowdata.action, true)
                                                            http.onreadystatechange = () => {
                                                                if (http.readyState === 4) {
                                                                    if (http.status === 200 && JSON.parse(http.responseText).code === 200) {
                                                                        progressSuccess(e)
                                                                        destroyProgress(e)
                                                                    } else {
                                                                        progressError(e)
                                                                        destroyProgress(e)
                                                                    }
                                                                }
                                                            }
                                                            http.send(null)
                                                        }
                                                    } else {
                                                        inputnode.on.keydown = (view, event) => {
                                                            if (event.keyCode === 13) {
                                                                let pbar = spawnProgress()
                                                                xhr('/api/v2/refresh_csrf', (a) => {
                                                                    let http = new XMLHttpRequest()
                                                                    http.open("POST", `/api/v2/admin/update/${row}.${srow}`, true)
                                                                    http.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
                                                                    http.onreadystatechange = () => {
                                                                        if (http.readyState === 4) {
                                                                            if (http.status === 200 && JSON.parse(http.responseText).code === 200) {
                                                                                progressSuccess(pbar)
                                                                                destroyProgress(pbar)
                                                                            } else {
                                                                                progressError(pbar)
                                                                                destroyProgress(pbar)
                                                                            }
                                                                        }
                                                                    }
                                                                    http.send(encode({
                                                                        "value": view.value,
                                                                        "csrf": a.data.csrf
                                                                    }))
                                                                })
                                                            }
                                                        }
                                                    }
                                                }
                                                return {
                                                    tag: 'div.indexed',
                                                    children: [
                                                        {
                                                            tag: 'strong',
                                                            data: LC(`${row}.${srow}`)
                                                        },
                                                        {
                                                            tag: 'p',
                                                            data: LC(`${row}.${srow}.desc`),
                                                            html: true
                                                        },
                                                        inputnode
                                                    ]
                                                }
                                            }
                                        }
                                    ]
                                })

                                let wasfirst = isfirst
                                isfirst = false

                                return {
                                    tag: data.handler === "header" ? 'div.header' : (wasfirst ? 'div.active' : 'div'),
                                    children: [
                                        {
                                            tag: 'strong',
                                            data: LC(row)
                                        }
                                    ],
                                    on: {
                                        click: (view) => {
                                            if (view.className.indexOf('header') === -1) {
                                                let currentactive = document.querySelector('nav > div.active')
                                                currentactive.className = ''
                                                view.className = 'active'
                                                let sactive = document.getElementsByClassName('sactive')[0]
                                                sactive.className = 'position-ref'
                                                sactive.style.display = 'none'
                                                let section = document.getElementById(`s-${row.replace(/\./g, '_')}`)
                                                section.style.display = 'block'
                                                section.className = 'position-ref sactive'
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    ]
                })
            })

        })
    })
}

if (typeof __SIGNIN17_AVAILABLE === 'undefined') {
    __setup_rjs()
}