function spinUpEditor (guid) {

  function callback (data) {
      taiga.render({
          '#s-pages': [{
              tag: 'div.editor-overlay',
              children: [
                  {
                      tag: 'nav.editor-sidebar',
                      children: [
                          {
                              tag: 'button',
                              attributes: {
                                  style: bgsize('/resources/svg/bold'),
                                  title: LC('bold.caption')
                              },
                              on: {
                                  click: () => {
                                      let textarea = document.getElementById('ta-editor')

                                      textarea.value += `**${LC('bold.placeholder')}**`
                                  }
                              }
                          },
                          {
                              tag: 'button',
                              attributes: {
                                  style: bgsize('/resources/svg/italics'),
                                  title: LC('italics.caption')
                              },
                              on: {
                                  click: () => {
                                      let textarea = document.getElementById('ta-editor')

                                      textarea.value += `*${LC('italics.placeholder')}*`
                                  }
                              }
                          },
                          {
                              tag: 'separator'
                          },
                          {
                              tag: 'button',
                              attributes: {
                                  style: bgsize('/resources/svg/publish'),
                                  title: LC('publish.caption')
                              },
                              on: {
                                  click: (view) => {
                                      view.className = view.className === 'active' ? '' : 'active'
                                  }
                              },
                              children: [{
                                  tag: 'dialog#editor-submit',
                                  on: {
                                      click: (_, e) => {
                                          e.stopPropagation()
                                      }
                                  },
                                  children: [
                                      {
                                          tag: 'div',
                                          children: [
                                              {
                                                  tag: 'label',
                                                  data: LC('uri')
                                              },
                                              {
                                                  tag: 'input#uri-editor',
                                                  attributes: {
                                                      type: 'text',
                                                      placeholder: LC('uri')
                                                  },
                                                  properties: {
                                                      value: data.URI
                                                  }
                                              }
                                          ]
                                      },
                                      {
                                          tag: 'div',
                                          children: [
                                              {
                                                  tag: 'label',
                                                  data: LC('tags')
                                              },
                                              {
                                                  tag: 'input#tags-editor',
                                                  attributes: {
                                                      type: 'text',
                                                      placeholder: LC('tags')
                                                  }
                                              }
                                          ]
                                      },,
                                      {
                                          tag: 'div',
                                          children: [
                                              {
                                                  tag: 'label',
                                                  data: LC('language')
                                              },
                                              {
                                                  tag: 'input#language-editor',
                                                  attributes: {
                                                      type: 'text',
                                                      value: data.Language,
                                                      placeholder: LC('language')
                                                  }
                                              }
                                          ]
                                      },
                                      {
                                          tag: 'div.space-between',
                                          children: [
                                              {
                                                  tag: 'label',
                                                  data: LC('isindex')
                                              },
                                              {
                                                  tag: 'input#isindex-editor',
                                                  attributes: {
                                                      type: 'checkbox'
                                                  },
                                                  properties: {
                                                      checked: data.IsIndex === "1"
                                                  }
                                              }
                                          ]
                                      },
                                      {
                                          tag: 'div',
                                          children: [
                                              {
                                                  tag: 'button',
                                                  data: LC('publish'),
                                                  on: {
                                                      click: () => {
                                                          let pbar = spawnProgress()

                                                          let uri = taiga.queryProperties('#uri-editor', ['value'])
                                                          let isindex = taiga.queryProperties('#isindex-editor', ['checked'])
                                                          let tags = taiga.queryProperties('#tags-editor', ['value'])
                                                          let language = taiga.queryProperties('#language-editor', ['value'])
                                                          let text = taiga.queryProperties('#ta-editor', ['value'])
                                                          let title = taiga.queryProperties('#title-editor', ['value'])

                                                          xhrp(`/api/v3/pages/write/${guid}`, () => {
                                                              progressSuccess(pbar)
                                                              destroyProgress(pbar)

                                                              taiga.render({
                                                                  'div.editor-overlay': false
                                                              })
                                                          }, {
                                                              title: title.value,
                                                              content: text.value,
                                                              language: language.value,
                                                              isindex: isindex.checked,
                                                              uri: uri.value,
                                                              tags: tags.value
                                                          }, pbar)
                                                      }
                                                  }
                                              }
                                          ]
                                      }
                                  ]
                              }]
                          }
                      ]
                  },
                  {
                      tag: 'article.editor-writer',
                      children: [
                          {
                              tag: 'div.floating',
                              children: [
                                  {
                                      tag: 'button.rounded.left',
                                      attributes: {
                                          style: bgsize('/resources/svg/code')
                                      }
                                  },
                                  {
                                      tag: 'separator'
                                  },
                                  {
                                      tag: 'button.rounded.right',
                                      attributes: {
                                          style: bgsize('/resources/svg/render')
                                      }
                                  }
                              ]
                          },
                          {
                              tag: 'input#title-editor',
                              attributes: {
                                  type: 'text',
                                  placeholder: LC('name_your_story'),
                                  value: data.Title
                              }
                          },
                          {
                              tag: 'div.container',
                              children: [
                                  {
                                      tag: 'textarea#ta-editor',
                                      attributes: {
                                          placeholder: LC('start_typing')
                                      },
                                      properties: {
                                          value: data.Content
                                      }
                                  },
                                  {
                                      tag: 'div.render#ta-render',
                                      children: [{
                                          tag: 'div.placeholder',
                                          data: LC('start_typing')
                                      }],
                                      attributes: {
                                          style: 'display: none'
                                      }
                                  }
                              ]
                          }
                      ]
                  }
              ]
          }]
      })
  }

  if (guid !== 'random') {
      xhr(`/api/v3/pages/read/${guid}`, (data) => {
          callback(data.data)
      })
  } else {
      callback({
          Title: "",
          Content: "",
          Language: "en-US",
          IsIndex: 0,
          URI: ''
      })
  }
}

xhr('/api/v3/pages/list', (s) => {
    taiga.render({
        '#s-pages': [
            {
                tag: 'div.page-grid',
                datasource: s.data,
                onitemsrendered: () => {
                    taiga.render({
                        '#s-pages > div.page-grid': [{
                            tag: 'div.page-entry.add',
                            data: LC('write_a_story'),
                            on: {
                                click: spinUpEditor.bind(null, 'random')
                            }
                        }]
                    })
                },
                onnewitem: (row, data) => {
                    return {
                        tag: 'div.page-entry',
                        children: [
                            {
                                tag: 'div',
                                children: [
                                    {
                                        tag: 'strong',
                                        data: data.Title
                                    },
                                    {
                                        tag: 'p',
                                        data: `${LC('written_in')} ${LC('lang-' + data.Language)}`
                                    }
                                ]
                            },
                            {
                                tag: 'div',
                                children: [
                                    {
                                        tag: 'button',
                                        data: '🖊️',
                                        on: {
                                            click: spinUpEditor.bind(null, data.GUID)
                                        }
                                    },
                                    {
                                        tag: 'button',
                                        data: '🗑️'
                                    }
                                ]
                            }
                        ]
                    }
                }
            }
        ]
    })
})