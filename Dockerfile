# Use PHP 7.1.9 with Apache.
# This is setup that is verified to work.
FROM php:7.1.9-apache

# Enable Rewrite and Headers Apache modules.
# These are recommended.
# Rewrite is required if using pretty links.
RUN a2enmod rewrite
RUN a2enmod headers

# Install PHP dependencies.
RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libpng12-dev \
        libxml2-dev \
        diffutils \
        patch

# Clean up after APT.
RUN apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Install PHP extensions that are required for Meerkat to work.
RUN docker-php-ext-install -j$(nproc) mcrypt mbstring pdo pdo_mysql json fileinfo
RUN docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/
RUN docker-php-ext-install -j$(nproc) gd

# Copy Meerkat files.
RUN mkdir -p /var/www/source
ADD app /var/www/source/app
ADD extensions /var/www/source/extensions
ADD themes /var/www/source/themes
ADD resources /var/www/source/resources
ADD cache /var/www/source/cache
ADD db /var/www/source/db
ADD tmp /var/www/source/tmp
ADD vendor /var/www/source/vendor
ADD .htaccess /var/www/source/.htaccess
ADD index.php /var/www/source/index.php
ADD meerkat /var/www/source/meerkat
ADD favicon.ico /var/www/source/favicon.ico
RUN mkdir -p /var/www/source/storage/uploads
RUN echo "OK">/var/www/source/health.html
ADD app/docker-entrypoint.sh /var/www/docker-entrypoint.sh
RUN chmod +x /var/www/docker-entrypoint.sh
RUN chmod +x /var/www/source/meerkat

ENTRYPOINT "/var/www/docker-entrypoint.sh"
CMD ["start"]