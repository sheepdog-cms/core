<?php
return function ($database) {
    if (!$database->Create('pages', array(
        'GUID' => $database->DataType_GUID,
        'Title' => $database->DataType_TEXT,
        'Content' => $database->DataType_TEXT,
        'Uri' => $database->DataType_TEXT,
        'IsIndex' => $database->DataType_BOOLEAN,
        'Language' => $database->DataType_CHARS(5)
    ))) {
        return false;
    }

    if (!$database->Insert(array(
        'GUID' => 'WELCOME',
        'Title' => "Welcome to your Meerkat!",
        'Content' => "*whizzing, whirring, metal clunking*\n\nOh, hi there! Thanks for installing Meerkat! Now see your brand new [admin panel](/admin).",
        'Uri' => 'welcome-to-meerkat',
        'IsIndex' => 1,
        'Language' => 'en-US'
    ), 'pages')) {
        return false;
    }

    return true;
};