<?php
return function ($database) {
    global $config;

    if (!$database->Create('settings', array(
        'Key'     => $database->DataType_TEXT,
        'Value' => $database->DataType_TEXT
    ))) {
        return false;
    }

    if (!$database->Insert(array(
        'Key' => 'site.name',
        'Value' => $config['app.name']
    ), 'settings')) {
        return false;
    }

    if (!$database->Insert(array(
        'Key' => 'site.desc',
        'Value' => ''
    ), 'settings')) {
        return false;
    }

    if (!$database->Insert(array(
        'Key' => 'site.keywords',
        'Value' => ''
    ), 'settings')) {
        return false;
    }

    if (!$database->Insert(array(
        'Key' => 'meerkat.extensions',
        'Value' => $config['meerkat.extensions']
    ), 'settings')) {
        return false;
    }

    return true;
};